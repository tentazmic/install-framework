# Common/Graphics

## Inheritance

As we may support multiple different graphics APIs all in the same binary, the
graphics part of the codebase uses inheritance.  
Simple inheritance specifically, one base class with multiple derived classes, ideally
one for each backend.

It would be nice to be able to swap between two backends on the fly. For things
like textures and mesh buffers, we can read the data from their current buffers
and send it to the new backend.  
However, we cannot do that for shaders. For this reason the path variable on shader
objects is not wrapped in the `HOLD_IDS` directive, whereas it is on textures.

All subject to change.

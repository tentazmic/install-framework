#pragma once

#include "core"

namespace tik {

struct frame_stamp
{
	u32 frame;
	f32 deltaTime;
	f32 timeSinceStart;
};

};

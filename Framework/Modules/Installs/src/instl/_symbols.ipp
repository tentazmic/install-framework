INSTALL_VAR(::instl::install_info, Information);

INSTALL_FUNC(void, Startup, ::instl::pointers&);
INSTALL_FUNC(void, Shutdown,);

INSTALL_FUNC(void, LoadScene, gfx::_TextureLib *, arr::list<game::sprite>&, arr::list<game::camera>&);

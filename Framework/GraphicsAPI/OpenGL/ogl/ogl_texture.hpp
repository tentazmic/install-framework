#pragma once
#include "math"
#include "Graphics/texture.hpp"
#include "Util/list.hpp"
#include "Util/wrap_list.hpp"

namespace gfx {

class OGLTexture : public _Texture
{
public:
	OGLTexture(texture_format, u32 width, u32 height, u32 depth);
	OGLTexture(const texture&);
	~OGLTexture() override;

	void Bind(u32 slot = 0) const override;
	void SetTextureData(const void *, size_t) override;

	const ETextureType type() const override
		{ return _dimensions.z <= 1 ? ETextureType::TWOD : ETextureType::THREED; }
	texture_format format() const override { return _format; }
	glm::uvec3 dimensions() const override { return _dimensions; }

#ifdef HOLD_IDS
	const char * GetPath() const override { return _path; }
#endif
private:
	u32 _ID;
	glm::uvec3 _dimensions;
	texture_format _format;

#ifdef HOLD_IDS
	const char *_path;
#endif
};

class OGLTextureLib : public _TextureLib
{
public:
	OGLTextureLib(size_t capacity);
	~OGLTextureLib() override;

	hdl_texture Create(texture_format, u32 width, u32 height, u32 depth = 0) override;
	hdl_texture Load(const texture&) override;
	_Texture * Get(hdl_texture) override;
	bool IsValid(hdl_texture) const override;

private:
	arr::dync_wrap_list<OGLTexture> _textures;
	mem::dync_generations_tracker<hdl_texture::TYPE> _generations;
};
};

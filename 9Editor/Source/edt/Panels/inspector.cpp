#include "pch.h"
#include <imgui.h>
#include "inspector.hpp"
#include "edt/GUI/inspect.hpp"
#include "edt/GUI/file_explorer.hpp"
#include "edt/editor.hpp"

namespace edt::panel::inspector {

PnlInspector::PnlInspector(PanelRegistry *registry, const inspect_target& t)
	: _Panel(), target{t}, ID{-1}, parent{-1}, iChild{-1},
	  hasChildren{false}, _registry{registry}, _shared{nullptr}
{
	PnlInspector *other = registry->GetPanel<PnlInspector>();
	if (other)
	{
		_shared = other->shared();
		ID = _shared->inspectors.count();
		parent = _shared->iLastFocused;

		if (parent != -1)
		{
			iChild = 0;
			_shared->inspectors[parent]->hasChildren = true;
			FOR (it, _shared->inspectors)
			{
				if ((*it)->parent == parent)
					iChild++;
			}
		}
	}
	else
	{
		_shared = new shared_data
		{
			.watcher = edt::asset::watcher("Assets", 30),
			.activeEntries = arr::HeapBitset(30)
		};
		ID = 0;
	}

	char *buffer = new char[15];
	sprintf(buffer, "Inspector %d", ID);
	_name = buffer;

	_shared->iLastFocused = ID;
	_shared->inspectors.push(this);
}

PnlInspector::~PnlInspector()
{
	for (auto it = _shared->inspectors.begin() + (ID + 1); it != _shared->inspectors.end(); it++)
	{
		(*it)->ID--;
		if ((*it)->parent > ID)
			(*it)->parent--;
		else if ((*it)->parent == ID)
			(*it)->parent = parent;
	}

	if (_shared->iLastFocused == ID)
	{
		_shared->iLastFocused = -1;
	}

	_shared->inspectors.remove(ID);
	if (_shared->inspectors.count() == 0)
		delete _shared;
	delete _name;
}

void PnlInspector::Update(f32)
{
	if (!target.IsActive() && _shared->filesystemUser == -1)
		_shared->filesystemUser = ID;
	if (target.IsActive() && _shared->filesystemUser == ID)
		_shared->filesystemUser = -1;
}

void PnlInspector::OnEvent(evt::event& e)
{

}

void PnlInspector::GUI()
{
	const char *preview = "NONE";
	if (parent != -1)
	{
		preview = _shared->inspectors[parent]->name();
		ImGui::Text("Child %d", iChild);
		ImGui::SameLine();
	}

	if (ImGui::BeginCombo("##Parent", preview))
	{
		i8 prevParent = parent;
		if (ImGui::Selectable("NONE", parent == -1))
			parent = -1;

		if (parent == -1)
			ImGui::SetItemDefaultFocus();

		FOR (it, _shared->inspectors)
		{
			if ((*it)->ID == ID) continue;

			const bool selected = parent == (*it)->ID;
			if (ImGui::Selectable((*it)->name(), selected))
				parent = it - _shared->inspectors.begin();

			// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
			if (selected)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();

		if (prevParent != parent)
		{
			i8 oldChild = iChild;
			iChild = 0;
			_shared->inspectors[prevParent]->hasChildren = false;
			FOR (it, _shared->inspectors)
			{
				if (*it == this)
					continue;

				// Update iChild
				if ((*it)->parent == parent)
					iChild++;
				// Update iChild on children of previous parent
				if ((*it)->parent == prevParent)
				{
					_shared->inspectors[prevParent]->hasChildren = true;
					if ((*it)->iChild > oldChild)
						(*it)->iChild--;
				}
			}
		}
	}

	if (!target.IsActive())
	{
		if (parent == -1)
			EmptyView();
	}
	else if (target.isEntity())
		EntityView();
	else
		WorldView();
}

void PnlInspector::OnFocusGained()
{
	_shared->iLastFocused = ID;
}

void PnlInspector::EmptyView()
{
	// A split view, pick either one of the opened scenes
	// or an inspectable file from the file system
	// On Children show a window showing parent and give a combo box to clear parent
	bool wider = _size.x > _size.y;
	ImVec2 ratRecents(1, 1);
	if (wider)
	{
		ratRecents.x = 0.35f;
	}
	else
	{
		ratRecents.y = 0.35f;
	}

	if (!edt::asset::GetActiveScenes().empty())
	{
		if (ImGui::BeginChild("Open Worlds", ImVec2(_size.x * ratRecents.x, _size.y * ratRecents.y), true))
		{
			ImGui::TextWrapped("This scenes that are already open");
			if (ImGui::BeginListBox("Worlds"))
			{
				FOR (it, edt::asset::GetActiveScenes())
				{
					if (ImGui::Selectable(it->first, false))
					{
						IntoNewTarget(inspect_target{ .scene = &it->second });
					}
				}
				ImGui::EndListBox();
			}
		} ImGui::EndChild();
	}

	if (_shared->filesystemUser != ID)
		return;

	ratRecents.x = 1.0f - ratRecents.x;
	ratRecents.y = 1.0f - ratRecents.y;
	if (wider) ImGui::SameLine();

	if (ImGui::BeginChild("Filesystem", ImVec2(_size.x * ratRecents.x, _size.y * ratRecents.y), true))
	{
		edt::asset::directory_tree tree = _shared->watcher.tree();
		edt::gui::explorer_selection selection = edt::gui::DrawFileExplorer(tree, _shared->selection, _shared->activeEntries.get(), edt::gui::explorer_filter{ .extension = ".scn" });
		if (selection.index != tree.count())
		{
			_shared->selection = selection.index;

			if (selection.byDoubleSelection && !tree[selection.index].isDirectory)
			{
				fs::path fullpath = _shared->watcher.GetFullPath(selection.index);
				edt::asset::scene *scene = edt::asset::LoadScene(fullpath);
				if (scene->world)
				{
					clbk::Invoke<void, const edt::asset::scene&>(editor::e_OpenScene, *scene);
					IntoNewTarget(inspect_target{ scene });
				}
			}
		}
	} ImGui::EndChild();
}

void PnlInspector::WorldView()
{
	i32 i = -1;
	FOR (entity, (*target.scene->world))
	{
		i++;
		const ecs::entity *e = entity.get();
		if (ImGui::Selectable(e->name, target.selected == i))
		{
			if (target.selected == i)
			{
				IntoNewTarget(inspect_target{ .scene = target.scene, .entity = entity.handle });
			}
			else
				target.selected = i;
		}
	}
}

void PnlInspector::EntityView()
{
	if (ImGui::Button("Back"))
	{
		Back();
		return;
	}

	const ecs::entity *entity = target.scene->world->GetEntity(target.entity);
	ImGui::Text(entity->name);

	CFOR (cmp, entity->components)
	{
		if (ImGui::CollapsingHeader(cmp->type->name.c_str(), ImGuiTreeNodeFlags_None))
		{
			inspect::InspectComponent(cmp->type, target.scene->world->GetComponent(cmp->type, target.entity));
		}
	}

	ImGui::Separator();
}

void PnlInspector::IntoNewTarget(const inspect_target& t)
{
	if (!hasChildren)
	{
		if (parent == -1)
		{
			target = t;
			return;
		}

		_shared->inspectors[parent]->target = t;
		return;
	}

	FOR (it, _shared->inspectors)
	{
		if ((*it)->parent == ID)
		{
			(*it)->target = target;
			target = t;
			break;
		}
	}
}

void PnlInspector::Back()
{
	FOR (it, _shared->inspectors)
	{
		if ((*it)->parent == ID)
		{
			target = (*it)->target;
			(*it)->Back();
			return;
		}
	}

	// No Children
	if (target.isEntity())
		target.entity = ecs::entity_handle{};
	else
		target = inspect_target{};
}
};

#include "pch.h"
#include "panel.hpp"

namespace edt::panel {

_Panel::_Panel()
	: _name("Panel"),
	  _position(), _prevPosition(),
	  _size(), _prevSize(),
	  _delete(false), _hovered(false),
	  _focused(false), _moved(false), _resized(false)
{ }

_Panel::_Panel(const char *name)
	: _name(name),
	  _position(), _prevPosition(),
	  _size(), _prevSize(),
	  _delete(false), _hovered(false),
	  _focused(false), _moved(false), _resized(false)
{ }

void _Panel::GUIRender()
{
	PreGUI();
	bool active = true;
	if (ImGui::Begin(_name, &active, _fWindow))
	{
		_prevPosition = _position;
		_position = ImGui::GetCursorScreenPos();
		_prevSize = _size;
		_size = ImGui::GetContentRegionAvail();
		_moved = _position.x != _prevPosition.x || _position.y != _prevPosition.y;
		_resized = _size.x != _prevSize.x || _size.y != _prevSize.y;

		bool prevFocus = _focused, prevHover = _hovered;
		_focused = ImGui::IsWindowFocused(ImGuiFocusedFlags_ChildWindows);
		_hovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows);
		if (_focused != prevFocus)
			_focused ? OnFocusGained() : OnFocusLost();
		if (_hovered != prevHover)
			_hovered ? OnHoverGained() : OnHoverLost();

		GUI();
	} ImGui::End();
	PostGUI();

	if (active) return;

	_delete = true;
}

PanelRegistry::PanelRegistry(const char *name, size_t nPanel)
	: _name{name}, _panels(nPanel), _iFocused(0), _iHovered(0)
{ }

PanelRegistry::~PanelRegistry()
{
	// FOR(it, _panels)
	// 	delete *it;
	// _panels.Delete();
}

void PanelRegistry::OnAttach()
{
}

void PanelRegistry::OnDetach()
{
}

void PanelRegistry::OnUpdate(f32 deltaTime)
{
	FOR(it, _panels)
		(*it)->Update(deltaTime);
}

void PanelRegistry::OnImGUIRender()
{
	_iHovered = _iFocused = -1;
	for (size_t i = 0; i < _panels.count(); i++)
	{
		_panels[i]->GUIRender();
		if (_panels[i]->hovered())
		{
			if (_iHovered == -1)
				_iHovered = i;
			else
				ED_WRN("It seems there are more than one hovered panels");
		}

		if (_panels[i]->focused())
		{
			if (_iFocused == -1)
				_iFocused = i;
			else
				ED_WRN("It seems there's more than one focused panel");
		}
	}
}

void PanelRegistry::OnEvent(evt::event& e)
{
	if (_iFocused != -1)
		_panels[_iFocused]->OnEvent(e);

	for (size_t i = 0; i < _panels.count(); i++)
	{
		if ((i32)i != _iFocused)
			_panels[i]->OnEvent(e);
	}
}

void PanelRegistry::AddPanel(_Panel *panel)
{
	_panels.push(panel);
}

void PanelRegistry::DeletePanel(_Panel *panel)
{
	auto it = std::find(_panels.begin(), _panels.end(), panel);
	if (it == _panels.end())
	{
		ED_WRN("Tried to delete unregistered panel {}", panel->name());
		return;
	}

	// (*it)->OnDelete();
	delete *it;
	_panels.remove(it);
}

void PanelRegistry::EndOfFrameCleanup()
{
	for (i32 i = _panels.count() - 1; i >= 0; i--)
	{
		if (_panels[i]->ToDelete())
		{
			delete _panels[i];
			_panels.remove(i);
		}
	}
}

_Panel * PanelRegistry::GetFocused()
{
	if (_iFocused < 0)
		return nullptr;

	return _panels[_iFocused];
}

_Panel * PanelRegistry::GetHovered()
{
	if (_iHovered < 0)
		return nullptr;

	return _panels[_iHovered];
}
};
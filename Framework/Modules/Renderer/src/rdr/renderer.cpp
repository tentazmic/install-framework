#include "pch.h"
#include "renderer.hpp"

#include "debug"

#include "res/assets.hpp"
#include "Application/events.hpp"
#include "Graphics/mesh_buffer.hpp"
#include "dbg/math.hpp"
#include "dbg/formatters.hpp"

namespace rdr {

// 0 ------- 1
// |         |
// |         |
// |         |
// |         |
// |         |
// 3 ------- 2

Renderer2D::Renderer2D(_RenderAPI *api, const library_sizes &sizes,
                       u32 numQuads, u32 numChars,
                       const glm::vec4 &clearColour /* = glm::vec4(1.0f) */)
	: _api{ api },
	  m_libMeshes{ nullptr }, m_libShaders{ nullptr },
	  m_libTextures{ nullptr }, m_libFrameBuffers{ nullptr },
	  _quadBatch(numQuads), _mshQuad{},
	  _textBatch(numChars), _mshText{}, _texFont{ mem::make_dead<gfx::hdl_texture>() },
	  _textureSlots(api->max_texture_slots()), _shdBatchedQuad{}, _shdText{},
	  _stats{}
{
	api->SetClearColour(clearColour);

	m_libMeshes       = api->CreateMeshBufferLibrary(sizes.meshBuffer);
	m_libShaders      = api->CreateShaderLibrary(sizes.shader);
	m_libTextures     = api->CreateTextureLibrary(sizes.texture);
	m_libFrameBuffers = api->CreateFrameBufferLibrary(sizes.frameBuffer);

	const size_t kMaxIndexes = math::max({ numQuads, numChars }) * 6;
	u32 *indexes = new u32[kMaxIndexes];
	u32 offset = 0;
	for (size_t i = 0; i < kMaxIndexes; i += 6)
	{
		indexes[i + 0] = offset + 0;
		indexes[i + 1] = offset + 1;
		indexes[i + 2] = offset + 2;

		indexes[i + 3] = offset + 0;
		indexes[i + 4] = offset + 2;
		indexes[i + 5] = offset + 3;

		offset += 4;
	}

	{
		gfx::vertex_buffer_data vbd{
			.size = numQuads * SCAST(u32)(sizeof(batched_quad)),
			.type = gfx::EVertexBufferType::DYNAMIC,
			.layout = {
				VB_ELEM("aPosition",     FLOAT2, false),
				VB_ELEM("aColour",       FLOAT4, false),
				VB_ELEM("aTexCoord",     FLOAT2, false),
				VB_ELEM("aTexIndex",     INT1,   false),
				VB_ELEM("aTilingFactor", FLOAT1, false)
			}
		};

		gfx::index_buffer_data ibd{ .indexes = indexes, .count = numQuads * 6 };

		arr::sttc_list<gfx::vertex_buffer_data, 1> vbds = { vbd };
		_mshQuad = m_libMeshes->Submit(ibd, vbds.get());
	}

	{
		gfx::vertex_buffer_data vbd{
			.size = numChars * SCAST(u32)(sizeof(batched_char)),
			.type = gfx::EVertexBufferType::DYNAMIC,
			.layout = {
				VB_ELEM("aPosition", FLOAT2, false),
				VB_ELEM("aColour",   FLOAT4, false),
				VB_ELEM("aTexCoord", FLOAT2, false)
			}
		};

		gfx::index_buffer_data ibd{ .indexes = indexes, .count = numChars * 6 };

		arr::sttc_list<gfx::vertex_buffer_data, 1> vbds = { vbd };
		_mshText = m_libMeshes->Submit(ibd, vbds.get());
	}

	delete[] indexes;

	i32 *samplers = new i32[api->max_texture_slots()];
	for (u32 i = 0; i < api->max_texture_slots(); i++)
		samplers[i] = i;

	_shdBatchedQuad = m_libShaders->Load(AssetPath(Shaders/quad2D.glsl));
	_shdText = m_libShaders->Load(AssetPath(Shaders/text.glsl));
	gfx::_Shader *s = m_libShaders->Get(_shdBatchedQuad);

	s->Bind();
	s->UploadInt1Array("uTextures", samplers, api->max_texture_slots());

	delete[] samplers;
}

Renderer2D::~Renderer2D()
{
	_api->FreeMeshBufferLibrary(m_libMeshes);
	_api->FreeShaderLibrary(m_libShaders);
	_api->FreeTextureLibrary(m_libTextures);
	_api->FreeFrameBufferLibrary(m_libFrameBuffers);
}

void Renderer2D::StartBatch(const render_target &target)
{
	glm::mat4 view = math::CalculateModel2D(
		target.camera.position,
		target.camera.rotation,
		glm::vec2(1.0f)
	);
	glm::mat4 projection = glm::ortho(
		-target.camera.halfSize.x, target.camera.halfSize.x,
		-target.camera.halfSize.y, target.camera.halfSize.y,
		-1.0f, 1.0f
	);

	gfx::_Shader *s = m_libShaders->Get(_shdBatchedQuad);
	s->Bind();
	s->UploadMat4("uViewProjection", projection * view);

	s = m_libShaders->Get(_shdText);
	s->Bind();
	s->UploadMat4("uViewProjection", projection * view);
}

void Renderer2D::FlushBatch()
{
	for (u32 i = 0; i < _textureSlots.count(); i++)
		m_libTextures->Get(_textureSlots[i])->Bind(i);

	if (!_quadBatch.empty())
	{
		gfx::_Shader *s = m_libShaders->Get(_shdBatchedQuad);
		s->Bind();

		u32 size = _quadBatch.count() * sizeof(batched_quad);
		gfx::_MeshBuffer *mesh = m_libMeshes->Get(_mshQuad);
		mesh->GetVertexBuffer(0)->AssignData(_quadBatch.data(), size);
		_api->DrawIndexed(mesh, _quadBatch.count() * 6);
		_stats.drawCalls++;
	}

	if (!_textBatch.empty())
	{
		gfx::_Shader *s = m_libShaders->Get(_shdText);
		s->Bind();
		m_libTextures->Get(_texFont)->Bind(0);

		u32 size = _textBatch.count() * sizeof(batched_char);
		gfx::_MeshBuffer *mesh = m_libMeshes->Get(_mshText);
		mesh->GetVertexBuffer(0)->AssignData(_textBatch.data(), size);
		_api->DrawIndexed(mesh, _textBatch.count() * 6);
		_stats.drawCalls++;
	}

	_quadBatch.clear();
	_textBatch.clear();
	_textureSlots.clear();
}

bool Renderer2D::SubmitQuad(const quad_data &data)
{
	i32 idxTexture = -1;
	const gfx::hdl_texture kDead = mem::make_dead<gfx::hdl_texture>();
	if (data.texture != kDead)
	{
		auto it = _textureSlots.find(data.texture);
		if (it != _textureSlots.end())
		{
			idxTexture = it - _textureSlots.begin();
		}
		else if (!_textureSlots.full())
		{
			idxTexture = _textureSlots.count();
			_textureSlots.push(data.texture);
		}
		else
		{
			return false;
		}
	}

	glm::mat3 transform;
	{
		glm::mat3 t = glm::translate(glm::mat3(1.0f), data.position);
		glm::mat3 rt = glm::rotate(t, data.rotation);
		transform = glm::scale(rt, data.size);
	}

	glm::vec3 positions[] = { { -0.5f, 0.5f, 1.f }, { 0.5f, 0.5f, 1.f },
	                          { 0.5f, -0.5f, 1.f }, { -0.5f, -0.5f, 1.f } };

	batched_quad quad;
	quad.vertexes[0] = {
		.position = glm::vec2(transform * positions[0]),
		.colour = data.colour,
		.uv = { 0.0f, 1.0f },
		.texture = idxTexture,
    .tilingFactor = data.tilingFactor
	};
	quad.vertexes[1] = {
		.position = glm::vec2(transform * positions[1]),
		.colour = data.colour,
		.uv = { 1.0f, 1.0f },
		.texture = idxTexture,
    .tilingFactor = data.tilingFactor
	};
	quad.vertexes[2] = {
		.position = glm::vec2(transform * positions[2]),
		.colour = data.colour,
		.uv = { 1.0f, 0.0f },
		.texture = idxTexture,
    .tilingFactor = data.tilingFactor
	};
	quad.vertexes[3] = {
		.position = glm::vec2(transform * positions[3]),
		.colour = data.colour,
		.uv = { 0.0f, 0.0f },
		.texture = idxTexture,
    .tilingFactor = data.tilingFactor
	};

	_quadBatch.push(quad);
	_stats.trisDrawn += 2;
	_stats.vertexCount += 4;
	_stats.indexCount += 6;
	return true;
}

bool Renderer2D::SubmitText(const text_data &data)
{
	if (data.text.font->texture() == mem::make_dead<gfx::hdl_texture>())
		return false;

	_texFont = data.text.font->texture();

	ui::metrics metrics = data.text.font->GetMetrics();

	glm::mat3 transform;
	{
		glm::mat3 t = glm::translate(glm::mat3(1.0f), data.position);
		glm::mat3 rt = glm::rotate(t, data.rotation);
		transform = glm::scale(rt, data.size);
	}

	math::vec2 position{ .x = 0, .y = 0 };
	f32 fsScale = 1.0f / (metrics.ascenderY - metrics.descenderY);

	u32 length = math::min({ data.text.length, SCAST(u32)(strlen(data.text.string)) });
	for (u32 i = 0; i < length; i++)
	{
		char c = data.text.string[i];

		if (c == '\r') continue;
		if (c == '\n')
		{
			position.x = 0;
			position.y -= fsScale * metrics.lineHeight + data.text.lineHeightOffset;
			continue;
		}

		if (c == '\t')
		{

		}

		ui::glyph glyph = data.text.font->GetGlyph(c);
		if (glyph.advance == 0.0f)
			glyph = data.text.font->GetGlyph('?');
		if (glyph.advance == 0.0f)
		{
			LG_WRN("Font has no ? glyph");
			continue;
		}

		math::rect plane = glyph.planeBounds;
		plane.min *= fsScale;
		plane.max *= fsScale;
		plane.min += position;
		plane.max += position;

		gfx::_Texture *texture = m_libTextures->Get(data.text.font->texture());
		if (!texture)
			return false;

		glm::vec2 texelSize = 1.0f / glm::vec2(texture->dimensions());

		math::rect texCoords = glyph.atlasBounds;
		texCoords.min *= math::vec2{ .x = texelSize.x, .y = texelSize.y };
		texCoords.max *= math::vec2{ .x = texelSize.x, .y = texelSize.y };

		batched_char bc;
		bc.vertexes[0] = {
			.position = transform * glm::vec3(plane.xMin, plane.yMax, 1.f),
			.colour = glm::vec4(1.0f),
			.uv = glm::vec2(texCoords.xMin, texCoords.yMax)
		};
		bc.vertexes[1] = {
			.position = transform * glm::vec3(plane.xMax, plane.yMax, 1.f),
			.colour = glm::vec4(1.0f),
			.uv = glm::vec2(texCoords.xMax, texCoords.yMax)
		};
		bc.vertexes[2] = {
			.position = transform * glm::vec3(plane.xMax, plane.yMin, 1.f),
			.colour = glm::vec4(1.0f),
			.uv = glm::vec2(texCoords.xMax, texCoords.yMin)
		};
		bc.vertexes[3] = {
			.position = transform * glm::vec3(plane.xMin, plane.yMin, 1.f),
			.colour = glm::vec4(1.0f),
			.uv = glm::vec2(texCoords.xMin, texCoords.yMin)
		};

		_textBatch.push(bc);
		_stats.trisDrawn += 2;
		_stats.vertexCount += 4;
		_stats.indexCount += 6;

		if (i < length - 1)
		{
			f32 advance = data.text.font->GetAdvance(c, data.text.string[i + 1]);
			position.x += fsScale * advance + data.text.kerningOffset;
		}
	}

	return true;
}

};

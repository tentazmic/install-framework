#!/usr/bin/python

import sys

__USAGE = f"""\
{sys.argv[0]} <SUBCOMMAND> [OPTIONS]

===== SUBCOMMANDs =====

base        | b  <string[]>         Compile the provided base projects
installs    | i  <string[]>         Compile the provided installs
externals   | x  <string[]>         Compile the provided externals
test        | t  <glob>             Run the matched tests
template    | p  <string> <string>  Create a file/folder structure from a template

config      | c  <OPTIONS>          Save options to be assumed in future runs

=====   OPTIONS   =====

-Help | -h | ?                      Prints this help dialogue
                                    Use with a subcommand to print its help dialogue

-Processes | -p  <integer>          The max number of processes to use
                                        {numproc}
-Timer     | -t                     Time the command
                                        {timer}
-Verbose                            Prints more information about whats going on
                                        {verbose}
-Chime     | -a  <always|a , failure|f , never|n>
                                    What situations the build chimes in
                                        {chime}

--- base ---
-Run       | -r                     Run the result of the build after
                                        {run}

--- base, installs, externals ---
-Clean     | -c  <none|n, shallow|s , deep|d>
                                    How we should use previously built objects
                                    - none   : Use them all
                                    - shallow: Rebuild the objects
                                    - deep   : Delete the relevant 00Source folder then build
                                        {clean}
"""

__NAME = '$name$'
__INSTALL_EXTENSION = '$install_extension$'

def read_arguments():
    args = sys.argv[1:]
    if len(args) == 0:
        print(__USAGE)
        exit(1)

if __name__ == '__main__':
    pass


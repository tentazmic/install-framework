#include "pch.h"
#include "asset_explorer.hpp"
#include "edt/GUI/file_explorer.hpp"
#include "edt/Assets/scene.hpp"
#include "edt/editor.hpp"
#include "Util/string.hpp"

namespace edt::panel::asset_explorer {

namespace {

	bool IsAncestorOfSelected(const arr::list<edt::asset::file_entry>& list, size_t index, size_t selected)
	{
		if (list[selected].parent == edt::asset::file_entry::NONE)
			return false;
		if (list[selected].parent == index)
			return true;
		return IsAncestorOfSelected(list, index, list[selected].parent);
	}

	void OpenAncestorDirectories(const edt::asset::directory_tree& tree, arr::bitset openDirectories, size_t index)
	{
		u16 parent = tree[index].parent;
		while (parent != edt::asset::file_entry::NONE)
		{
			openDirectories[parent] = true;
			parent = tree[parent].parent;
		}
	}
};

PnlAssetExplorer::PnlAssetExplorer(const fs::path& directory)
	: _watcher(directory, 30), _activeEntries(arr::HeapBitset(30)), _selection{0}, _directoryContentsSelection{0}
{ }

void PnlAssetExplorer::GUI()
{
	bool wider = _size.x > _size.y;
	ImVec2 ratios(1, 0.15f);
	if (wider)
	{
		float tmp = ratios.x;
		ratios.x = ratios.y;
		ratios.y = tmp;
	}

	const edt::asset::directory_tree tree = _watcher.tree();
	if (ImGui::BeginChild("Directories", ImVec2(ratios.x * _size.x, ratios.y * _size.y), true))
	{
		edt::gui::explorer_selection selection = edt::gui::DrawFileExplorer(tree, _selection, _activeEntries.get(), edt::gui::explorer_filter{ .onlyDirectories = true });
		if (selection.index != tree.count())
		{
			_selection = selection.index;
		}
	} ImGui::EndChild();

	ratios.x = 1.0f - ratios.x;
	ratios.y = 1.0f - ratios.y;
	if (wider) ImGui::SameLine();

	if (ImGui::BeginChild("Entries", ImVec2(ratios.x * _size.x, ratios.y * _size.y), true))
	{
		edt::gui::explorer_selection selection = edt::gui::DrawDirectoryContents(tree, _selection, _directoryContentsSelection, edt::gui::contents_config{}, edt::gui::explorer_filter{ });
		if (selection.index != tree.count())
		{
			_directoryContentsSelection = selection.index;

			if (tree[_directoryContentsSelection].isDirectory)
			{
				OpenAncestorDirectories(tree, _activeEntries.get(), _directoryContentsSelection);
				_selection = selection.index;
			}

			if (selection.byDoubleSelection && !tree[_directoryContentsSelection].isDirectory && str::EndsWith(tree[_directoryContentsSelection].name, ".scn"))
			{
				fs::path fullpath = _watcher.GetFullPath(_directoryContentsSelection);
				edt::asset::scene *scene = edt::asset::LoadScene(fullpath);
				if (scene->world)
					clbk::Invoke<void, const edt::asset::scene&>(editor::e_OpenScene, *scene);
			}
		}
	} ImGui::EndChild();
}

};

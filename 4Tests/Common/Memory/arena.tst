#include "pch.h"
#include "core"
#include "Memory/arena.hpp"

$ test linear alloc
{
	byte area[128];
	mem::stack_area sarea(area, &area[127]);
	mem::Arena<mem::linear_allocator, mem::no_bounds_checking> arena(sarea);

	char *hello = (char *)arena.Allocate(strlen("Hello World!"), 0);
	strcpy(hello, "Hello World!");

	IS(strcmp((char *)arena.start(), "Hello World!"), 0)
}

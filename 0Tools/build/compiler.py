import os
import sys
import glob
import shutil
import subprocess
import multiprocessing

import build.build_unit as bdut
import reflection.reflection as refl
import reflection.ppprocess as pppr
import util.settings as stts

from util.timer import Timer

def get_object_file_name(path: str) -> str:
	dir, name = os.path.split(path)
	qualifier = ''
	for i, d in enumerate(dir.split(os.sep)):
		qualifier += d[:i]
	return f'{qualifier}+{name}.o'

def is_file_dirty(build_folder: str, path: str) -> bool:
	object_file = f'{stts.DIR_BUILD}/{build_folder}/' \
		+ get_object_file_name(path)
	if not os.path.exists(object_file):
		return True

	# If the source file is more recent, the file is dirty
	return os.path.getmtime(path) > os.path.getmtime(object_file)

def update_file_dirtiness(build_folder: str, file_dirtiness: dict):
	for f in file_dirtiness:
		if file_dirtiness[f]:
			continue

		file_dirtiness[f] = is_file_dirty(build_folder, f)

class CompilerInputs:
	def __init__(self, includes=[], macros=[], libraries=[], options=[]):
		self.includes = includes
		self.macros = macros
		self.libraries = libraries
		self.options = options

	def __str__(self):
		return f'Includes {self.includes}, Macros {self.macros}, ' +\
			f'Libraries {self.libraries}, Options {self.options}'

	def add(self, o):
		self.includes = list(set(self.includes + o.includes))
		self.macros = list(set(self.macros + o.macros))
		self.options = list(set(self.options + o.options))

		for l in o.libraries:
			for i in self.libraries:
				if l == i:
					break
			else:
				self.libraries.append(l)

	def copy(self):
		return CompilerInputs(
			includes=self.includes.copy(),
			macros=self.macros.copy(),
			libraries=self.libraries.copy(),
			options=self.options.copy(),
		)

	def cleanup(self):
		self.includes = list(set(self.includes))
		self.macros = list(set(self.macros))
		self.options = list(set(self.options))

		seen = set([ '' ])
		old = self.libraries
		self.libraries = []
		for l in old:
			if not l in seen:
				self.libraries.append(l)
				seen.add(l)

def get_compiler_inputs(settings: stts.BuildSettings, obj) -> CompilerInputs:
	inps = CompilerInputs()

	if not settings.target_exe and hasattr(obj, 'install_folders'):
		inps.includes += [ stts.DIR_SOURCE + f for f in obj.install_folders ]
	elif hasattr(obj, 'folders'):
		inps.includes += [ stts.DIR_SOURCE + f for f in obj.folders ]

	if not settings.target_exe and hasattr(obj, 'shared'):
		inps.includes += [ stts.DIR_SOURCE + s for s in obj.shared ]

	if hasattr(obj, 'includes'):
		inps.includes.extend(obj.includes)

	if not settings.target_exe and hasattr(obj, 'install_macros'):
		inps.macros = obj.install_macros.copy()
	elif hasattr(obj, 'macros'):
		inps.macros = obj.macros.copy()

	# The libraries in LIBRARIES would be LIB_NAME
	# dependencies so we add them second
	if hasattr(obj, 'lib_names') and len(obj.lib_names) > 0:
		inps.libraries.extend(obj.lib_names)

	if hasattr(obj, 'libraries'):
		inps.libraries = obj.libraries.copy()

	if hasattr(obj, 'options'):
		inps.options = obj.options.copy()

	dependencies = []
	if hasattr(obj, 'dependencies'):
		dependencies += obj.dependencies

	if hasattr(obj, 'externals'):
		dependencies += obj.externals

	if isinstance(obj, bdut.GraphicsAPI):
		dependencies.append(bdut.M_RENDERER)

	for dep in dependencies:
		inps.add(get_compiler_inputs(settings, dep))

	return inps

def alert_subprocess_results(command: str, out: subprocess.CompletedProcess,
                             verbosity_level: int, build_chime: int):
	play_audio(out.returncode == 0, build_chime)

	if out.returncode != 0 or verbosity_level >= 2:
		print(command)

	if out.stdout != None and (len(out.stdout) > 0 and verbosity_level >= 1):
		print(out.stdout.decode('utf-8'))

	if out.stderr != None and len(out.stderr) > 0:
		print(out.stderr.decode('utf-8'))

def slave(file: str, command: str) -> (str, subprocess.CompletedProcess):
	out = subprocess.run(command, capture_output=True, shell=True)
	return (file, command, out)

def parallel_compile(name: str, work: list, settings: stts.BuildSettings) -> int:
	print(f'Compiling {len(work)} files')
	if settings.verbosity_level >= 1:
		for i, (file, _) in enumerate(work):
			print(f'{i} {file}')

	if settings.use_timing:
		timer = Timer()
		timer.start()

	returncode = 0
	with multiprocessing.Pool(processes=settings.num_processes) as pool:
		for i, (_, cmd, out) in enumerate(pool.starmap(slave, work)):
			returncode += out.returncode
			alert_subprocess_results(cmd, out,
			                         settings.verbosity_level, stts.CHIME_NEVER)

			if i != len(work) and (len(out.stdout) > 0 or len(out.stderr) > 0):
				print('=====================')

	if settings.use_timing:
		timer.stop()
		print(f'{name} Compile Time {timer.print()}')

	play_audio(returncode == 0, settings.build_chime)
	return returncode



TU_COMPILE_COMMAND = shutil.which('g++') +\
	' -std=c++1z -c {inp} -o {out}'

BUILD_EXE_COMMAND = shutil.which('g++') + ' -o {out}'

BUILD_STATIC_LIB_COMMAND = shutil.which('ar') + ' rvs {lib_name} {files}'

match sys.platform:
	case 'win32':
		__shared_flags = ' -shared'
	case 'linux' | 'linux2':
		__shared_flags = ' -shared -rdynamic'
	case _:
		print(f'Unknown platform {sys.platform}')
		exit(1)

BUILD_SHARED_LIB_COMMAND = shutil.which('g++') + __shared_flags +\
	' -std=c++1z -o {lib_name} {objs}'

def compile_files(settings: stts.BuildSettings, build_step: str, files: dict,
                  cinputs: CompilerInputs)\
                  -> (list, int):
	cinputs.cleanup()
	includes = ' '.join([ '-I' + i for i in cinputs.includes ])
	macros = ' '.join([ '-D' + m for m in cinputs.macros ])
	options = ' '.join(cinputs.options)

	command = TU_COMPILE_COMMAND
	if not settings.target_exe:
		command += ' -fPIC'
	command += f' {includes} {macros} {options}'

	update_file_dirtiness(settings.build_name, files)
	in_out = [ (
		os.path.abspath(f),
		os.path.abspath(f'{stts.DIR_BUILD}/{settings.build_name}/{get_object_file_name(f)}'),
		files[f]
		) for f in files ]
	work = [
		(inp, command.format(inp=inp, out=out))
		for inp, out, is_dirty in in_out if is_dirty
	]

	if len(work) == 0:
		if settings.verbosity_level >= 2:
			print(f'{build_step}: Nothing to compile')
		return [ i[1] for i in in_out ], 0

	if not os.path.exists(f'{stts.DIR_BUILD}/{settings.build_name}'):
		os.makedirs(f'{stts.DIR_BUILD}/{settings.build_name}')

	print(f'{build_step}: ', end='')
	returncode = parallel_compile(build_step, work, settings)

	return [ i[1] for i in in_out ], returncode

def link_executable(settings: stts.BuildSettings, objs: list, cinputs: CompilerInputs):
	cinputs.cleanup()
	inputs = ' '.join(objs)
	libraries = ' '.join([ '-l' + l for l in cinputs.libraries ])

	if not os.path.exists(stts.DIR_EXEC):
		os.makedirs(stts.DIR_EXEC)

	command = BUILD_EXE_COMMAND.format(out=stts.DIR_EXEC + settings.exe_name)
	# Library stuff must come last
	command = f'{command} {inputs} -L' + os.path.abspath(stts.DIR_LIBS) + ' ' + libraries

	# Capture output so that in the case of failure we can print
	# the output after printing the command
	out = subprocess.run(command, capture_output=True, shell=True)
	alert_subprocess_results(command, out, settings.verbosity_level, settings.build_chime)

def link_static_library(settings: stts.BuildSettings, lib_name: str, objs: list) -> int:
	command = BUILD_STATIC_LIB_COMMAND.format(
		lib_name=f'{stts.DIR_LIBS}/{lib_name}', files=' '.join(objs)
	)

	if not os.path.exists(stts.DIR_LIBS):
		os.makedirs(stts.DIR_LIBS)

	out = subprocess.run(command, capture_output=True, shell=True)
	alert_subprocess_results(command, out, settings.verbosity_level, settings.build_chime)

	return out.returncode

def link_shared_library(settings: stts.BuildSettings,
                        objs: list, cinputs: CompilerInputs) -> int:

	cinputs.cleanup()
	inputs = ' '.join(objs)
	libraries = ' '.join([ '-l' + l for l in cinputs.libraries ])

	command = BUILD_SHARED_LIB_COMMAND.format(
		lib_name=settings.exe_name, objs=' '.join(objs)
	)
	command +=  f' -L{os.path.abspath(stts.DIR_LIBS)} {libraries}'

	out = subprocess.run(command, capture_output=True, shell=True)
	alert_subprocess_results(command, out, settings.verbosity_level, settings.build_chime)

	return out.returncode


def run_output(settings: stts.BuildSettings):
	exe = os.path.abspath(stts.DIR_EXEC + settings.exe_name)
	subprocess.run(exe)


##########################
# CONVENIENCE FUNCTIONS
##########################

'''
Functions to shorten the process of compiling build units

BUILD_NAME is the folder under 00Build/ where the
compiled files go
'''

def get_files(folder: str, extensions: tuple) -> list:
	files = []
	for ext in extensions:
		files += glob.glob(f'{folder}/**/*{ext}', recursive=True)

	return files

def prep_and_compile(build_step: str, files: list, settings: stts.BuildSettings,
             context: dict, cinputs: CompilerInputs, file_statuses: dict)\
             -> (list, dict, int):
	'''
	PPProcesses and compiles the passed in files, according to the compiler inputs
	Returns the compiled obj files, the update file statuses, and the compilation
	return code
	'''
	if settings.verbosity_level >= 2:
		print("Considering Files")
		for f in files:
			print(f)

	dirties = pppr.execute(files, context, redo_all=settings.use_clean_slate)
	file_statuses.update(dict(
		[ (pppr.get_processed_file(file), file in dirties) for file in files ]
	))
	file_statuses = refl.propogate_dirtiness(file_statuses, cinputs.includes, cinputs.macros)

	src_files = [
		os.path.normpath(pppr.get_processed_file(file))
		for file in files if file.endswith('.cpp')
	]
	units_to_compile = dict(
		[ (file, file_statuses[file]) for file in src_files ]
	)

	if settings.verbosity_level >= 1:
		if len(dirties) == 0:
			print('Clean')
		else:
			print('Found Dirties:')
			for d in [ c for c in units_to_compile if units_to_compile[c] ]:
				print(d)

	objs, returncode = compile_files(settings, build_step, units_to_compile, cinputs)
	return objs, file_statuses, returncode

def compile_common(settings: stts.BuildSettings, profile: bdut.Profile, context=dict(),
                   cinputs=CompilerInputs())\
	                 -> (CompilerInputs, list, dict, int):
	if settings.verbosity_level >= 1:
		print('Build Common')

	for unit in [ bdut.COMMON, profile ]:
		cinputs.add(get_compiler_inputs(settings, unit))
	cinputs.includes.append(stts.DIR_COMMON + '_Includes') # For pch.h

	files = get_files(stts.DIR_COMMON, ('.[hic]pp', '.cmp', '.sys'))
	files += [
		i for i in get_files(stts.DIR_COMMON + '_Includes', ('', ))
		if os.path.isfile(i)
	]

	if profile.debug:
		cinputs.add(get_compiler_inputs(settings, bdut.DEBUG))
		for folder in bdut.DEBUG.folders:
			files += get_files(folder, ('.[hic]pp', ))

	obj_files, file_statuses, returncode =\
		prep_and_compile('Common', files, settings, context, cinputs, dict())

	return cinputs, obj_files, file_statuses, returncode

def compile_build_unit(settings: stts.BuildSettings, obj,
	                     context: dict, cinputs: CompilerInputs, file_statuses: dict)\
	                     -> (CompilerInputs, list, dict, int):
	dep_objs = []
	my_inputs = cinputs.copy()
	if hasattr(obj, 'dependencies'):
		for dep in obj.dependencies:
			comp, objs, statuses, returncode =\
			  compile_build_unit(settings, dep, context, cinputs, file_statuses)
			if returncode != 0:
				return cinputs, [], file_statuses, returncode
			my_inputs.add(comp)
			dep_objs += objs
			file_statuses.update(statuses)

	if settings.verbosity_level >= 1:
		print('Compiling Unit ' + obj.name)

	obj_inputs = get_compiler_inputs(settings, obj)
	cinputs.macros.extend(obj_inputs.macros)
	cinputs.libraries.extend(obj_inputs.libraries)
	my_inputs.add(obj_inputs)

	if not settings.target_exe and hasattr(obj, 'install_folders'):
		folders = obj.install_folders
	elif hasattr(obj, 'folders'):
		folders = obj.folders
	else:
		print(f'Build Unit {obj} has no folders')
		return cinputs, [], file_statuses, 0

	files = []
	for f in folders:
		files += get_files(f, ('.[hic]pp', '.cmp', '.sys'))

	obj_files, file_statuses, returncode = prep_and_compile(
		obj.name, files, settings, context, my_inputs, file_statuses
	)

	return my_inputs, obj_files, file_statuses, returncode

def compile_exe(cinputs: CompilerInputs,
                settings: stts.BuildSettings, profile: bdut.Profile,
                platform: bdut.Platform, build_units: list)\
                -> (CompilerInputs, list, dict, dict, int):
	'''
	A helper function for compiling all files relating to building executables,
	i.e. the game and the editor.
	Compiles Common, the passed in platform and modules accordinng to the Profile
	passed in.
	Returns the compiler inputs, the compiled object code, the ppprocess context dictionary,
	the file statuses and the return code
	'''
	components = []
	for f in glob.glob('Framework/Common/**/*.cmp', recursive=True):
		with open(f, 'rb') as input:
			comps = refl.read_components(input)
		components += comps

	context = { 'COMPONENTS': components, 'root_folder': 'Framework/Common' }

	settings.target_exe=True
	if settings.build_chime == stts.CHIME_ALWAYS:
		settings.build_chime = stts.CHIME_FAIL

	cinputs, all_objs, statuses, returncode =\
		compile_common(settings, profile, context, cinputs)

	if returncode != 0:
		return cinputs, all_objs, dict(), statuses, returncode

	cinputs.macros.append(f'"PRJ_NAME=\\"{settings.app_name}\\""')

	for bu in build_units:
		_, objs, stats, rc =\
			compile_build_unit(settings, bu, context, cinputs, statuses)
		all_objs += objs
		statuses.update(stats)

		if returncode != 0:
			returncode = rc

	_, objs, stats, rc =\
		compile_build_unit(settings, platform, context, cinputs, statuses)
	all_objs += objs
	statuses.update(stats)

	returncode += rc

	return cinputs, all_objs, context, statuses, returncode


#################
### AUDIO
#################

def play_audio(result: bool, play_sound: int):
	py = shutil.which('python')
	command = [ py, '0Tools/util/chime.py', ' ' if result else '', str(play_sound) ]
	match sys.platform:
		case 'win32':
			subprocess.Popen(command, creationflags=subprocess.DETACHED_PROCESS)
		case 'linux' | 'linux2':
			subprocess.Popen(command, start_new_session=True,
				stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
			)

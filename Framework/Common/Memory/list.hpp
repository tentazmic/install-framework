#pragma once

#include "Util/list.hpp"
#include "arena.hpp"

namespace arr {

template<typename T, class AllocatorPolicy, class BoundsCheckerPolicy>
struct arena_list
{
	using access_type = typename list<T>::access_type;
	using pop_type = typename list<T>::pop_type;
	using iter = typename list<T>::iter;
	using citer = typename list<T>::citer;
	using ARENA = mem::Arena<AllocatorPolicy, BoundsCheckerPolicy>;

	arena_list() : _arena(), _list() { }
	arena_list(ARENA *arena, size_t capacity, size_t alignment, const char *file = nullptr, size_t line = 0)
		: _arena{ arena }
	{ initialise(capacity, alignment, file, line); }

	arena_list(const arena_list& other) = delete;
	arena_list(arena_list&& other) = default;

	~arena_list()
	{
		if (!_list.data())
			return;

		FOR (it, _list)
		{
if constexpr (std::is_pointer_v<T>) {
			delete *it;
} else if constexpr (!std::is_pod_v<T>) {
			it->~T();
}
		}

		// Need some way to get file and line info
		// into a destructor
		Free(nullptr, 0);
	}

	arena_list& operator =(const arena_list& other) = delete;
	arena_list& operator =(arena_list&& other) = default;

	void initialise(size_t capacity, size_t alignment, const char *file = nullptr, size_t line = 0)
	{
		Free(file, line);

		if (capacity == 0)
		{
			_list = list<T>();
			return;
		}

		const size_t kSize = sizeof(size_t) + sizeof(T) * capacity;
		size_t *data = (size_t *)_arena->Allocate(kSize, alignment, file, line);
		if (data)
		{
			size_t *count = data;
			*count = 0;
			_list = list<T>(RCAST(T*)(data + 1), count, capacity);
			return;
		}
		LG_ERR("Failed Allocation of {}B", kSize);
		_list = list<T>();
	}

	inline list<T> get() const { return _list; }

	INLINE void push(access_type item) { _list.push(item); }
	INLINE pop_type pop() { return _list.pop(); }
	INLINE void insert(size_t index, access_type item) { _list.insert(index, item); }
	template<class... Args>
	INLINE void emplace(Args&&... args) { _list.emplace(std::forward<Args>(args)...); }

	template<bool is_const>
	INLINE void remove(const iterator<is_const, T>& it) { _list.remove(it); }
	INLINE void remove(size_t index) { _list.remove(index); }
	INLINE void clear() { _list.clear(); }
	INLINE void fill(access_type value) { _list.fill(value); }

	INLINE T& at(size_t index) { return _list.at(index); }
	INLINE access_type at(size_t index) const { return _list.at(index); }

	INLINE T& operator [](size_t index) { return at(index); }
	INLINE access_type operator [](size_t index) const { return at(index); }

	INLINE iter find(access_type other) { return _list.find(other); }
	INLINE citer find(access_type other) const { return _list.find(other); }

	INLINE iter begin() { return _list.begin(); }
	INLINE iter end() { return _list.end(); }

	INLINE iter rbegin() { return _list.rbegin(); }
	INLINE iter rend() { return _list.rend(); }

	INLINE citer cbegin() const { return _list.cbegin(); }
	INLINE citer cend() const { return _list.cend(); }

	INLINE citer rcbegin() const { return _list.rcbegin(); }
	INLINE citer rcend() const { return _list.rcend(); }

	INLINE T& front() { return _list.front(); }
	INLINE T& back() { return _list.back(); }
	INLINE access_type front() const { return _list.front(); }
	INLINE access_type back() const { return _list.back(); }

	INLINE bool full() const { return _list.full(); }
	INLINE bool empty() const { return _list.empty(); }

	INLINE T * data() { return _list.data(); }
	INLINE size_t count() const { return _list.count(); }
	INLINE size_t capacity() const { return _list.capacity(); }

private:
	void Free(const char *file, size_t line)
	{
		if (_list.data())
		{
			size_t *start = RCAST(size_t *)(_list.data()) - 1;
			_arena->Free(start, file, line);
		}
	}

	ARENA *_arena;
	list<T> _list;
};
};

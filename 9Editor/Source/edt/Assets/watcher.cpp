#include "pch.h"

#include "watcher.hpp"

namespace edt::asset {

void FillDirectoryItems(const fs::path& root, directory_tree& tree, bool trimBeginning, u16 parent)
{
	file_entry entry;
	entry.parent = parent;
	entry.isDirectory = fs::is_directory(root);
	entry.lastWriteTime = fs::directory_entry(root).last_write_time();

	fs::path pathName = trimBeginning ? root.filename() : root;

	char *name = (char *)malloc(pathName.string().size() + 1);
	strcpy(name, pathName.string().c_str());
	entry.name = name;

	u16 index = tree.count();
	tree.push(entry);

	if (!entry.isDirectory) return;

	for (const auto& entry : fs::directory_iterator(root))
	{
		FillDirectoryItems(entry.path(), tree, true, index);
	}
}

bool DirectoryHasChildren(const directory_tree& tree, size_t iDirectory)
{
	if (!tree[iDirectory].isDirectory)
	{
		LG_WRN("Called on a file {}", tree[iDirectory].name);
		return false;
	}

	for (size_t i = iDirectory + 1; i < tree.count(); i++)
		if (tree[i].parent == iDirectory)
			return true;

	return false;
}

bool DirectoryHasChildDirectories(const directory_tree& tree, size_t iDirectory)
{
	if (!tree[iDirectory].isDirectory)
	{
		LG_WRN("Called on a file {}", tree[iDirectory].name);
		return false;
	}

	for (size_t i = iDirectory + 1; i < tree.count(); i++)
		if (tree[i].parent == iDirectory && tree[i].isDirectory)
			return true;

	return false;
}

watcher::watcher(const fs::path& root, size_t capacity)
	: _root(root), _isWatchingDirectory{false}, _files()
{
	if (!fs::exists(_root))
	{
		LG_ERR("Path does not exist: {}", _root);
		return;
	}
	_isWatchingDirectory = fs::is_directory(_root);
	_files = arr::dync_list<file_entry>(_isWatchingDirectory ? capacity : 1);

	arr::list<file_entry> rep = _files.get();
	FillDirectoryItems(root, rep);
}


size_t watcher::GetIndexFromFilePath(const fs::path& path) const
{
	arr::dync_list<file_entry>::citer begin = _files.cbegin();

	for (auto it : path)
	{
		auto pos = std::find_if(begin, _files.cend(), [&](const file_entry& e) { return strcmp(e.name, it.string().c_str()) == 0; });
		if (pos == _files.cend())
			return MAX(u16);

		begin = pos;
	}

	return begin - _files.cbegin();
}

fs::path watcher::GetFullPath(size_t idxEntry)
{
	fs::path path = _files[idxEntry].name;
	size_t index = idxEntry;
	while (_files[index].parent != MAX(u16))
	{
		index = _files[index].parent;
		path = _files[index].name / path;
	}

	return path;
}

// void watcher::Tick()
// {
	
// }
};

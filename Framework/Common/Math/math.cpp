#include "pch.h"

#include "math.hpp"

namespace math {
uvec2 uvec2::zero = { 0, 0 };

glm::mat4 CalculateModel2D(const glm::vec2& position, float rotation, const glm::vec2& scale)
{
	glm::mat4 translation = glm::translate(
		glm::mat4(1.0f), glm::vec3(position, 0.0f)
	);
	glm::mat4 rotationTranslation = glm::rotate(
		translation, rotation, glm::vec3(0, 0, 1)
	);
	return glm::scale(
		rotationTranslation, glm::vec3(scale, 1.0f)
	);
}
};

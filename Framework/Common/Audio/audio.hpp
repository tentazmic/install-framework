#pragma once

#include "core"
#include "Memory/generations.hpp"

// TODO This should be a module

namespace snd {

// Sample
//     A single segment of audio from a channel
// Stereo Sample
//     A synchronised segment of audio from all channels

using sample = i16;

struct buffer
{
	sample *left;
	sample *right;
	size_t length;
};

u32 GetSamplesPerSecond();
};

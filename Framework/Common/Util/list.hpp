#pragma once

#include "debug"
#include "magic.hpp"

// list        A memory space inspecific list, cannot be initialised normaly
// sttc_list  A stack list
// dync_list   A heap list, as the data is freed in the destuctor, you cannot copy this

namespace arr {

template<bool is_const, typename T>
struct iterator
{
	using iterator_category = std::random_access_iterator_tag;
	using difference_type = ptrdiff_t;
	using value_type = std::conditional_t<is_const, const T, T>;
	using pointer    = value_type*;
	using reference  = value_type&;

	using ptr_return = std::conditional_t<std::is_pointer_v<T>, value_type, value_type*>;

	using self = iterator<is_const, T>;
	iterator(pointer p) : ptr(p) { }
	template<bool _const>
	iterator(const iterator<_const, T>& other)
	{
if constexpr (!is_const && _const) {
	LG_ERR("Tried to initialise a non const iterator with a const iterator");
} else {
		ptr = other.ptr;
}
	}

	template<bool _const>
	self& operator =(const iterator<_const, T>& other)
	{
if constexpr (!is_const && _const) {
	LG_ERR("Tried to initialise a non const iterator with a const iterator");
} else {
		ptr = other.ptr;
}
		return *this;
	}

	reference operator *() { return *ptr; }
	ptr_return operator ->()
	{
if constexpr (std::is_pointer_v<T>) {
		return *ptr;
} else {
		return ptr;
}
	}

	/* prefix */  self& operator ++() { ptr++; return *this; }
	/* postfix */ self  operator ++(int) { self tmp = *this; operator++(); return tmp; }
	/* prefix */  self& operator --() { ptr--; return *this; }
	/* postfix */ self  operator --(int) { self tmp = *this; operator--(); return tmp; }

		self& operator +=(size_t i) { ptr += i; return *this; }
		friend self operator +(const self& iter, size_t i) { return self(iter.ptr + i); }
		friend self operator +(size_t i, const self& iter) { return self(iter.ptr + i); }

		self& operator -=(size_t i) { ptr -= i; return *this; }
		friend self operator -(const self& iter, size_t i) { return self(iter.ptr - i); }
		friend difference_type operator -(const self& a, const self& b) { return a.ptr - b.ptr; }

		self operator [](int i) const { self iter = *this; iter += i; return iter; }

	pointer ptr;
};

#define OP_OVERLOAD(OP) template<typename T, bool A, bool B> \
bool operator OP(const iterator<A, T>& a, const iterator<B, T>& b)

OP_OVERLOAD(==) { return a.ptr == b.ptr; }
OP_OVERLOAD(!=) { return !(a == b); }
OP_OVERLOAD(>)  { return a.ptr > b.ptr; }
OP_OVERLOAD(>=) { return a > b && a == b; }
OP_OVERLOAD(<)  { return !(a > b) && !(a == b); }
OP_OVERLOAD(<=) { return !(a > b); }
#undef OP_OVERLOAD

template<typename T> struct dync_list;

template<typename T>
struct list
{
	// Some types may be smaller than a pointer
	// If we're doing a const array access it will be better
	// to just copy the object rather than return a refernce
	using access_type = std::conditional_t<sizeof(T) <= sizeof(void*), T, const T&>;
	using pop_type =
		std::conditional_t<std::is_move_constructible_v<T>, T&&,
			std::conditional_t<std::is_copy_constructible_v<T>, T, T*>>;
	using  iter = iterator<false, T>;
	using citer = iterator<true, T>;

	list() : _data{nullptr}, _count{nullptr}, _capacity{0} { }
	list(T *data, size_t *count, size_t capacity)
		: _data{data}, _count{count}, _capacity{capacity}
	{ }

	list(const list& other)
		: _data{other._data}, _count{other._count},
		  _capacity{other._capacity}
	{ }

	list(list&& other)
		: _data{other._data}, _count{other._count},
		  _capacity{other._capacity}
	{
		MOVE_CONSTRUCTOR_CHECK
		other._data = nullptr;
		other._count = nullptr;
		other._capacity = 0;
	}

	list& operator =(const list& other)
	{
		_data = other._data;
		_count = other._count;
		_capacity = other._capacity;
		return *this;
	}

	list& operator =(list&& other)
	{
		MOVE_CHECK
		_data = other._data;
		_count = other._count;
		_capacity = other._capacity;
		other._data = nullptr;
		other._count = nullptr;
		other._capacity = 0;
		return *this;
	}

#define COUNT_PTR
#include "_list_funcs.ipp"
#undef COUNT_PTR

	friend struct dync_list<T>;

private:
	void clear_pointers() { _data = nullptr; _count = nullptr; }

	T *_data;
	size_t *_count;
	size_t _capacity;
};

template<typename T, size_t CAPACITY>
struct sttc_list
{
	using access_type = typename list<T>::access_type;
	using pop_type = typename list<T>::pop_type;
	using  iter = typename list<T>::iter;
	using citer = typename list<T>::citer;

	sttc_list() : _count{0} {}
	sttc_list(std::initializer_list<T> l) : _count{0}
	{
		for (auto it : l)
		{
			if (_count == CAPACITY)
			{
				LG_ERR("Init List is Too Large STACK_LIST {} INIT LIST {}", CAPACITY, l.size());
				return;
			}
			_data[_count] = it;
			_count++;
		}
	}

	sttc_list(const list<T>& other) : _count{0}
	{
		size_t max = other.count() > CAPACITY ? CAPACITY : other.count();
		for (size_t i = 0; i < max; i++)
			push(other[i]);
	}

	sttc_list(const sttc_list& other) : _count(other._count)
	{
		memcpy(_data, other._data, sizeof(T) * _count);
	}

	sttc_list(sttc_list&& other) : _count(other._count)
	{
		MOVE_CONSTRUCTOR_CHECK
		memcpy(_data, other._data, sizeof(T) * _count);
		other.clear();
	}

	~sttc_list()
	{
if constexpr (std::is_function_v<T>) {
	for (iter it = begin(); it != end(); it++)
	{
	if constexpr (std::is_pointer_v<T>) {
		delete *it;
	} else if constexpr (!std::is_pod_v<T>) {
		it->~T();
	}
	}
}
	}

	sttc_list& operator =(const sttc_list& other)
	{
		_count = other._count;
		memcpy(_data, other._data, sizeof(T) * _count);
		return *this;
	}

	sttc_list& operator =(sttc_list&& other)
	{
		MOVE_CHECK
		_count = other._count;
		memcpy(_data, other._data, sizeof(T) * _count);
		other.clear();
		return *this;
	}

	inline list<T> get() const { return list<T>(CCAST(T*)(_data), CCAST(size_t*)(&_count), CAPACITY); }

#define _capacity CAPACITY
#include "_list_funcs.ipp"
#undef _capacity

private:
	T _data[CAPACITY];
	size_t _count;
};

template<typename T>
struct dync_list
{
	using access_type = typename list<T>::access_type;
	using pop_type = typename list<T>::pop_type;
	using iter = typename list<T>::iter;
	using citer = typename list<T>::citer;

	dync_list() : _list() { }
	dync_list(size_t capacity) { initialise(capacity); }

	dync_list(const dync_list& other) = delete;
	dync_list(dync_list&& other) = default;

	~dync_list() { Free(); }

	dync_list& operator =(const dync_list& other) = delete;
	dync_list& operator =(dync_list&& other) = default;

	void initialise(size_t capacity)
	{
		Free();

		const size_t kSize = sizeof(size_t) + sizeof(T) * capacity;
		size_t *data = (size_t *)malloc(kSize);
		if (data)
		{
			size_t *count = data;
			*count = 0;
			_list = list<T>(RCAST(T*)(data + 1), count, capacity);
			return;
		}
		LG_ERR("Failed Allocation of {}B", kSize);
		_list = list<T>();
	}

	inline list<T> get() const { return _list; }

	INLINE void push(access_type item) { _list.push(item); }
	INLINE pop_type pop() { return _list.pop(); }
	INLINE void insert(size_t index, access_type item) { _list.insert(index, item); }
	template<class... Args>
	INLINE void emplace(Args&&... args) { _list.emplace(std::forward<Args>(args)...); }

	template<bool is_const>
	INLINE void remove(const iterator<is_const, T>& it) { _list.remove(it); }
	INLINE void remove(size_t index) { _list.remove(index); }
	INLINE void clear() { _list.clear(); }
	INLINE void fill(access_type value) { _list.fill(value); }

	INLINE T& at(size_t index) { return _list.at(index); }
	INLINE access_type at(size_t index) const { return _list.at(index); }

	INLINE T& operator [](size_t index) { return at(index); }
	INLINE access_type operator [](size_t index) const { return at(index); }

	INLINE iter find(access_type other) { return _list.find(other); }
	INLINE citer find(access_type other) const { return _list.find(other); }

	INLINE iter begin() { return _list.begin(); }
	INLINE iter end() { return _list.end(); }

	INLINE iter rbegin() { return _list.rbegin(); }
	INLINE iter rend() { return _list.rend(); }

	INLINE citer cbegin() const { return _list.cbegin(); }
	INLINE citer cend() const { return _list.cend(); }

	INLINE citer rcbegin() const { return _list.rcbegin(); }
	INLINE citer rcend() const { return _list.rcend(); }

	INLINE T& front() { return _list.front(); }
	INLINE T& back() { return _list.back(); }
	INLINE access_type front() const { return _list.front(); }
	INLINE access_type back() const { return _list.back(); }

	INLINE bool full() const { return _list.full(); }
	INLINE bool empty() const { return _list.empty(); }

	INLINE T * data() { return _list.data(); }
	INLINE size_t count() const { return _list.count(); }
	INLINE size_t capacity() const { return _list.capacity(); }

private:
	void Free()
	{
		if (!_list.data())
			return;

		size_t *start = RCAST(size_t *)(_list.data()) - 1;
		free(start);
		_list.clear_pointers();
	}

	list<T> _list;
};
};

import sys
from playsound import playsound
import settings as stts

ASSETS = stts.DIR_TOOLS + 'assets/'
SUCCESS = "comp_success.wav"
FAIL = "comp_fail.wav"

def play_chime(success: bool, play_sound: int):
	if play_sound == stts.CHIME_NEVER:
		return

	if success and play_sound != stts.CHIME_FAIL:
		playsound(ASSETS + SUCCESS)
	elif not success:
		playsound(ASSETS + FAIL)

if __name__ == '__main__':
	play_chime(bool(sys.argv[1]), int(sys.argv[2]))

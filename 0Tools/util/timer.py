import time

'''
A simple timer class
'''

class Timer:
	def __init__(self):
		self.start_time = 0.0
		self.elapsed_time = 0.0

	def start(self):
		self.start_time = time.perf_counter()

	def stop(self):
		self.elapsed_time = time.perf_counter() - self.start_time

	def print(self, precision=4) -> str:
		return f'{self.elapsed_time:0.{precision}f} secs'

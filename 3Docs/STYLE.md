# Code Styling

Not hard enforced, just general patterns I've been following.

Favour POD types without heap allocations
POD types are named with snake_case
Namespace name are aimed to be kept at three characters long.

Enum names are written as `EPascalCase`.  
Enum values are written in `SCREAMING_SNAKE_CASE`.

## OOP

Softish rule on only one level of inheritance, mainly for use with
graphics stuff so that we can use them without having to call
the specific type.

The base classes have a prefix of _.

## Class vs Structs

Structs are for simple data containers. All members should be public.
They can have constructors, methods and operators. No inheritance.
Variables should come before all functions.
Member functions should only do small operations.  
Written in `snake_case`.  
Members variables use `camelCase`. Member functions use `snake_case`.

Classes are for more complex data structures.
Constructors, destructors, inheritance, protected and private members.
Functions should come before all variables.  
Written in `PascalCase`.  
Member functions in `PascalCase`.  
Public member variables in `camelCase`.  
Simple getter/setter functions in `snake_case`.  
All virtual functions in `PascalCase`.  
Private variables in `_camelCase`.  
Owned resources (pointers) in `m_camelCase`.  

## Functions

When it is obvious, omit the parameter name in declaration.

#include "pch.h"
#include "events"
#include "debug"

#include "res/assets.hpp"
#include "instl/install.hpp"

const char *res::g_AssetRoot = PRJ_ROOT;
const char *res::g_LocalAssetRoot = LOCAL_PRJ_ROOT;

instl::install_info instl_Information{
	.name = "Sandbox"
};

void instl_Startup(instl::pointers& p)
{
	LG_INIT;
	DBG(instl_Information.name);
}

void instl_Shutdown()
{
	DBG("Leave {}", instl_Information.name);
}

void instl_LoadScene(gfx::_TextureLib *textures, arr::list<game::sprite> &sprites, arr::list<game::camera> &cameras)
{
	gfx::texture tex;
	res::LoadTexture(LocalAssetPath(Textures/sandbox.png), &tex);
	gfx::hdl_texture h = textures->Load(tex);
	res::DeleteTexture(tex);

	game::sprite s1{
		.position = glm::vec2(-3.f, -3.7f),
		.rotation = glm::radians(5.f),
		.moveable = false,
		.texture = h,
		.colour = glm::vec4(1.f, 1.f, 1.f, .5f)
	};
	game::sprite s2{
		.position = glm::vec2(4.f, 4.3f),
		.scale = glm::vec2(2.f, 1.f),
		.rotation = glm::radians(-30.f),
		.moveable = false,
		.colour = glm::vec4(1.f, 0.f, 1.f, .7f)
	};

	sprites.push(s1);
	sprites.push(s2);
}

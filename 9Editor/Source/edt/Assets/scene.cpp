#include "pch.h"
#include "scene.hpp"

#include "edt/editor.hpp"

#include "res/scene.hpp"
#include "rdr/renderer.hpp"
#include "Memory/arena.hpp"
#include "Memory/memory.hpp"

namespace edt::asset {
namespace {
	// using ARENA = mem::Arena<mem::pool_allocator, mem::simple_bounds_checking>;
	// ARENA *ar_Worlds;

	// arr::dync_list<cached_scene> s_CachedScenes;
	std::map<const char *, scene> s_Scenes;

	// void DeleteWorld(ecs::World *world)
	// {
	// 	u16 *slot = RCAST(u16*)(world) - 1;
	// 	DEL(ar_Worlds, slot);
	// }

	// const size_t kSize = sizeof(ecs::World) + sizeof(u16);
	// const size_t kAlignment = 16;
};

void SceneInit()
{
	// mem::heap_area heap(24 * kSize, kSize, kAlignment);
	// ar_Worlds = new ARENA(heap);

	// s_CachedScenes = arr::dync_list<cached_scene>(32);
}

void Shutdown()
{
	s_Scenes.~map();
}

scene * LoadScene(const fs::path& filepath)
{
	{
		auto it = s_Scenes.find(filepath.string().c_str());
		if (it != s_Scenes.end())
		{
			return &it->second;
		}
	}

	::res::asset_roots roots{ PRJ_ROOT, PRJ_ROOT };
	::res::Scene scene = ::res::ReadScene(filepath.string().c_str(), roots);
	if (!scene)
	{
		ED_ERR("Could not load scene at {}", filepath);
		return { };
	}

	char *path = (char *)malloc(filepath.string().size() + 1);
	strncpy(path, filepath.string().c_str(), filepath.string().size() + 1);

	// ::edt::asset::scene s;
	s_Scenes.insert(std::pair{path, ::edt::asset::scene()});
	rdr::twod::render_scene::CreateDefault(editor::CurrentAPI, s_Scenes[path].scene, s_Scenes[path].defaults);
	s_Scenes[path].world = new ecs::World(32, 16, std::string(filepath.string()));

	::res::scene_context ctx{
		.roots = roots,
		.texture = s_Scenes[path].scene.textures
	};
	if (!::res::LoadSceneIntoWorld(scene, *s_Scenes[path].world, ctx))
	{
		ED_ERR("Could not load parsed scene at {} into world", filepath);
		delete s_Scenes[path].world;
		s_Scenes.erase(path);
		// Free libs
		return { };
	}

	return &s_Scenes[path];
}

std::map<const char *, scene>& GetActiveScenes() { return s_Scenes; }
};
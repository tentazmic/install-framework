#pragma once

#include <spdlog/fmt/fmt.h>

namespace inp {

const char * KeyToString(EKey);
const char * MouseButtonToString(EMouseButton);
};

template<typename OStream>
OStream& operator<<(OStream& out, inp::EKey key) noexcept
{
	out << inp::KeyToString(key);
	return out;
}

template<typename OStream>
OStream& operator<<(OStream& out, inp::EMouseButton button) noexcept
{
	out << inp::MouseButtonToString(button);
	return out;
}

template<>
struct fmt::formatter<inp::EKey> : fmt::formatter<std::string>
{
	auto format(inp::EKey k, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), inp::KeyToString(k));
	}
};

template<>
struct fmt::formatter<inp::EMouseButton> : fmt::formatter<std::string>
{
	auto format(inp::EMouseButton m, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), inp::MouseButtonToString(m));
	}
};
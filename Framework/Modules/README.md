# Modules

A place for stuff that may or may not be included in an install.

## Roadmap

AssetFilesystem's header files will need to be moved to a new module when it comes
time to create an asset system that uses cooked assets.

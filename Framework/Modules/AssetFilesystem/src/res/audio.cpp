#include "pch.h"
#include "res/audio.hpp"
#include "Audio/audio.hpp"

#include "debug"

namespace res {

// WAV File Format
// 4 Bytes - "RIFF" File descriptor
// 4 Bytes - Size of the rest of the file (without this field and RIFF)

// 4 Bytes - "WAVE"
// 4 Bytes - "fmt " note the space (SubChunk1ID)
// 4 Bytes - Chunk Size (SubChunk1Size)
// 2 Bytes - Audio Format (1 = PCM)
// 2 Bytes - Num Channels
// 4 Bytes - Sample Rate
// 4 Bytes - Byte Rate
// 2 Bytes - Block Align
// 2 Bytes - BitsPerSample



audio_stream LoadAudioClip(const char *path)
{
	std::ifstream *file = new std::ifstream(path, std::ifstream::binary);
	if (!file->is_open())
	{
		AS_ERR("No audio clip at {}", path);
		return { };
	}

	if (!(file->get() == 'R' && file->get() == 'I' && file->get() == 'F' && file->get() == 'F'))
	{
		return {};
	}

	size_t fileSize = 0;
	fileSize |= (size_t)file->get() << 0;
	fileSize |= (size_t)file->get() << 8;
	fileSize |= (size_t)file->get() << 16;
	fileSize |= (size_t)file->get() << 24;
	fileSize += 8;

	if (!(file->get() == 'W' && file->get() == 'A' && file->get() == 'V' && file->get() == 'E'))
	{
		return {};
	}

	if (!(file->get() == 'f' && file->get() == 'm' && file->get() == 't' && file->get() == ' '))
	{
		return {};
	}

	u32 subChunkSize = 0;
	subChunkSize |= (u32)file->get() << 0;
	subChunkSize |= (u32)file->get() << 8;
	subChunkSize |= (u32)file->get() << 16;
	subChunkSize |= (u32)file->get() << 24;

	u16 audioFormat = 0;
	audioFormat |= (u16)file->get() << 0;
	audioFormat |= (u16)file->get() << 8;

	u16 numChannels = 0;
	numChannels |= (u16)file->get() << 0;
	numChannels |= (u16)file->get() << 8;

	// LG_INF("{} {} {}", subChunkSize, audioFormat, numChannels);

	u32 sampleRate = 0;
	sampleRate |= (u32)file->get() << 0;
	sampleRate |= (u32)file->get() << 8;
	sampleRate |= (u32)file->get() << 16;
	sampleRate |= (u32)file->get() << 24;

	u32 byteRate = 0;
	byteRate |= (u32)file->get() << 0;
	byteRate |= (u32)file->get() << 8;
	byteRate |= (u32)file->get() << 16;
	byteRate |= (u32)file->get() << 24;

	u16 blockAlign = 0;
	blockAlign |= (u32)file->get() << 0;
	blockAlign |= (u32)file->get() << 8;

	u16 bitsPerSample = 0;
	bitsPerSample |= (u32)file->get() << 0;
	bitsPerSample |= (u32)file->get() << 8;

	// LG_INF("{} {} {} {}", sampleRate, byteRate, blockAlign, bitsPerSample);

	if (!(file->get() == 'L' && file->get() == 'I' && file->get() == 'S' && file->get() == 'T'))
	{
		return {};
	}

	// Listing stuff
	// Name of the song, author, etc
	u32 listingSize = 0;
	listingSize |= (u32)file->get() << 0;
	listingSize |= (u32)file->get() << 8;
	listingSize |= (u32)file->get() << 16;
	listingSize |= (u32)file->get() << 24;

	for (u32 i = 0; i < listingSize; i++)
		file->get();

	if (!(file->get() == 'd' && file->get() == 'a' && file->get() == 't' && file->get() == 'a'))
	{
		return {};
	}

	u32 dataLength = 0;
	dataLength |= (u32)file->get() << 0;
	dataLength |= (u32)file->get() << 8;
	dataLength |= (u32)file->get() << 16;
	dataLength |= (u32)file->get() << 24;

	size_t end = SCAST(size_t)(file->tellg()) + dataLength;
	return { file, end };
}

void StreamAudioClip(snd::buffer &buffer, audio_stream &stream)
{
	for (size_t i = 0; i < buffer.length && stream.pos() < stream.end; i++)
	{
		snd::sample l = 0;
		l |= stream.stream->get() << 0;
		l |= stream.stream->get() << 8;
		buffer.left[i] = l * stream.scale;

		snd::sample r = 0;
		r |= stream.stream->get() << 0;
		r |= stream.stream->get() << 8;
		buffer.right[i] = r * stream.scale;
	}
}
};

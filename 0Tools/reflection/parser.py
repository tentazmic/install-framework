import io
import re

class Parser:
	'''
	A wrapper around an input stream
	Mainly to keep track of line numbers
	'''
	def __init__(self, input: io.IOBase, start_line=0):
		self.stream = input
		self.start_line = start_line
		self.line_num = start_line

	def seek(self, pos: int):
		self.stream.seek(0)
		self.line_num = self.start_line

		for i in range(pos):
			if self.stream.read(1) == '\n':
				self.line_num += 1

	def step_back(self, count=1):
		new_pos = self.stream.tell() - count
		self.seek(new_pos)

	def read_char(self) -> str:
		c = self.stream.read(1)
		if c == '\n':
			self.line_num += 1
		return c

	def peek(self) -> str:
		c = self.read_char()
		self.step_back()
		return c

	def readline(self) -> str:
		self.line_num += 1
		return self.stream.readline()

	def get_line_num(self) -> int:
		return self.line_num

	def read_until(self, pattern: str) -> str:
		pos = self.stream.tell()
		remaining = str(self.stream.read(), encoding='utf-8')
		match = re.search(pattern, remaining)
		if match == None:
			print(f'Could not find {pattern} in remaining' +\
			      f'\nLine {self.line_num}\n{remaining}')
			return ''

		self.seek(pos)
		# match.begin() is relative from pos
		return str([ self.read_char() for i in range(match.begin()) ])


	def skip_until(self, pattern: str):
		pos = self.stream.tell()
		remaining = str(self.stream.read(), encoding='utf-8')
		match = re.search(pattern, remaining)
		if match == None:
			print(f'Could not find {pattern} in remaining' +\
			      f'\nLine {self.line_num}\n{remaining}')
			return

		new_pos = pos + match.end()
		self.seek(new_pos)

RGX_IF = re.compile('#if')
RGX_ENDIF = re.compile('#endif')

def c_go_to_endif(input: io.IOBase):
	depth = 1
	for line in input:
		if '#if' in line:
			depth += 1
		elif '#endif' in line:
			depth -= 1

		if depth == 0:
			break

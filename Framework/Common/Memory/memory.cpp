#include "pch.h"
#include "memory.hpp"
#include "arena.hpp"
#include "generations.hpp"

namespace mem {
byte * Align(byte *ptr, u16 alignment)
{
	union
	{
		byte *as_byte;
		uintptr_t as_num;
	};
	as_byte = ptr;

	if (alignment == 0 || as_num % alignment == 0) return ptr;

	uintptr_t div = as_num / alignment;
	div++;
	as_num = div * alignment;
	return as_byte;
}

void * StackAllocate(size_t size) { return alloca(size); }

void StackFree(void *)
{
	// LG_ERR("Cannot free stack allocated memory");
}
};

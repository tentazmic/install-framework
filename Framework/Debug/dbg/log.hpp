#pragma once

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

namespace dbg {

void Init();
void Shutdown();

extern spdlog::logger *LogGeneral;
extern spdlog::logger *LogPlain;

extern spdlog::logger *LogAssets;
extern spdlog::logger *LogAudio;
extern spdlog::logger *LogRender;
extern spdlog::logger *LogPlatform;

#ifdef BLD_EDITOR
extern spdlog::logger *LogEditor;
#endif

void __debugbreak();
};

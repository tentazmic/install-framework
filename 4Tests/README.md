# Test Framework

Any file ending in `.tst` is considered at test unit. The path to the test
unit from `4Tests/` doens't have to match the file being tested.
Test functions are denoted by `$ test` followed by a description.

## ROADMAP

Module tests: a folder in `src` called `tst` for the tests

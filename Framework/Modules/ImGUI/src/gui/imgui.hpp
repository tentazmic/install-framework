#pragma once

#include "Application/events.hpp"
#include "Util/list.hpp"

namespace gui {

void Initialise(evt::dispatcher&, arr::list<u32> configFlags);
void Shutdown();

void BeginFrame();
void EndFrame();
};

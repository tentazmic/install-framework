#pragma once
#include "core"

#include "UI/font.hpp"
#include "Util/wrap_list.hpp"
#include "Memory/generations.hpp"

namespace res {

using hdl_font = mem::handle<u16, 8, 8>;

struct font
{
	char *path;
	ui::font_data *data;
	gfx::hdl_texture texture;
};

class FontLibrary
{
public:
	FontLibrary(gfx::_TextureLib*, size_t capacity);
	~FontLibrary();

	hdl_font Load(const char *, bool expensiveColouring = false);

	inline ui::Font Get(hdl_font h) const
	{
		if (!IsValid(h))
			return ui::Font();

		const font *f = &_fonts[h.index];
		return ui::Font(f->data, f->texture);
	}

	inline bool IsValid(hdl_font h) const { return _generations[h.index] == h.generation; }

private:
	gfx::_TextureLib *_library;
	arr::dync_wrap_list<font> _fonts;
	mem::dync_generations_tracker<hdl_font::TYPE> _generations;
};
};

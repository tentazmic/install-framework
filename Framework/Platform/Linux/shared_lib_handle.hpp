#pragma once
#include <dlfcn.h>
#include "debug"

struct shared_library
{
private:
	void *_library;
	char *_path;

public:
	shared_library(const char *filepath)
		: _library(dlopen(filepath, RTLD_LAZY)), _path(nullptr)
	{
		if (!_library)
		{
			PF_WRN("Could not find shared lib at {}", filepath);
			return;
		}

		_path = RCAST(char *)(malloc(strlen(filepath) + 1));
		strcpy(_path, filepath);
		_path[strlen(filepath)] = 0;
	}

	~shared_library()
	{
		if (_library && dlclose(_library) != 0)
		{
			PF_ERR("Could not close shared lib {}", _path);
			ERR(dlerror());
		}
		free(_path);
	}

	bool IsValid() const { return _library; }
	const char * GetPath() const { return _path; }

	void * GetFunction(const char *name) const { return dlsym(_library, name); }
};

#shader vertex
#version 330 core

layout(location = 0) in vec2  aPosition;
layout(location = 1) in vec4  aColour;
layout(location = 2) in vec2  aTexCoord;
layout(location = 3) in float aTexture;
layout(location = 4) in float aTilingFactor;

uniform mat4 uViewProjection;

out vec4 vColour;
out vec2 vTexCoord;
flat out int vTexture;
out float vTilingFactor;

void main()
{
	vColour = aColour;
	vTexCoord = aTexCoord;
	vTexture = int(aTexture);
	vTilingFactor = aTilingFactor;

	gl_Position = uViewProjection * vec4(aPosition, 0.0, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 oColour;

in vec4 vColour;
in vec2 vTexCoord;
flat in int vTexture;
in float vTilingFactor;

uniform sampler2D uTextures[32];

void main()
{
	vec4 colour = vColour;
	switch (vTexture)
	{
		case 0:
			colour *= texture(uTextures[0], vTexCoord * vTilingFactor); break;
		case 1:
			colour *= texture(uTextures[1], vTexCoord * vTilingFactor); break;
		case 2:
			colour *= texture(uTextures[2], vTexCoord * vTilingFactor); break;
		case 3:
			colour *= texture(uTextures[3], vTexCoord * vTilingFactor); break;
		case 4:
			colour *= texture(uTextures[4], vTexCoord * vTilingFactor); break;
		case 5:
			colour *= texture(uTextures[5], vTexCoord * vTilingFactor); break;
		case 6:
			colour *= texture(uTextures[6], vTexCoord * vTilingFactor); break;
		case 7:
			colour *= texture(uTextures[7], vTexCoord * vTilingFactor); break;
		case 8:
			colour *= texture(uTextures[8], vTexCoord * vTilingFactor); break;
		case 9:
			colour *= texture(uTextures[9], vTexCoord * vTilingFactor); break;
		case 10:
			colour *= texture(uTextures[10], vTexCoord * vTilingFactor); break;
		case 11:
			colour *= texture(uTextures[11], vTexCoord * vTilingFactor); break;
		case 12:
			colour *= texture(uTextures[12], vTexCoord * vTilingFactor); break;
		case 13:
			colour *= texture(uTextures[13], vTexCoord * vTilingFactor); break;
		case 14:
			colour *= texture(uTextures[14], vTexCoord * vTilingFactor); break;
		case 15:
			colour *= texture(uTextures[15], vTexCoord * vTilingFactor); break;
		case 16:
			colour *= texture(uTextures[16], vTexCoord * vTilingFactor); break;
		case 17:
			colour *= texture(uTextures[17], vTexCoord * vTilingFactor); break;
		case 18:
			colour *= texture(uTextures[18], vTexCoord * vTilingFactor); break;
		case 19:
			colour *= texture(uTextures[19], vTexCoord * vTilingFactor); break;
		case 20:
			colour *= texture(uTextures[20], vTexCoord * vTilingFactor); break;
		case 21:
			colour *= texture(uTextures[21], vTexCoord * vTilingFactor); break;
		case 22:
			colour *= texture(uTextures[22], vTexCoord * vTilingFactor); break;
		case 23:
			colour *= texture(uTextures[23], vTexCoord * vTilingFactor); break;
		case 24:
			colour *= texture(uTextures[24], vTexCoord * vTilingFactor); break;
		case 25:
			colour *= texture(uTextures[25], vTexCoord * vTilingFactor); break;
		case 26:
			colour *= texture(uTextures[26], vTexCoord * vTilingFactor); break;
		case 27:
			colour *= texture(uTextures[27], vTexCoord * vTilingFactor); break;
		case 28:
			colour *= texture(uTextures[28], vTexCoord * vTilingFactor); break;
		case 29:
			colour *= texture(uTextures[29], vTexCoord * vTilingFactor); break;
		case 30:
			colour *= texture(uTextures[30], vTexCoord * vTilingFactor); break;
		case 31:
			colour *= texture(uTextures[31], vTexCoord * vTilingFactor); break;
	}

	oColour = colour;
}

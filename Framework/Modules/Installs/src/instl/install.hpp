#pragma once

#if defined(BLD_INSTALL)
	#ifndef INSTALL_API
		#error Failed to pass the shared library exposure declaration for the installs
	#endif
	#define INSTALL_VAR(TYPE, NAME)  INSTALL_API extern TYPE instl_##NAME
	#define INSTALL_FUNC(TYPE, NAME, ...) INSTALL_API TYPE instl_##NAME (__VA_ARGS__)
#else
	#define INSTALL_VAR(TYPE, NAME) extern const char *id_##NAME
	#define INSTALL_FUNC(TYPE, NAME, ...) \
	       extern const char *id_##NAME; using fn_##NAME = TYPE (*) (__VA_ARGS__)
#endif

#include "game_data.hpp"
#include "Graphics/texture.hpp"

namespace instl {

struct install_info
{
	const char *name;

	// Install Dependencies
	// Install Type
	// etc
};

struct pointers
{
	void *assets;
};
};

// Install Functions are going to be added so infrequently plus
// they would be hand written in the actual install so there's
// no point in ppprocessing this

#ifdef BLD_INSTALL
extern "C" {
#else
namespace instl {
#endif

#include "_symbols.ipp"
};

#undef INSTALL_VAR
#undef INSTALL_FUNC

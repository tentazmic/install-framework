#pragma once
#include "Audio/audio.hpp"

namespace dsound {

snd::buffer GetBuffer();

void Initialise(u32 samplesPerSecond, f32 bufferDuration, f32 latency);
void Shutdown();

void Play();
void Stop();

void Update();

};

#!/usr/bin/python

import sys
import os

import templates

__USAGE = f"""\
python {sys.argv[0]} [OPTIONS]

-Help | -h | ?
-Name | -n                 <string>
-MainExecutableName | -exe <string>
-InstallExtension | -ext   <string>

-Platform | -p             <string[]>

-Verbose | -v
"""

NAME = None
EXECUTABLE_NAME = "game.exe"
INSTALL_EXTENSION = "ssj"

PLATFORMS = []

VERBOSE = False

ER_NO_NAME = "ERROR No Name provided"
ER_NO_EXECUTABLE_NAME = "ERROR No value for -MainExectuableName|-exe argument"
ER_NO_INSTALL_EXTENSION = "ERROR No value for -InstallExtension|-ext argument"
ER_UNKNOWN_PLATFORM = "ERROR Platform {} is unknown"

NM_BASE = "Base"
NM_INSTALLS = "Installs"

def read_args():
	global NAME
	global EXECUTABLE_NAME
	global INSTALL_EXTENSION
	global VERBOSE
	global PLATFORMS

	args = sys.argv[1:]
	while len(args) > 0:
		arg = args.pop(0)
		match arg:
			case '-Help' | '-h' | '?':
				print(__USAGE)
				exit(0)

			case '-Name' | '-n':
				name = args.pop(0)
				if name.startswith('-') or name == '?':
					print(ER_NO_NAME)
					exit(1)
				NAME = name
			case '-MainExecutableName' | '-exe':
				name = args.pop(0)
				if name.startswith('-'):
					print(ER_NO_EXECUTABLE_NAME)
					exit(1)
				EXECUTABLE_NAME = name
			case '-InstallExtension' | '-ext':
				ext = args.pop(0)
				if ext.startswith('-'):
					print(ER_NO_INSTALL_EXTENSION)
					exit(1)
				INSTALL_EXTENSION = ext
			case '-Platform' | 'p':
				while len(args) > 0 and not args[0].startswith('-'):
					p = args.pop(0)
					match p.lower():
						case 'windows' | 'win':
							PLATFORMS.append('Windows')
						case 'linux' | 'lin':
							PLATFORMS.append('Linux')
						case _:
							print(ER_UNKNOWN_PLATFORM.format(p))
							exit(1)
			case '-Verbose' | '-n':
				VERBOSE = True

	return

def make_dir(path: str):
	if os.path.exists(path):
		if VERBOSE:
			print(f"{path} already exists")
	else:
		os.mkdir(path)
		if VERBOSE:
			print(f"Made {path}")

if __name__ == "__main__":
	read_args()

	if NAME == None:
		print(ER_NO_NAME)
		exit(0)

	print("Setting up game project")

	if VERBOSE:
		print("Make relevant folders")

	make_dir("Assets")
	make_dir(NM_BASE)
	make_dir(NM_BASE + "/" + NAME)
	make_dir(NM_INSTALLS)

	with open(f"{NM_BASE}/{NAME}/launcher", "w") as f:
		f.write(f"name: {NAME}\n")
		f.write(f"exe: {EXECUTABLE_NAME}\n")
		f.write("modules: application\n")

	templates.execute('launcher.cpp', f'{NM_BASE}/{NAME}/game.cpp')

	make_dir("Platforms")
	for p in PLATFORMS:
		path = f'Platforms/{p}.cpp'
		templates.execute(path, path)

	context = {
		'numproc': 10,
		'timer': False,
		'verbose': False,
		'chime': 'always',
		'run': False,
		'clean': 'none',
		'no-lines': ''
	}
	templates.execute('config.py', 'config.py', context)

	context = {
		'name': NAME,
		'install_extension': INSTALL_EXTENSION
	}
	templates.execute('if.py', 'if.py', context)


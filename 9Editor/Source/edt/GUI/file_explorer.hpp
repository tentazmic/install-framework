#pragma once
#include "edt/Assets/watcher.hpp"
#include "Util/list.hpp"
#include "Util/bitset.hpp"

namespace edt::gui {

struct explorer_selection
{
	size_t index;
	// Is true when the entry at INDEX has been clicked
	// while already being selected
	bool byDoubleSelection;
};

struct explorer_filter
{
	bool onlyDirectories;
	const char *extension;
};

// Draws the entries in a tree view
// Returns the selected entry, if no entry has been selected INDEX
// will be set to the count of TREE
explorer_selection DrawFileExplorer(const edt::asset::directory_tree& tree, size_t currentSelected, arr::bitset activeEntries, const explorer_filter& filter);

struct contents_config
{
	bool useListView;
};

// Draws the contents of the directory at idxDirectory
explorer_selection DrawDirectoryContents(const edt::asset::directory_tree& tree, size_t idxDirectory,  size_t currentSelected, const contents_config& config, const explorer_filter& filter);

};

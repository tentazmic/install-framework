#include "pch.h"
#include "events"
#include "debug"

#include "instl/install.hpp"

#include "res/assets.hpp"
#include "game_data.hpp"

const char *res::g_AssetRoot = PRJ_ROOT;
const char *res::g_LocalAssetRoot = LOCAL_PRJ_ROOT;

instl::install_info instl_Information{
	.name =  "Playground"
};

void instl_Startup(instl::pointers& p)
{
	LG_INIT;
	DBG(instl_Information.name);
}

void instl_Shutdown()
{
	DBG("Leave {}", instl_Information.name);
}

void instl_LoadScene(gfx::_TextureLib *textures, arr::list<game::sprite> &sprites, arr::list<game::camera> &cameras)
{
	gfx::texture tex;
	res::LoadTexture("Assets/Textures/logo_render.png", &tex);
	gfx::hdl_texture h = textures->Load(tex);
	res::DeleteTexture(tex);

	game::sprite s{
		.position = glm::vec2(2.4f, -3.f),
		.rotation = 0,
		.moveable = false,
		.texture = h
	};

	sprites.push(s);
}

#include "pch.h"
#include "_ogl.hpp"
#include "ogl_texture.hpp"
#include "debug"

static_assert(sizeof(GLenum) <= sizeof(u32), "Glenum is larger than size of format_data.internal and format_data.pixel");

namespace gfx {

namespace {

	struct format_data { GLenum internal, pixel, type; u8 bytesPerPixel; };

	format_data GetFormatData(texture_format);
};

OGLTexture::OGLTexture(texture_format format, u32 width, u32 height, u32 depth)
		: _ID(0), _dimensions(width, height, depth), _format{ format }
#ifdef HOLD_IDS
		  , _path(nullptr)
#endif
		// _internalFormat(GL_RGBA8), _dataFormat(GL_RGBA)
{
	switch (type())
	{
		case ETextureType::TWOD:
		{
			format_data f = GetFormatData(format);
			OGL_CHECK(glCreateTextures(GL_TEXTURE_2D, 1, &_ID))
			OGL_CHECK(glTextureStorage2D(_ID, 1, f.internal, width, height))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_WRAP_S, GL_REPEAT))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_WRAP_T, GL_REPEAT))
			break;
		}
		case ETextureType::THREED:
		{
			RD_ERR("Not set up 3D Textures");
			return;
		}
		default:
			RD_ERR("Unknown texture type");
	}
}

OGLTexture::OGLTexture(const texture& tex)
	: _ID(0), _dimensions(tex.dimensions), _format{ tex.format }
#ifdef HOLD_IDS
		  , _path(nullptr)
#endif
		// _internalFormat(0), _dataFormat(0)
{
	format_data f = GetFormatData(tex.format);
/*
	switch (tex.format.numChannels)
	{
		case 3:
		{
			_internalFormat = GL_RGB8;
			_dataFormat = GL_RGB;
			break;
		}
		case 4:
		{
			_internalFormat = GL_RGBA8;
			_dataFormat = GL_RGBA;
			break;
		}
	}

	if (!(_internalFormat || _dataFormat))
	{
		RD_ERR("Texture formats not set properly");
		return;
	}
	_dimensions = { tex.dimensions.x, tex.dimensions.y };
*/
	switch (type())
	{
		case ETextureType::TWOD:
		{
			OGL_CHECK(glCreateTextures(GL_TEXTURE_2D, 1, &_ID))
			OGL_CHECK(
				glTextureStorage2D(_ID, 1, f.internal, _dimensions.x, _dimensions.y)
			)
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_WRAP_S, GL_REPEAT))
			OGL_CHECK(glTextureParameteri(_ID, GL_TEXTURE_WRAP_T, GL_REPEAT))
			OGL_CHECK(
				glTextureSubImage2D(_ID, 0, 0, 0, _dimensions.x, _dimensions.y,
														f.pixel, f.type, tex.image)
			)
			break;
		}
		case ETextureType::THREED:
		{
			RD_ERR("Not set up 3D Textures");
			break;
		}
		default:
			RD_ERR("Unknown texture type");
	}

#ifdef HOLD_IDS
	if (tex.path)
	{
		size_t length = strlen(tex.path);
		char *buffer = SCAST(char *)(malloc(length + 1));
		strcpy(buffer, tex.path);
		buffer[length] = '\0';
		_path = buffer;
	}
#endif
}

OGLTexture::~OGLTexture()
{
	glDeleteTextures(1, &_ID);
#ifdef HOLD_IDS
	if (_path)
		FREE(_path);
#endif
}

void OGLTexture::Bind(u32 slot) const { OGL_CHECK(glBindTextureUnit(slot, _ID)) }

void OGLTexture::SetTextureData(const void *data, size_t size)
{
	format_data f = GetFormatData(_format);
	// u32 bytesPerPixel = f.bytesPerComponent * _format.numChannels;
	ASSERT(size == _dimensions.x * _dimensions.y * f.bytesPerPixel,
	       "The size of the passed in data does match the alloted space for this texture");

	switch (type())
	{
		case ETextureType::TWOD:
		{
			OGL_CHECK(
				glTextureSubImage2D(_ID, 0, 0, 0, _dimensions.x, _dimensions.y,
														f.pixel, f.type, data)
			)
			break;
		}
		case ETextureType::THREED:
		{
			RD_ERR("Not set up 3D Textures");
			break;
		}
		default:
			RD_ERR("Unknown texture type");
	}
}



OGLTextureLib::OGLTextureLib(size_t capacity)
	: _textures(capacity), _generations(capacity)
{ }

OGLTextureLib::~OGLTextureLib()
{
	_textures.clear();
}

hdl_texture OGLTextureLib::Create(texture_format format, u32 width, u32 height, u32 depth /* = 0 */)
{
	hdl_texture::TYPE index = _textures.head();
	new (SCAST(void *)(_textures.next_slot())) OGLTexture(format, width, height, depth);
	return hdl_texture{ index, _generations.tick(index) };
}

hdl_texture OGLTextureLib::Load(const texture& tex)
{
#if defined(ENABLE_ASSERTS) && defined(HOLD_IDS)
	FOR(it, _textures)
	{
		if (!it->GetPath() || strcmp(it->GetPath(), tex.path) != 0)
			continue;

		RD_WRN("Already loaded texture {}", tex.path);
		hdl_texture::TYPE index = it - _textures.begin();
		return hdl_texture{ index, _generations[index] };
	}
#endif

	hdl_texture::TYPE index = _textures.head();
	new (SCAST(void *)(_textures.next_slot())) OGLTexture(tex);
	return hdl_texture{ index, _generations.tick(index) };
}

_Texture * OGLTextureLib::Get(hdl_texture handle)
{
#ifdef ENABLE_ASSERTS
	if (! IsValid(handle))
	{
		hdl_texture::TYPE got = handle.generation;
		RD_ERR("Invalid Texture Access GOT {} EXPECT {}",
			got, _generations[handle.index]
			);
		return nullptr;
	}
#endif
	return DCAST(_Texture *)(&_textures[handle.index]);
}

bool OGLTextureLib::IsValid(hdl_texture handle) const
{
	return handle.generation == _generations[handle.index];
}

namespace {

#define UNHANDLED_TEXEL(TYPE) \
case ETextureTexel:: TYPE :   \
RD_ERR("Have not configured " #TYPE ); break;

	format_data GetFormatData(texture_format format)
	{
		switch (format.texel)
		{
			UNHANDLED_TEXEL(HALF_FLOAT)
			UNHANDLED_TEXEL(FLOAT)
			UNHANDLED_TEXEL(UINT8)
			UNHANDLED_TEXEL(UINT16)
			UNHANDLED_TEXEL(UINT32)
			UNHANDLED_TEXEL(SINT8)
			UNHANDLED_TEXEL(SINT16)
			UNHANDLED_TEXEL(SINT32)
			UNHANDLED_TEXEL(NORMALISED_FLOAT)
			case ETextureTexel::NORMALISED_UINT8:
				switch (format.numChannels)
				{
					case 3:
						return format_data {
							.internal = GL_RGB8, .pixel = GL_RGB,
							.type = GL_UNSIGNED_BYTE, .bytesPerPixel = 3
						};
					case 4:
						return format_data {
							.internal = GL_RGBA8, .pixel = GL_RGBA,
							.type = GL_UNSIGNED_BYTE, .bytesPerPixel = 4
						};
					default:
						RD_ERR("NORMALISED_UINT8 Cannot handle {} channels", format.numChannels);
				}
			UNHANDLED_TEXEL(NORMALISED_UINT16)
			UNHANDLED_TEXEL(NORMALISED_SINT8)
			UNHANDLED_TEXEL(NORMALISED_SINT16)
		}

		return format_data{};
	}
};
};

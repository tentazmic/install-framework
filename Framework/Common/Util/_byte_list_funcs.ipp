#ifdef COUNT_PTR
	#define COUNT (*_count)
#else
	#define COUNT _count
#endif

void push(const byte *item)
{
#ifdef ENABLE_ASSERTS
	if (COUNT == _capacity)
	{
		LG_ERR("List is Full {}", _capacity);
	}
#endif
	byte *slot = at(COUNT++);
	std::memcpy(slot, item, _elementSize);
}

byte * pop()
{
#ifdef ENABLE_ASSERTS
	if (COUNT == 0)
	{
		LG_ERR("List is Empty");
		return _data;
	}
#endif
	return at(COUNT--);
}

void insert(size_t index, const byte *item)
{
#ifdef ENABLE_ASSERTS
	if (index >= _capacity)
	{
		LG_ERR("Out of bounds insert IDX {} CAPACITY {}", index, _capacity);
		return;
	}
	if (COUNT >= _capacity)
	{
		LG_ERR("List is Full {}", _capacity);
		return;
	}
#endif
	if (index >= COUNT)
	{
		push(item);
		return;
	}

	for (size_t i = COUNT - 1; i >= index; i--)
		std::memcpy(at(i + 1), at(i), _elementSize);

	std::memcpy(at(index), item, _elementSize);
	COUNT++;
}

template<bool is_const>
void remove(const byte_iterator<is_const>& it)
{
#ifdef ENABLE_ASSERTS
	if (it < begin() || it >= end())
	{
		LG_ERR("Illegal List Address ITER {}; BEG {} END {}",
			RCAST(uintptr_t)(it.ptr),
			RCAST(uintptr_t)(begin().ptr),
			RCAST(uintptr_t)(end().ptr)
		);
		return;
	}
#endif
	size_t index = it - begin();
	remove(index);
}

void remove(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Remove IDX {} COUNT {}", index, COUNT);
		return;
	}
#endif
	if (index == COUNT - 1)
	{
		pop();
		return;
	}

	COUNT--;
	for (; index < COUNT; index++)
		std::memcpy(at(index), at(index + 1), _elementSize);
}

void clear()
{
	std::memset(_data, 0, _capacity * _elementSize);
	COUNT = 0;
}

void set(size_t index, const byte *item)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return;
	}
#endif
	memcpy(at(index), item, _elementSize);
}

byte * at(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data;
	}
#endif
	return &_data[index * _elementSize];
}

const byte * at(size_t index) const
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data;
	}
#endif
	return &_data[index * _elementSize];
}

INLINE byte * operator [](size_t index) { return at(index); }
INLINE const byte * operator [](size_t index) const { return at(index); }

inline biter find(const byte *other)
{ return std::find_if(begin(), end(), [&](const byte *elem) { return memcmp(other, elem, _elementSize); }); }
inline bciter find(const byte *other) const
{ return std::find_if(cbegin(), cend(), [&](const byte *elem) { return memcmp(other, elem, _elementSize); }); }

INLINE biter begin() { return biter(&_data[0], _elementSize); }
INLINE biter end()   { return biter(&_data[COUNT * _elementSize], _elementSize); }

INLINE biter rbegin() { return biter(at(COUNT - 1), _elementSize); }
INLINE biter rend()   { return biter(&_data[-_elementSize], _elementSize); }

INLINE bciter cbegin() const { return bciter(&_data[0], _elementSize); }
INLINE bciter cend() const { return bciter(&_data[COUNT * _elementSize], _elementSize); }

INLINE bciter rcbegin() const { return bciter(at(COUNT - 1), _elementSize); }
INLINE bciter rcend() const { return bciter(&_data[-_elementSize], _elementSize); }

INLINE bool full() const { return COUNT == _capacity; }
INLINE bool empty() const { return COUNT == 0; }

INLINE byte * front() { return _data; }
INLINE byte * back() { return at(COUNT - 1); }
INLINE const byte * front() const { return _data; }
INLINE const byte * back() const { return at(COUNT - 1); }

inline byte * data() { return _data; }
inline size_t elementSize() const { return _elementSize; }
inline size_t count() const { return COUNT; }
inline size_t capacity() const { return _capacity; }

#undef COUNT
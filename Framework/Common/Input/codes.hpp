#pragma once
#include "core"

namespace inp {

ENUM EKey
{
	const static EKey SPACE;
	const static EKey APOSTROPHE;
	const static EKey COMMA;
	const static EKey MINUS;
	const static EKey PERIOD;
	const static EKey FSLASH;
	const static EKey NM_0;
	const static EKey NM_1;
	const static EKey NM_2;
	const static EKey NM_3;
	const static EKey NM_4;
	const static EKey NM_5;
	const static EKey NM_6;
	const static EKey NM_7;
	const static EKey NM_8;
	const static EKey NM_9;
	const static EKey SEMICOLON;
	const static EKey EQUAL;
	const static EKey A;
	const static EKey B;
	const static EKey C;
	const static EKey D;
	const static EKey E;
	const static EKey F;
	const static EKey G;
	const static EKey H;
	const static EKey I;
	const static EKey J;
	const static EKey K;
	const static EKey L;
	const static EKey M;
	const static EKey N;
	const static EKey O;
	const static EKey P;
	const static EKey Q;
	const static EKey R;
	const static EKey S;
	const static EKey T;
	const static EKey U;
	const static EKey V;
	const static EKey W;
	const static EKey X;
	const static EKey Y;
	const static EKey Z;
	const static EKey LBRACKET;
	const static EKey BSLASH;
	const static EKey RBRACKET;
	const static EKey GRAVE;
	const static EKey WORLD_1;
	const static EKey WORLD_2;

	// Function keys
	const static EKey ESC;
	const static EKey ENTER;
	const static EKey TAB;
	const static EKey BACKSPACE;
	const static EKey INSERT;
	const static EKey DELETE;
	const static EKey RIGHT;
	const static EKey LEFT;
	const static EKey DOWN;
	const static EKey UP;
	const static EKey PAGE_UP;
	const static EKey PAGE_DOWN;
	const static EKey HOME;
	const static EKey END;
	const static EKey CAPS;
	const static EKey SCROLL;
	const static EKey NUM_LOCK;
	const static EKey PRINT_SCREEN;
	const static EKey PAUSE;
	const static EKey F1;
	const static EKey F2;
	const static EKey F3;
	const static EKey F4;
	const static EKey F5;
	const static EKey F6;
	const static EKey F7;
	const static EKey F8;
	const static EKey F9;
	const static EKey F10;
	const static EKey F11;
	const static EKey F12;
	const static EKey F13;
	const static EKey F14;
	const static EKey F15;
	const static EKey F16;
	const static EKey F17;
	const static EKey F18;
	const static EKey F19;
	const static EKey F20;
	const static EKey F21;
	const static EKey F22;
	const static EKey F23;
	const static EKey F24;
	const static EKey F25;
	const static EKey KP_0;
	const static EKey KP_1;
	const static EKey KP_2;
	const static EKey KP_3;
	const static EKey KP_4;
	const static EKey KP_5;
	const static EKey KP_6;
	const static EKey KP_7;
	const static EKey KP_8;
	const static EKey KP_9;
	const static EKey KP_DECIMAL;
	const static EKey KP_DIVIDE;
	const static EKey KP_MULTIPLY;
	const static EKey KP_SUBTRACT;
	const static EKey KP_ADD;
	const static EKey KP_ENTER;
	const static EKey KP_EQUAL;
	const static EKey LSHIFT;
	const static EKey LCONTROL;
	const static EKey L_ALT;
	const static EKey LSUPER;
	const static EKey RSHIFT;
	const static EKey RCONTROL;
	const static EKey R_ALT;
	const static EKey RSUPER;
	const static EKey MENU;

	u16 value;
};

ENUM EMouseButton
{
	const static EMouseButton LEFT;
	const static EMouseButton RIGHT;
	const static EMouseButton MIDDLE;

	const static EMouseButton BTN_1;
	const static EMouseButton BTN_2;
	const static EMouseButton BTN_3;
	const static EMouseButton BTN_4;
	const static EMouseButton BTN_5;
	const static EMouseButton BTN_6;
	const static EMouseButton BTN_7;
	const static EMouseButton BTN_8;

	u8 value;
};

ENUM EGamepadButton
{
	const static EGamepadButton FACE_N;
	const static EGamepadButton FACE_S;
	const static EGamepadButton FACE_W;
	const static EGamepadButton FACE_E;
	const static EGamepadButton DPAD_U;
	const static EGamepadButton DPAD_D;
	const static EGamepadButton DPAD_L;
	const static EGamepadButton DPAD_R;
	const static EGamepadButton L_BUMP;
	const static EGamepadButton R_BUMP;
	const static EGamepadButton START;
	const static EGamepadButton SELECT;
	const static EGamepadButton HOME;
	const static EGamepadButton L_STICK;
	const static EGamepadButton R_STICK;

	u8 value;
};

ENUM EGamepadAxis
{
	const static EGamepadAxis LEFT_X;
	const static EGamepadAxis LEFT_Y;
	const static EGamepadAxis RIGHT_X;
	const static EGamepadAxis RIGHT_Y;
	const static EGamepadAxis L_TRIGGER;
	const static EGamepadAxis R_TRIGGER;

	u8 value;
};

inline bool operator ==(const inp::EKey a, const inp::EKey b)
	{ return a.value == b.value; }
inline bool operator !=(const inp::EKey a, const inp::EKey b)
	{ return !(a == b); }

inline bool operator ==(const inp::EMouseButton a, const inp::EMouseButton b)
	{ return a.value == b.value; }
inline bool operator !=(const inp::EMouseButton a, const inp::EMouseButton b)
	{ return !(a == b); }

struct key_less
{
	constexpr bool operator()(const EKey& lhs, const EKey& rhs) const
	{ return lhs.value < rhs.value; }
};

struct mbutton_less
{
	constexpr bool operator()(const EMouseButton& lhs, const EMouseButton& rhs) const
	{ return lhs.value < rhs.value; }
};
};

#ifdef ENABLE_LOG
	#include "dbg/codes.ipp"
#endif

#pragma once
#include "panel.hpp"
#include "ECS/ecs.hpp"
#include "Memory/ref.hpp"
#include "edt/Assets/watcher.hpp"
#include "Util/bitset.hpp"

// TODO Features to implement
//   Upon clicking a folder in the asset explorer, its
//   README.md file is renderered in the inspector

namespace edt::panel::inspector {

enum struct ETargetType { EMPTY, WORLD, ENTITY };

struct inspect_target
{
	edt::asset::scene *scene;
	ecs::entity_handle entity;
	i32 selected = -1;

	bool IsActive() const { return scene && scene->world; }
	inline bool isEntity() const { return entity.IsActive(); }
};

class PnlInspector;

// Created upon creation of the first inspector in the registry
// Deleted upon deletion of the last """"
struct shared_data
{
	u8 iLastFocused = 255;
	arr::sttc_list<PnlInspector *, 8> inspectors;

	// Filesystem data
	i8 filesystemUser;
	edt::asset::watcher watcher;
	arr::dync_bitset activeEntries;
	size_t selection;
};

class PnlInspector : public _Panel
{
public:
	const static u16 MAX_NUM = 0;

	PnlInspector(PanelRegistry *registry, const inspect_target& t = inspect_target{});
	~PnlInspector() override;

	void Update(f32 deltaTime) override;
	void OnEvent(evt::event& e) override;
	void GUI() override;

	void OnFocusGained() override;

	shared_data * shared() { return _shared; }

	inspect_target target;
	// ID     = index in the inspectors list of the shared
	// parent = index of this inspector's parent
	// iChild = the index in the parent's child list
	i8 ID, parent, iChild;
	bool hasChildren;

private:
	void EmptyView();
	void WorldView();
	void EntityView();

	void IntoNewTarget(const inspect_target& t);
	void Back();

	PanelRegistry *_registry;
	shared_data *_shared;
};
};
#pragma once

namespace res {

struct shader_source
{
	const char *vertex;
	const char *pixel;
};

bool ParseShader(const char *path, shader_source&);
};

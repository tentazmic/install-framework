#pragma once

#define ASSERT_STR "ASSERT FAIL "

#ifdef ENABLE_DEBUGGER
	#define BREAKPOINT ::dbg::__debugbreak()
#else
	#define BREAKPOINT
#endif

#if defined(ENABLE_LOG) || defined(ENABLE_ASSERTS)
	#include "dbg/log.hpp"

	#define LG_INIT ::dbg::Init()
	#define LG_SHUTDOWN ::dbg::Shutdown()

	#define INLINE
#else
	#define LG_INIT
	#define LG_SHUTDOWN

	#define INLINE inline
#endif

#ifdef ENABLE_LOG
	#define TRC(...) SPDLOG_LOGGER_TRACE(::dbg::LogPlain, __VA_ARGS__)
	#define DBG(...) SPDLOG_LOGGER_DEBUG(::dbg::LogPlain, __VA_ARGS__)
	#define INF(...) SPDLOG_LOGGER_INFO(::dbg::LogPlain, __VA_ARGS__)
	#define WRN(...) SPDLOG_LOGGER_WARN(::dbg::LogPlain, __VA_ARGS__)
	#define ERR(...) SPDLOG_LOGGER_ERROR(::dbg::LogPlain, __VA_ARGS__)
	#define CRT(...) SPDLOG_LOGGER_CRITICAL(::dbg::LogPlain, __VA_ARGS__)

	#define LG_TRC(...) SPDLOG_LOGGER_TRACE(::dbg::LogGeneral, __VA_ARGS__)
	#define LG_DBG(...) SPDLOG_LOGGER_DEBUG(::dbg::LogGeneral, __VA_ARGS__)
	#define LG_INF(...) SPDLOG_LOGGER_INFO(::dbg::LogGeneral, __VA_ARGS__)
	#define LG_WRN(...) SPDLOG_LOGGER_WARN(::dbg::LogGeneral, __VA_ARGS__)
	#define LG_ERR(...) SPDLOG_LOGGER_ERROR(::dbg::LogGeneral, __VA_ARGS__)
	#define LG_CRT(...) SPDLOG_LOGGER_CRITICAL(::dbg::LogGeneral, __VA_ARGS__)

	#define AS_TRC(...)    SPDLOG_LOGGER_TRACE(::dbg::LogAssets, __VA_ARGS__)
	#define AS_DBG(...)    SPDLOG_LOGGER_DEBUG(::dbg::LogAssets, __VA_ARGS__)
	#define AS_INF(...)    SPDLOG_LOGGER_INFO(::dbg::LogAssets, __VA_ARGS__)
	#define AS_WRN(...)    SPDLOG_LOGGER_WARN(::dbg::LogAssets, __VA_ARGS__)
	#define AS_ERR(...)    SPDLOG_LOGGER_ERROR(::dbg::LogAssets, __VA_ARGS__)
	#define AS_CRT(...)    SPDLOG_LOGGER_CRITICAL(::dbg::LogAssets, __VA_ARGS__)

	#define AD_TRC(...)    SPDLOG_LOGGER_TRACE(::dbg::LogAudio, __VA_ARGS__)
	#define AD_DBG(...)    SPDLOG_LOGGER_DEBUG(::dbg::LogAudio, __VA_ARGS__)
	#define AD_INF(...)    SPDLOG_LOGGER_INFO(::dbg::LogAudio, __VA_ARGS__)
	#define AD_WRN(...)    SPDLOG_LOGGER_WARN(::dbg::LogAudio, __VA_ARGS__)
	#define AD_ERR(...)    SPDLOG_LOGGER_ERROR(::dbg::LogAudio, __VA_ARGS__)
	#define AD_CRT(...)    SPDLOG_LOGGER_CRITICAL(::dbg::LogAudio, __VA_ARGS__)

	#define RD_TRC(...)    SPDLOG_LOGGER_TRACE(::dbg::LogRender, __VA_ARGS__)
	#define RD_DBG(...)    SPDLOG_LOGGER_DEBUG(::dbg::LogRender, __VA_ARGS__)
	#define RD_INF(...)    SPDLOG_LOGGER_INFO(::dbg::LogRender, __VA_ARGS__)
	#define RD_WRN(...)    SPDLOG_LOGGER_WARN(::dbg::LogRender, __VA_ARGS__)
	#define RD_ERR(...)    SPDLOG_LOGGER_ERROR(::dbg::LogRender, __VA_ARGS__)
	#define RD_CRT(...)    SPDLOG_LOGGER_CRITICAL(::dbg::LogRender, __VA_ARGS__)

	#define PF_TRC(...)    SPDLOG_LOGGER_TRACE(::dbg::LogPlatform, __VA_ARGS__)
	#define PF_DBG(...)    SPDLOG_LOGGER_DEBUG(::dbg::LogPlatform, __VA_ARGS__)
	#define PF_INF(...)    SPDLOG_LOGGER_INFO(::dbg::LogPlatform, __VA_ARGS__)
	#define PF_WRN(...)    SPDLOG_LOGGER_WARN(::dbg::LogPlatform, __VA_ARGS__)
	#define PF_ERR(...)    SPDLOG_LOGGER_ERROR(::dbg::LogPlatform, __VA_ARGS__)
	#define PF_CRT(...)    SPDLOG_LOGGER_CRITICAL(::dbg::LogPlatform, __VA_ARGS__)

	#define ED_TRC(...)    SPDLOG_LOGGER_TRACE(::dbg::LogEditor, __VA_ARGS__)
	#define ED_DBG(...)    SPDLOG_LOGGER_DEBUG(::dbg::LogEditor, __VA_ARGS__)
	#define ED_INF(...)    SPDLOG_LOGGER_INFO(::dbg::LogEditor, __VA_ARGS__)
	#define ED_WRN(...)    SPDLOG_LOGGER_WARN(::dbg::LogEditor, __VA_ARGS__)
	#define ED_ERR(...)    SPDLOG_LOGGER_ERROR(::dbg::LogEditor, __VA_ARGS__)
	#define ED_CRT(...)    SPDLOG_LOGGER_CRITICAL(::dbg::LogEditor, __VA_ARGS__)

	#define MOVE_CONSTRUCTOR_CHECK if (this == &other) { LG_WRN("Attempted to move object to self"); return; }
	#define MOVE_CHECK if (this == &other) { LG_WRN("Attempted to move object to self"); return *this; }
	#define COPY_CHECK if (this == &other) { LG_WRN("Attempted to copy object to self"); return *this; }
#else
	#define TRC(...)
	#define DBG(...)
	#define INF(...)
	#define WRN(...)
	#define ERR(...)
	#define CRT(...)

	#define LG_TRC(...)
	#define LG_DBG(...)
	#define LG_INF(...)
	#define LG_WRN(...)
	#define LG_ERR(...)
	#define LG_CRT(...)

	#define AS_TRC(...)
	#define AS_DBG(...)
	#define AS_INF(...)
	#define AS_WRN(...)
	#define AS_ERR(...)
	#define AS_CRT(...)

	#define AD_TRC(...)
	#define AD_DBG(...)
	#define AD_INF(...)
	#define AD_WRN(...)
	#define AD_ERR(...)
	#define AD_CRT(...)

	#define RD_TRC(...)
	#define RD_DBG(...)
	#define RD_INF(...)
	#define RD_WRN(...)
	#define RD_ERR(...)
	#define RD_CRT(...)

	#define PF_TRC(...)
	#define PF_DBG(...)
	#define PF_INF(...)
	#define PF_WRN(...)
	#define PF_ERR(...)
	#define PFq_CRT(...)

	#define ED_TRC(...)
	#define ED_DBG(...)
	#define ED_INF(...)
	#define ED_WRN(...)
	#define ED_ERR(...)
	#define ED_CRT(...)

	#define MOVE_CONSTRUCTOR_CHECK if (this == &other) return;
	#define MOVE_CHECK if (this == &other) return *this;
	#define COPY_CHECK if (this == &other) return *this;
#endif

#ifdef ENABLE_ASSERTS
	#define ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogGeneral, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
	#define AS_ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogAssets, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
	#define AD_ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogAudio, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
	#define RD_ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogRender, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
	#define PF_ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogPlatform, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
	#define ED_ASSERT(condition, STR, ...) if(!(condition)) { \
		SPDLOG_LOGGER_CRITICAL(::dbg::LogEditor, ASSERT_STR STR __VA_OPT__(,) __VA_ARGS__); BREAKPOINT; }
#else
	#define ASSERT(condition, STR, ...)
	#define AS_ASSERT(condition, STR, ...)
	#define AD_ASSERT(condition, STR, ...)
	#define RD_ASSERT(condition, STR, ...)
	#define PF_ASSERT(condition, STR, ...)
	#define ED_ASSERT(condition, STR, ...)
#endif

#ifdef ENABLE_PROFILING
	#include "dbg/profiling.hpp"

	#define PRF_SESSION(NAME) \
		::dbg::profile::session ___##NAME; ::dbg::profile::NewSession(&___##NAME, #NAME)
	#define SET_SESSION(NAME) ::dbg::profile::SetSession(&___##NAME)
	#define CLEAR_SESSION ::dbg::profile::SetSession(nullptr)

	#define START_SESSION(NAME, FILEPATH, ...) \
		::dbg::profile::StartSession(&___##NAME, FILEPATH __VA_OPT__(,) __VA_ARGS__)
	#define END_SESSION(NAME) ::dbg::profile::EndSession(&___##NAME)

	#define PRF_SC(NAME) ::dbg::profile::timer time_##__LINE__(NAME)
	#define PRF_FN ::dbg::profile::timer time_##__LINE__(__PRETTY_FUNCTION__)
#else
	#define PRF_SESSION(NAME)
	#define SET_SESSION(NAME)
	#define CLEAR_SESSION
	#define START_SESSION(NAME, FILEPATH, ...)
	#define END_SESSION(NAME)
	#define PRF_SC(name)
	#define PRF_FN
#endif

#ifdef HOLD_IDS
	#define PARAM_ID
#else
	#define PARAM_ID __attribute__((unused))
#endif

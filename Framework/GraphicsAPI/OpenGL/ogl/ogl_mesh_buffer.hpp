#pragma once
#include "Graphics/mesh_buffer.hpp"
#include "Util/list.hpp"
#include "Util/wrap_list.hpp"

namespace gfx {

struct index_buffer
{
	u32 ID;
	u64 count;
};

struct vertex_buffer
{
	u32 ID;
	vertex_buffer_layout layout;
};

class OGLVertexBuffer : public _VertexBuffer
{
public:
	OGLVertexBuffer(const vertex_buffer &vb) : _data{ vb } { }
	~OGLVertexBuffer() { }

	void Bind() const override;
	void UnBind() const override;

	void AssignData(void *, u32) override;

	const vertex_buffer_layout& layout() const override { return _data.layout; }

private:
	vertex_buffer _data;
};

class OGLMeshBuffer : public _MeshBuffer
{
public:
	OGLMeshBuffer() : _ID(0), _indexBuffer{}, _vertexBuffers{}, _vertexCount{9} { }
	OGLMeshBuffer(index_buffer, const arr::list<vertex_buffer>&, u32 vertexCount);
	~OGLMeshBuffer() override;

	void Bind() const override;
	void UnBind() const override;
	unit<_VertexBuffer> GetVertexBuffer(u8) override;

  u32 indexes() const override { return _indexBuffer.count; }
	u32 vertexes() const override { return _vertexCount; }
	u32 triangles() const override { return _indexBuffer.count / 3; }
	
	const static u8 kMaxVertexBuffers = 3;

private:
	u32 _ID;
	index_buffer _indexBuffer;
	arr::sttc_list<vertex_buffer, 3> _vertexBuffers;
	u32 _vertexCount;
};

class OGLMeshBufferLib : public _MeshBufferLib
{
public:
	OGLMeshBufferLib(size_t capacity);
	~OGLMeshBufferLib() override;

	hdl_mesh Submit(index_buffer_data ib, const arr::list<vertex_buffer_data>&) override;
	_MeshBuffer * Get(hdl_mesh) override;
	bool IsValid(hdl_mesh) const override;

private:
	arr::dync_wrap_list<OGLMeshBuffer> _vertexArrays;
	mem::dync_generations_tracker<hdl_mesh::TYPE> _generations;
};
};

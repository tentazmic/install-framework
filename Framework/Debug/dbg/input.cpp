#include "pch.h"
#include "input"

namespace inp {

const char * KeyToString(EKey key)
{
	if (key == EKey::SPACE)        return "space";
	if (key == EKey::APOSTROPHE)   return "apostrophe";
	if (key == EKey::COMMA)        return "comma";
	if (key == EKey::MINUS)        return "minus";
	if (key == EKey::PERIOD)       return "period";
	if (key == EKey::FSLASH)       return "f slash";
	if (key == EKey::NM_0)         return "num 0";
	if (key == EKey::NM_1)         return "num 1";
	if (key == EKey::NM_2)         return "num 2";
	if (key == EKey::NM_3)         return "num 3";
	if (key == EKey::NM_4)         return "num 4";
	if (key == EKey::NM_5)         return "num 5";
	if (key == EKey::NM_6)         return "num 6";
	if (key == EKey::NM_7)         return "num 7";
	if (key == EKey::NM_8)         return "num 8";
	if (key == EKey::NM_9)         return "num 9";
	if (key == EKey::SEMICOLON)    return "semicolon";
	if (key == EKey::EQUAL)        return "equal";
	if (key == EKey::A)            return "A";
	if (key == EKey::B)            return "B";
	if (key == EKey::C)            return "C";
	if (key == EKey::D)            return "D";
	if (key == EKey::E)            return "E";
	if (key == EKey::F)            return "F";
	if (key == EKey::G)            return "G";
	if (key == EKey::H)            return "H";
	if (key == EKey::I)            return "I";
	if (key == EKey::J)            return "J";
	if (key == EKey::K)            return "K";
	if (key == EKey::L)            return "L";
	if (key == EKey::M)            return "M";
	if (key == EKey::N)            return "N";
	if (key == EKey::O)            return "O";
	if (key == EKey::P)            return "P";
	if (key == EKey::Q)            return "Q";
	if (key == EKey::R)            return "R";
	if (key == EKey::S)            return "S";
	if (key == EKey::T)            return "T";
	if (key == EKey::U)            return "U";
	if (key == EKey::V)            return "V";
	if (key == EKey::W)            return "W";
	if (key == EKey::X)            return "X";
	if (key == EKey::Y)            return "Y";
	if (key == EKey::Z)            return "Z";
	if (key == EKey::LBRACKET)     return "l bracket";
	if (key == EKey::BSLASH)       return "b slash";
	if (key == EKey::RBRACKET)     return "r bracket";
	if (key == EKey::GRAVE)        return "grave `";

	if (key == EKey::ESC)          return "escape";
	if (key == EKey::ENTER)        return "enter";
	if (key == EKey::TAB)          return "tab";
	if (key == EKey::BACKSPACE)    return "backspace";
	if (key == EKey::INSERT)       return "insert";
	if (key == EKey::DELETE)       return "delete";
	if (key == EKey::RIGHT)        return "right";
	if (key == EKey::LEFT)         return "left";
	if (key == EKey::DOWN)         return "down";
	if (key == EKey::UP)           return "up";
	if (key == EKey::PAGE_UP)      return "page up";
	if (key == EKey::PAGE_UP)      return "page down";
	if (key == EKey::HOME)         return "home";
	if (key == EKey::END)          return "end";
	if (key == EKey::CAPS)         return "caps";
	if (key == EKey::SCROLL)       return "scroll";
	if (key == EKey::NUM_LOCK)     return "num lock";
	if (key == EKey::PRINT_SCREEN) return "print screen";
	if (key == EKey::PAUSE)        return "pause";
	if (key == EKey::F1)           return "fn 1";
	if (key == EKey::F2)           return "fn 2";
	if (key == EKey::F3)           return "fn 3";
	if (key == EKey::F4)           return "fn 4";
	if (key == EKey::F5)           return "fn 5";
	if (key == EKey::F6)           return "fn 6";
	if (key == EKey::F7)           return "fn 7";
	if (key == EKey::F8)           return "fn 8";
	if (key == EKey::F9)           return "fn 9";
	if (key == EKey::F10)          return "fn 10";
	if (key == EKey::F11)          return "fn 11";
	if (key == EKey::F12)          return "fn 12";
	if (key == EKey::F13)          return "fn 13";
	if (key == EKey::F14)          return "fn 14";
	if (key == EKey::F15)          return "fn 15";
	if (key == EKey::F16)          return "fn 16";
	if (key == EKey::F17)          return "fn 17";
	if (key == EKey::F18)          return "fn 18";
	if (key == EKey::F19)          return "fn 19";
	if (key == EKey::F20)          return "fn 20";
	if (key == EKey::F21)          return "fn 21";
	if (key == EKey::F22)          return "fn 22";
	if (key == EKey::F23)          return "fn 23";
	if (key == EKey::F24)          return "fn 24";
	if (key == EKey::F25)          return "fn 25";
	if (key == EKey::KP_0)         return "kp 0";
	if (key == EKey::KP_1)         return "kp 1";
	if (key == EKey::KP_2)         return "kp 2";
	if (key == EKey::KP_3)         return "kp 3";
	if (key == EKey::KP_4)         return "kp 4";
	if (key == EKey::KP_5)         return "kp 5";
	if (key == EKey::KP_6)         return "kp 6";
	if (key == EKey::KP_7)         return "kp 7";
	if (key == EKey::KP_8)         return "kp 8";
	if (key == EKey::KP_9)         return "kp 9";
	if (key == EKey::KP_DECIMAL)   return "kp decimal";
	if (key == EKey::KP_DIVIDE)    return "kp divide";
	if (key == EKey::KP_MULTIPLY)  return "kp multiply";
	if (key == EKey::KP_SUBTRACT)  return "kp subtract";
	if (key == EKey::KP_ADD)       return "kp add";
	if (key == EKey::KP_ENTER)     return "kp enter";
	if (key == EKey::KP_EQUAL)     return "kp equal";
	if (key == EKey::LSHIFT)       return "l shift";
	if (key == EKey::LCONTROL)     return "l control";
	if (key == EKey::L_ALT)        return "l alt";
	if (key == EKey::LSUPER)       return "l super";
	if (key == EKey::RSHIFT)       return "r shift";
	if (key == EKey::RCONTROL)     return "r control";
	if (key == EKey::R_ALT)        return "r alt";
	if (key == EKey::RSUPER)       return "r super";
	if (key == EKey::MENU)         return "menu";

	return "kp unknown";
}

const char * MouseButtonToString(EMouseButton button)
{
	if (button == EMouseButton::LEFT)   return "mb left";
	if (button == EMouseButton::MIDDLE) return "mb middle";
	if (button == EMouseButton::RIGHT)  return "mb right";

	if (button == EMouseButton::BTN_1)  return "mb 1";
	if (button == EMouseButton::BTN_2)  return "mb 2";
	if (button == EMouseButton::BTN_3)  return "mb 3";
	if (button == EMouseButton::BTN_4)  return "mb 4";
	if (button == EMouseButton::BTN_5)  return "mb 5";
	if (button == EMouseButton::BTN_6)  return "mb 6";
	if (button == EMouseButton::BTN_7)  return "mb 7";
	if (button == EMouseButton::BTN_8)  return "mb 8";

	return "mb unknown";
}
};

namespace dinp {

namespace {
	inp::mouse s_mouse;
	inp::keyboard s_keyboard;
};

void Update()
{
#ifndef BLD_INSTALL
	MouseUpdate(s_mouse); KeyboardUpdate(s_keyboard);
#endif
}

glm::vec2 GetMousePos() { return s_mouse.position; }
inp::EState GetKeyState(inp::EKey key) { return s_keyboard.keys[key]; }
inp::EState GetMouseButton(inp::EMouseButton button) { return s_mouse.buttons[button]; }

void RegisterKey(inp::EKey key) { s_keyboard.RegisterKey(key); }
void UnregisterKey(inp::EKey key) { s_keyboard.UnregisterKey(key); }
void RegisterKey(inp::EMouseButton button) { s_mouse.RegisterButton(button); }
void UnregisterKey(inp::EMouseButton button) { s_mouse.UnregisterButton(button);  }
};

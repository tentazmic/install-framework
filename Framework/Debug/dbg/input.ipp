#pragma once

#include <spdlog/fmt/fmt.h>

template<typename OStream>
OStream& operator<<(OStream& out, inp::EState state) noexcept
{
	if (state == inp::EState::RELEASED) out << "released";
	if (state == inp::EState::DOWN)     out << "down";
	if (state == inp::EState::HELD)     out << "held";
	if (state == inp::EState::UP)       out << "up";
	return out;
}

namespace dinp {

void Update();

glm::vec2 GetMousePos();
inp::EState GetKeyState(inp::EKey);
inp::EState GetMouseButton(inp::EMouseButton);

void RegisterKey(inp::EKey);
void UnregisterKey(inp::EKey);
void RegisterKey(inp::EMouseButton);
void UnregisterKey(inp::EMouseButton);
};

template<>
struct fmt::formatter<inp::EState> : fmt::formatter<std::string>
{
	auto format(inp::EState s, format_context &ctx) -> decltype(ctx.out())
	{
		if (s == inp::EState::RELEASED) format_to(ctx.out(), "released");
		if (s == inp::EState::DOWN)     format_to(ctx.out(), "down");
		if (s == inp::EState::HELD)     format_to(ctx.out(), "held");
		if (s == inp::EState::UP)       format_to(ctx.out(), "up");

		return format_to(ctx.out(), "state unknown");
	}
};

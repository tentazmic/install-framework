#pragma once

#include "core"
#include "debug"

#include "callback.hpp"
#include "Memory/memory.hpp"

namespace arr {

namespace {

	constexpr size_t GetMinBytes(size_t bitLength1, size_t bitLength2)
	{
		size_t bitCount = bitLength1 > bitLength2 ? bitLength2 : bitLength1;
		size_t byteCount = bitCount / 8;
		return byteCount % 8 == 0 ? byteCount : byteCount + 1;
	}

	constexpr size_t CalcByteCount(size_t length)
	{
		size_t byteCount = length / 8;
		return byteCount % 8 == 0 ? byteCount : byteCount + 1;
	}

	constexpr byte FlipBit(byte input, u8 index)
	{
		const byte mask = 1 << index;
		const byte flippedMask = ~mask;
		byte flipped = ~input;
		return (flipped & mask) | (input & flippedMask);
	}

};

struct bit
{
	bit(byte *ptr, u8 index) : _ptr(ptr), _index(index) { }

	bit(const bit& other) : _ptr(other._ptr), _index(other._index) { }
	bit(bit&& other) : _ptr(other._ptr), _index(other._index)
	{ MOVE_CONSTRUCTOR_CHECK }

	constexpr bit& operator =(const bit& other)
	{
		_ptr = other._ptr;
		_index = other._index;
		return *this;
	}

	constexpr bit& operator =(bit&& other)
	{
		MOVE_CHECK
		_ptr = other._ptr;
		_index = other._index;
		other._ptr = nullptr;
		other._index = 0;
		return *this;
	}

	constexpr bit& operator =(bool value)
	{
		const u8 mask = ~(1 << _index);
		*_ptr = value << _index | (*_ptr & mask);
		return *this;
	}
	constexpr operator bool() const { return *_ptr & (1 << _index); }

	constexpr void flip()
	{
		*_ptr = FlipBit(*_ptr, _index);
	}

private:
	byte *_ptr;
	u8 _index;
};

struct bitset
{
	bitset() : _data{ nullptr }, _length{ nullptr } { }
	bitset(byte *data, size_t *length) : _data{ data }, _length{ length } { }

	bitset(const bitset& other) = default;
	// 	: _data{ other._data }, _length{ other._length }
	// { }
	bitset(bitset&& other) = default;
	// 	: _data{ other._data }, _length{ other._length }
	// {
	// 	MOVE_CONSTRUCTOR_CHECK
	// 	other._data = nullptr;
	// 	other._length = _length;
	// }

	bitset& operator =(const bitset& other) = default;
	bitset& operator =(bitset&& other) = default;

	#define BITSET bitset
	#define BYTE_COUNT byteCount()
	#define LENGTH_PTR
	#include "_bitset_funcs.ipp"
	#undef LENGTH_PTR
	#undef BYTE_COUNT
	#undef BITSET

	size_t byteCount() const { return CalcByteCount(*_length); }

	byte * data() { return _data; }
	const byte * data() const { return _data; }

	size_t length() const { return *_length; }

private:
	byte *_data;
	size_t *_length;
};

	// &, |, ^, ~, ==
template<size_t LEN>
struct sttc_bitset
{
	sttc_bitset() { }

	sttc_bitset(const bitset& other)
	{
		size_t length = BYTE_COUNT > other.byteCount() ? other.byteCount() : BYTE_COUNT;
		std::memcpy(&_data[0], other.data(), length);
	}

	sttc_bitset(const sttc_bitset<LEN>& other) = default;
	sttc_bitset(sttc_bitset<LEN>&& other) = default;

	sttc_bitset<LEN>& operator=(const sttc_bitset<LEN>& other) = default;
	sttc_bitset<LEN>& operator=(sttc_bitset<LEN>&& other) = default;

	#define BITSET sttc_bitset<LEN>
	#include "_bitset_funcs.ipp"
	#undef BITSET

	INLINE bitset get() { return bitset(&_data[0], &_length); }
	INLINE const bitset get() const { return bitset(&_data[0], &_length); }

	INLINE size_t byteCount() const { return BYTE_COUNT; }

private:
	constexpr static size_t BYTE_COUNT = CalcByteCount(LEN);
	byte _data[BYTE_COUNT];
	size_t _length = LEN;
};

struct dync_bitset
{
	using ALLOC = void * (*)(size_t size);

	dync_bitset() : _bitset() { }
	dync_bitset(clbk::delegate<void * (size_t)> fnAlloc, clbk::delegate<void (void*)> fnFree)
		: _fnAlloc(fnAlloc), _fnFree(fnFree), _bitset() { }
	dync_bitset(clbk::delegate<void * (size_t)> fnAlloc, clbk::delegate<void (void*)> fnFree, size_t length)
		: _fnAlloc(fnAlloc), _fnFree(fnFree) { initialise(length); }

	~dync_bitset()
	{
		Free();
	}

	dync_bitset(const dync_bitset& other) = delete;
	dync_bitset(dync_bitset&& other) = default;

	dync_bitset& operator =(const dync_bitset& other) = delete;
	dync_bitset& operator =(dync_bitset&& other) = default;

	void initialise(size_t length)
	{
		Free();

		if (length == 0)
		{
			_bitset = bitset();
			return;
		}

		const size_t size = sizeof(size_t) + CalcByteCount(length);
		size_t *data = (size_t *)_fnAlloc(size);
		if (data)
		{
			size_t *count = data;
			*count = length;
			_bitset = bitset(RCAST(byte *)(data + 1), count);
			return;
		}

		LG_ERR("Failed Bitset Allocation of {}B", size);
		_bitset = bitset();
	}

	INLINE bitset get() { return _bitset; }
	INLINE const bitset get() const { return _bitset; }

	INLINE friend bool operator ==(const dync_bitset& A, const dync_bitset& B) { return A.get() == B.get(); }
	INLINE friend bool operator !=(const dync_bitset& A, const dync_bitset& B) { return A.get() != B.get(); }

#define OPERATOR(OP) \
INLINE dync_bitset& operator OP##=(const bitset& other) { _bitset &= other; return *this; } \
INLINE dync_bitset& operator OP##=(const dync_bitset& other) { _bitset &= other.get(); return *this; }

	OPERATOR(&)
	OPERATOR(|)
	OPERATOR(^)

#undef OPERATOR

	INLINE bit at(size_t index) { return _bitset.at(index); }
	INLINE const bit at(size_t index) const { return _bitset.at(index); }

	INLINE bit operator [](size_t index) { return _bitset[index]; }
	INLINE const bit operator [](size_t index) const { return _bitset[index]; }

	INLINE void flip() { _bitset.flip(); }
	INLINE void clear() { _bitset.clear(); }

private:
	void Free()
	{
		if (!_bitset.data())
			return;

		size_t *count = RCAST(size_t *)(_bitset.data());
		count--;
		_fnFree(count);
	}

	clbk::delegate<void * (size_t)> _fnAlloc;
	clbk::delegate<void (void *)> _fnFree;
	bitset _bitset;
};

inline dync_bitset HeapBitset()
{
	return dync_bitset(clbk::delegate<void * (size_t)>::Create<malloc>(), clbk::delegate<void (void *)>::Create<free>());
}
inline dync_bitset HeapBitset(size_t length)
{
	return dync_bitset(clbk::delegate<void * (size_t)>::Create<malloc>(), clbk::delegate<void (void *)>::Create<free>(), length);
}

inline dync_bitset StackBitset()
{
	return dync_bitset(clbk::delegate<void * (size_t)>::Create<mem::StackAllocate>(), clbk::delegate<void (void *)>::Create<mem::StackFree>());
}
inline dync_bitset StackBitset(size_t length)
{
	return dync_bitset(clbk::delegate<void * (size_t)>::Create<mem::StackAllocate>(), clbk::delegate<void (void *)>::Create<mem::StackFree>(), length);
}

};

#include "pch.h"
#include "_ogl.hpp"
#include "ogl_frame_buffer.hpp"
#include "debug"

namespace gfx {

OGLFrameBuffer::OGLFrameBuffer(const frame_buffer& frame_buffer)
	: _data(frame_buffer), _ID(0), _att_Colour(0), _att_Depth(0)
{
	Recreate();
}

OGLFrameBuffer::~OGLFrameBuffer()
{
	OGL_CHECK(glDeleteFramebuffers(1, &_ID))
	OGL_CHECK(glDeleteTextures(1, &_att_Colour))
	OGL_CHECK(glDeleteTextures(1, &_att_Depth))
}

void OGLFrameBuffer::Recreate()
{
	if (_ID)
	{
		OGL_CHECK(glDeleteFramebuffers(1, &_ID))
		OGL_CHECK(glDeleteTextures(1, &_att_Colour))
		OGL_CHECK(glDeleteTextures(1, &_att_Depth))
	}

	OGL_CHECK(glCreateFramebuffers(1, &_ID))
	OGL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, _ID))

	// Colour
	OGL_CHECK(glCreateTextures(GL_TEXTURE_2D, 1, &_att_Colour))
	OGL_CHECK(glBindTexture(GL_TEXTURE_2D, _att_Colour))
	OGL_CHECK(
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
		             _data.width, _data.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr)
	)
	OGL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR))
	OGL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR))
	OGL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	                                 GL_TEXTURE_2D, _att_Colour, 0))

	// Depth
	OGL_CHECK(glCreateTextures(GL_TEXTURE_2D, 1, &_att_Depth))
	OGL_CHECK(glBindTexture(GL_TEXTURE_2D, _att_Depth))
	// OGL_CHECK(glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_STENCIL, _data.width, _data.height))
	OGL_CHECK(
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_STENCIL,
		             _data.width, _data.height, 0,
		             GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL))
	OGL_CHECK(
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D,
		                       _att_Depth, 0)
	)

	ASSERT(
		glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE,
		"Frame buffer is not complete"
	);

	OGL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0))
}

void OGLFrameBuffer::Bind()
{
	OGL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, _ID))
	OGL_CHECK(glViewport(0, 0, _data.width, _data.height))
}
void OGLFrameBuffer::UnBind() { OGL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0)) }

void OGLFrameBuffer::Resize(u32 width, u32 height)
{
	_data.width = width;
	_data.height = height;
	Recreate();
}


OGLFrameBufferLib::OGLFrameBufferLib(size_t capacity)
	: _frameBuffers(capacity), _generations(capacity)
{ }

OGLFrameBufferLib::~OGLFrameBufferLib()
{
	_frameBuffers.clear();
}

hdl_frame_bfr OGLFrameBufferLib::Create(const frame_buffer& frameBuffer)
{
	size_t index = _frameBuffers.head();
	new ((void *)_frameBuffers.next_slot()) OGLFrameBuffer(frameBuffer);
	return hdl_frame_bfr{
		(hdl_frame_bfr::TYPE) index, _generations.tick(index)
	};
}

_FrameBuffer * OGLFrameBufferLib::Get(hdl_frame_bfr handle)
{
#ifdef ENABLE_ASSERTS
	if (! IsValid(handle))
	{
		hdl_frame_bfr::TYPE got = handle.generation;
		RD_ERR("Invalid Frame Buffer Access GOT {} EXPECT {}",
			got, _generations[handle.index]
			);
		return nullptr;
	}
#endif
	return (_FrameBuffer *) &_frameBuffers[handle.index];
}
bool OGLFrameBufferLib::IsValid(hdl_frame_bfr handle) const
{
	return handle.generation == _generations[handle.index];
}
};

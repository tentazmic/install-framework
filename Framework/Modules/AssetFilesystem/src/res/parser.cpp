#include "pch.h"
#include "parser.hpp"

#include "debug"

namespace res {

parser::parser(const char *name, std::istream& stream)
	: _name(name), _stream(stream), _lineNumber(1), _linePos(1), _streamMax(0)
{
	size_t pos = _stream.tellg();
	_stream.seekg(0, std::ios::end);
	_streamMax = _stream.tellg();
	_stream.seekg(pos, std::ios::beg);
}

char parser::next()
{
	char c = _stream.get();
	_linePos++;
	if (c == '\n')
	{
		_lineNumber++;
		_linePos = 1;
	}
	return c;
}

char parser::peek() { return _stream.peek(); }

void parser::back()
{
	_stream.unget();
	_linePos--;
	if (_stream.peek() != '\n')
		return;

	seek(stream_pos());
}

char parser::until(char c)
{
	char n = next();
	while (!at_end() && n != c) n = next();

	return at_end() ? 0 : n;
}

void parser::skip_whitespace()
{
	char n = peek();
	while (!at_end() && isspace(n))
	{
		next();
		n = peek();
	}
}

void parser::seek(size_t pos)
{
	if (pos == stream_pos())
		return;

	if (pos < stream_pos())
	{
		_lineNumber = 1;
		_linePos = 1;
		_stream.seekg(0, std::ios::beg);
	}

	for (size_t i = stream_pos(); i < pos; i++)
		next();
}

bool parser::read_until_whitespace(char *buffer, size_t length)
{
	size_t i = 0;
	for (; i < length - 1; i++)
	{
		char c = peek();
		if (isspace(c))
			break;

		buffer[i] = next();
	}

	buffer[i] = '\0';
	return !isspace(peek());
}

bool parser::read_length(size_t length, char *buffer, size_t buffSize)
{
	size_t i = 0;
	size_t limit = std::min(length, buffSize - 1);
	for (; i < limit; i++)
	{
		buffer[i] = next();
		if (at_end())
			break;
	}

	buffer[i] = '\0';
	return i == limit;
}

bool parser::at_end() const
{
	return _stream.eof() || stream_pos() == _streamMax;
}

bool parser::is_next_string(const char *str)
{
	size_t spos = stream_pos();
	size_t linePos = line_pos();
	size_t lineNum = line_number();

	bool failed = false;
	for (size_t i = 0; i < strlen(str); i++)
	{
		if (next() == str[i]) continue;

		failed = true;
		break;
	}

	if (failed)
	{
		_stream.seekg(spos);
		_linePos = linePos;
		_lineNumber = lineNum;
		return false;
	}

	return true;
}
};

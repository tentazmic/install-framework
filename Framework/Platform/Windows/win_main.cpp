#include "pch.h"

#include <GLFW/glfw3.h>
#include "debug"
#include "dsound.hpp"

#ifdef GAPI_OPENGL
	#include "ogl/ogl_render_api.hpp"
#endif

#include "app/application.hpp"
#include "Util/list.hpp"

#ifdef INSTALL_USER
	#include "instl/install.hpp"
	#include "instl/install_driver.hpp"
#endif

#define FLUSH_RATE 30

namespace {
	GLFWwindow *s_window;

	const char *s_title = PRJ_NAME;

	const struct {
		u32 windowWidth  = 1280;
		u32 windowHeight = 720;
		bool vSync = true;
		app::EGraphicsAPI graphicsAPI = app::EGraphicsAPI::OPENGL;
	} kStarting;

	u32 s_width;
	u32 s_height;
	bool s_vSync;
	app::EGraphicsAPI s_graphicsAPI;

	arr::sttc_list<u8, 4> s_controllers;

	rdr::_RenderAPI * CreateRenderAPI(app::EGraphicsAPI api);
};

namespace app {

	bool alive = true;
	bool running = true;
	bool nextVSync = true;

	bool IsAlive() { return alive; }
	bool IsRunning() { return running; }
	void Kill() { alive = false; }
	void Restart() { running = false; }

	void * NativeWindow() { return s_window; }
	glm::uvec2 WindowDimensions() { return glm::uvec2(s_width, s_height); }

	bool VSyncEnabled() { return s_vSync; }
	void SetVSync(bool enabled) { nextVSync = enabled; }
};

#include "glfw/glfw_window.ipp"
#include "glfw/glfw_input.ipp"

#include <Windows.h>
// A macro defined Windows.h that prevents glfw::CreateWindow
// from being called correctly
#undef CreateWindow

#include "Application/tick.hpp"
#include "Util/list.hpp"
#include "dll_handle.hpp"

namespace {
	const char *s_fullExecutablePath = NULL;

#ifdef INSTALL_USER
	arr::dync_list<dll_handle> s_installs;
	bool FindInstallDLLs();
#endif

	DWORD WINAPI thrd_Audio(LPVOID);
};

int main(void)
{
	LG_INIT;
	// Get some config from the launcher
	// process the command line arguments

	{
		DWORD lenBuffer = 256;
		DWORD trueLength = 0;
		char *buffer = NULL;
		while (!buffer)
		{
			buffer = RCAST(char *)(malloc(lenBuffer));
			trueLength = GetModuleFileNameA(NULL, buffer, lenBuffer);
			if (trueLength == lenBuffer && GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				lenBuffer *= 2;
				free(buffer);
				buffer = NULL;
				// SetLastError(0);
			}
		}

		if (trueLength == lenBuffer)
		{
			s_fullExecutablePath = buffer;
		}
		else
		{
			//                                         NULL terminatior
			char *path = RCAST(char *)(malloc(trueLength + 1));
			strcpy(path, buffer);
			free(buffer);
			s_fullExecutablePath = path;
		}
	}

	s_width  = kStarting.windowWidth;
	s_height = kStarting.windowHeight;
	s_vSync = kStarting.vSync;
	s_graphicsAPI = kStarting.graphicsAPI;

#ifdef INSTALL_USER
	// Find Installs
	if (!FindInstallDLLs())
		s_installs.initialise(0);

	instl::install_driver installDriver;
	instl::FindInstalls(installDriver);
#endif

	// By setting app.running to false and leaving app.alive at true
	// we get an application restart
	while (app::alive)
	{
		app::running = true;

		// Doing this here, rather than in the top level of main
		// means that the window will be closed and then recreated upon
		// restarting, not sure if I want one or the other
		// These creation hints are to prevent the window being dragged or resized
		glfw::CreateWindow();
		glfwSetWindowPos(s_window, 100, 100);

		inp::PopulateControllers();

		// initialise audio
		// TODO Someway to tell it not to initialise audio
		dsound::Initialise(48000, 2.0f, 0.5f);

		rdr::_RenderAPI *api = CreateRenderAPI(s_graphicsAPI);
#ifdef INSTALL_USER
	instl::SetDriver(&installDriver);
#endif

		f64 startTime = glfwGetTime();
		f64 lastTime = startTime;
		GameStartup(app::startup_config{ .api = api }, dsound::GetBuffer());

		// dsound::Play();
		DWORD tidAudio;
		HANDLE hAudio = CreateThread(
			NULL,
			0,
			thrd_Audio,
			nullptr,
			0,
			&tidAudio
		);

		u32 frame = 0;
		PRF_SESSION(Update);
		PRF_SESSION(Render);

		while (app::running && app::alive)
		{
			frame++;
			f64 time = glfwGetTime();
			tik::frame_stamp fs {
				.frame = frame,
				.deltaTime = SCAST(f32)(time - lastTime),
				.timeSinceStart = SCAST(f32)(time - startTime)
			};

			lastTime = time;

#ifdef ENABLE_PROFILING
/*
			if (frame % FLUSH_RATE == 1)
			{
				if (frame != 1)
				{
					END_SESSION(Update);
					END_SESSION(Render);
				}

				char sessionName[14];
				sprintf(sessionName, "Frame %d", frame);

				char fileName[50];
				sprintf(fileName, "00Output/GameUpdate_f%d-%d.json", frame, frame + FLUSH_RATE);
				START_SESSION(Update, fileName);
				sprintf(fileName, "00Output/GameRender_f%d-%d.json", frame, frame + FLUSH_RATE);
				START_SESSION(Render, fileName);
			}
			*/
#endif

			// Did profiling for the below two functions, turns out these
			// two functions are too fast to matter for the varied frame times
			//

			// Trigger a thread for each of these
			GameUpdate(fs, s_controllers.get());
			GameRender();

			//GameStreamAudio(dsound::GetBuffer());
			//dsound::Update();

			// TODO On Windows moving and resizing the window blocks the process,
			// leading to errors caused by very long, unexpected thread
			// blockages
			glfwSwapBuffers(s_window);
			glfwPollEvents();

			if (app::nextVSync != s_vSync)
				glfw::SetVSync(app::nextVSync);

			GameFireEvents();

			// TODO w/ Text Rendering
			// Replace with a debug only info bar at the bottom of the screen
			// that shows ms/frame, fps, etc
			// f32 msPerFrame = fs.deltaTime * 1000.0f;
			// f32 fps = 1.0f / fs.deltaTime;
			// PF_TRC("F{}: Timing {:.3f}ms | {:.1f}FPS", frame, msPerFrame, fps);
		}

		WaitForSingleObject(hAudio, 30);
		dsound::Shutdown();
		GameShutdown();
		delete api;

		glfw::DestroyWindow();
	}

	LG_SHUTDOWN;
	return 0;
}

namespace {
	rdr::_RenderAPI * CreateRenderAPI(app::EGraphicsAPI api)
	{
		switch (api)
		{
	#ifdef GAPI_OPENGL
			case app::EGraphicsAPI::OPENGL:
				return new rdr::OGLRenderAPI();
	#endif
		}

		PF_ERR("{} Render API not available", api);
		return nullptr;
	}

#ifdef INSTALL_USER
	bool FindInstallDLLs()
	{
#ifdef ENABLE_ASSERTS
		if (!s_fullExecutablePath)
		{
			PF_ERR("FAIL Find Installs without EXE Path");
			return false;
		}
#endif

		DWORD lenExe = strlen(s_fullExecutablePath);
		DWORD lastSepPos = lenExe;
		while (lastSepPos != 0)
		{
			if (s_fullExecutablePath[lastSepPos] == '\\')
				break;

			lastSepPos--;
		}

		char * installPath = RCAST(char *)(malloc(lastSepPos + sizeof("Installs") + 1));
		strncpy(installPath, s_fullExecutablePath, lastSepPos + 1);
		strcpy(installPath + lastSepPos + 1, "Installs");

		if (!fs::exists(installPath))
		{
			PF_DBG("No Installs Found");
			return false;
		}

		u32 count = 0;
		for (const fs::directory_entry& entry : fs::recursive_directory_iterator(installPath))
		{
			if (entry.is_directory() || entry.path().extension().string().compare(INSTALL_EXT) != 0)
				continue;

			count++;
		}

		s_installs.initialise(count);
		for (const fs::directory_entry& entry : fs::recursive_directory_iterator(installPath))
		{
			if (entry.is_directory() || entry.path().extension().string().compare(INSTALL_EXT) != 0)
				continue;

			s_installs.emplace(entry.path().string().c_str());
			if (!s_installs.back().IsValid())
			{
				PF_ERR("{} Not a DLL", entry.path().string());
				s_installs.pop();
				continue;
			}

			if (!s_installs.back().GetFunction("instl_Information"))
			{
				PF_ERR("{} Not an Install", entry.path().string());
				s_installs.pop();
				continue;
			}

			// Use the install info to categorise and filter the install
		}

		free(installPath);
		return true;
	}
#endif

	DWORD WINAPI thrd_Audio(LPVOID)
	{
		dsound::Play();

		while (app::running && app::alive)
		{
			GameStreamAudio(dsound::GetBuffer());
			dsound::Update();
		}

		return 0;
	}
};

#ifdef INSTALL_USER
namespace instl {

void FindInstalls(install_driver &driver)
{
	driver._startups.initialise(s_installs.count());
	driver._shutdowns.initialise(s_installs.count());
	driver._loadScenes.initialise(s_installs.count());

	FOR (it, s_installs)
	{
		install_info *info
		  = RCAST(install_info *)(it->GetFunction(id_Information));
		PF_INF("    Loading Install Functions from {}", info->name);

		fn_Startup startup
		  = RCAST(fn_Startup)(it->GetFunction(id_Startup));
		if (startup)
		{
			driver._startups.push(startup);
			PF_INF(id_Startup);
		}

		fn_Shutdown shutdown
		  = RCAST(fn_Shutdown)(it->GetFunction(id_Shutdown));
		if (shutdown)
		{
			driver._shutdowns.push(shutdown);
			PF_INF(id_Shutdown);
		}

		fn_LoadScene loadScene
		  = RCAST(fn_LoadScene)(it->GetFunction(id_LoadScene));
		if (loadScene)
		{
			driver._loadScenes.push(loadScene);
			PF_INF(id_LoadScene);
		}
	}
}
};
#endif

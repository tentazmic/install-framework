#pragma once
#include <imgui.h>
#include "math"
#include "panel.hpp"
#include "ECS/ecs.hpp"
#include "rdr/renderer.hpp"
#include "Graphics/frame_buffer.hpp"
#include "Memory/ref.hpp"
#include "Memory/arena.hpp"
#include "edt/Assets/scene.hpp"

#include "edt/Assets/watcher.hpp"
#include "Util/bitset.hpp"

// A viewport panel has one associated scene
// You can open multiple scene's at once you just create
// another panel

// You can divide a panel into multiple screens
// This is what the window struct is for

namespace edt::panel::viewport {

// TODO
// Will need to do this differently.
// Rather than each window getting its own World, all windows occupy
// the same Tools World. The tools specific to window will be the child
// of an entity which will be enabled when it is time to call the machine
// on that window and deactivated otherwise.
// Requirements:
// Entity Enabling and Disabling
// Entity Parenting
// Entity Messages: OnEnable, OnDisable
struct window
{
	math::rect region;
	ecs::World *tools;
	gfx::_FrameBuffer *screen;

	math::rect GetWindowRect(const ImVec2& viewportPos, const ImVec2& viewportSize) const;
};

class PnlViewport : public _Panel
{
public:
	const static u16 MAX_NUM = 0;

	PnlViewport(rdr::_RenderAPI *api);
	PnlViewport(rdr::_RenderAPI *api, edt::asset::scene *scene);
	~PnlViewport() override;

	void Update(f32 deltaTime) override;
	void OnEvent(evt::event& e) override;
	void PreGUI() override;
	void GUI() override;
	void PostGUI() override;

	void OnFocusGained() override;
	void OnFocusLost() override;

	inline bool HasWorld() const { return _scene && _scene->world; }

private:
	void OpenScene(const edt::asset::scene& scene);

	void CreateWindow(const math::rect& rect);
	bool IsWindowHovered(const window& w);

	rdr::twod::Renderer2D _renderer;
	edt::asset::scene *_scene;
	arr::sttc_list<window, 4> _windows;
	mem::Arena<mem::pool_allocator, mem::no_bounds_checking> _arTools;
	gfx::_FrameBufferLib *_frameBuffers;

	edt::asset::watcher _watcher;
	arr::dync_bitset _activeEntries;
	size_t _selection;
};
};

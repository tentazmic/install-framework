#pragma once
#include <glad/glad.h>

#ifdef ENABLE_LOG

// Can't wrap this in braces as sometimes the exression is an assignment
// to a variable that is used else where
#define OGL_CHECK(EXPR) \
EXPR;                    \
while (GLenum error = glGetError()) \
	RD_TRC("[OpenGL Error] {} {} {} ({})", __FILE__, __LINE__, #EXPR, error);

#else

#define OGL_CHECK(EXPR)

#endif

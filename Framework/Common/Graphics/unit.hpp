#pragma once

// Simple scoped pointer reference, intended for use
// with the data backed by a handle

namespace gfx {

template<typename T>
struct unit
{
	unit() : data(nullptr) { }
	unit(T *t) : data(t) { }
	~unit() { delete data; }

	unit(const unit&) = delete;
	unit(unit&& other) : data(other.data)
	{
		MOVE_CONSTRUCTOR_CHECK
		other.data = nullptr;
	}

	unit & operator =(const unit&) = delete;
	unit & operator =(unit&& other)
	{
		MOVE_CHECK
		data = other.data;
		other.data = nullptr;
		return *this;
	}

	T * operator ->() { return data; }
	const T * operator ->() const { return data; }

	T *data;
};

template<typename T, typename ... Args>
unit<T> make_unit(Args&& ... args) { return unit(new T(std::forward<Args>(args)...)); }
};

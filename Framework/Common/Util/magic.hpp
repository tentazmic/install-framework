#pragma once

template<size_t X, size_t Y>
struct same_num : std::false_type {};

template<size_t X>
struct same_num<X, X> : std::true_type {};

template<bool B> struct bool_type { };

template<typename T>
struct type_and_count { };

template<typename T, size_t N>
struct type_and_count<T[N]>
{
	typedef T TYPE;
	static const size_t COUNT = N;
};

template<typename T>
struct pointer_extract { using TYPE = T; };

template<typename T>
struct pointer_extract<T*> { using TYPE = typename pointer_extract<T>::TYPE; };

// using pod_type = bool_type<true>;
// using non_pod_type = bool_type<false>;

// template<typename T>
// struct is_pod
// {
// 	static const bool VALUE = false;
// };

// template<typename T>
// struct is_pod_t
// {
// 	typedef bool_type<is_pod<T>::VALUE> TYPE;
// };

// #define IS_POD(TYPE)  template<> struct is_pod<TYPE> { static const bool VALUE = true; };

// IS_POD(void)
// IS_POD(bool)

// IS_POD(char)
// IS_POD(unsigned char)
// IS_POD(short)
// IS_POD(unsigned short)
// IS_POD(int)
// IS_POD(unsigned int)
// IS_POD(long)
// IS_POD(unsigned long)

// IS_POD(float)
// IS_POD(double)

// #undef IS_POD

// template<typename T>
// struct is_pod<T*> { static const bool VALUE = is_pod<T>::VALUE; };


// static_assert(is_pod<void *>::VALUE, "Should not be false");

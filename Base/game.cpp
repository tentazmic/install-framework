#include "pch.h"

#include "app/application.hpp"
#include "Application/events.hpp"
#include "Application/tick.hpp"

#include "rdr/renderer.hpp"

#include "res/assets.hpp"
#include "res/audio.hpp"

#include "UI/font.hpp"
#include "res/font.hpp"

#include "Audio/audio.hpp"

#include "input"

#include "instl/install_driver.hpp"

#include "Util/list.hpp"

#include "game_data.hpp"

const char *res::g_AssetRoot = PRJ_ROOT;
const char *res::g_LocalAssetRoot = LOCAL_PRJ_ROOT;

#ifdef DEV_WINDOW
  #include "dev_window.hpp"
  #define DWIN(x) x
#else
  #define DWIN(x)
#endif

namespace {
	rdr::Renderer2D *s_renderer;

	res::audio_stream s_clip;
	res::FontLibrary *s_fonts;

	res::hdl_font s_font;

	evt::dispatcher *s_dispatcher;

	instl::install_driver *s_driver;

	inp::gamepad s_gamepad;

	arr::sttc_list<game::sprite, 8> s_sprites;
	arr::sttc_list<game::camera, 2> s_cameras;
	u8 s_mainCamera = 0;

	void HandleWindowClose(evt::event& e)
	{
		app::Kill();
	}

	void HandleMouseScroll(evt::event &e)
	{
		game::camera &c = s_cameras[s_mainCamera];
		f32 zoom = c.zoom - e.offset.y * .25f;
		zoom = glm::max(zoom, .25f);
		c.zoom = zoom;
		c.projection = glm::ortho(
			-(c.zoom * c.aspectRatio), c.zoom * c.aspectRatio,
			-c.zoom, c.zoom
		);
	}

	void HandleWindowResize(evt::event& e)
	{
#ifdef ENABLE_CHECKS
		if (!s_renderer)
		{
			LG_ERR("Window Resize Event but s_renderer has not been set");
			return;
		}
#endif

		FOR(it, s_cameras)
		{
			it->aspectRatio = SCAST(f32)(e.size.x) / SCAST(f32)(e.size.y);
			it->projection = glm::ortho(
				-(it->zoom * it->aspectRatio), it->zoom * it->aspectRatio,
				-it->zoom, it->zoom
			);
		}
	}
};

void instl::SetDriver(instl::install_driver *id) { s_driver = id; }

void GameStartup(const app::startup_config& config, snd::buffer buffer)
{
	rdr::library_sizes sizes;
	s_renderer = new rdr::Renderer2D(config.api, sizes, 64, 64, { .2f, .2f, .2f, 1 });

	s_dispatcher = new evt::dispatcher(256);
	s_dispatcher->OnWindowClose += evt::signature::del::Create<&HandleWindowClose>();
	s_dispatcher->OnWindowResized += evt::signature::del::Create<&HandleWindowResize>();
	s_dispatcher->OnMouseScrolled += evt::signature::del::Create<&HandleMouseScroll>();
	// Look through the old version and see where we were adding events to the dispatcher

  DWIN(dbg::dwin::Initialise());

	instl::pointers p{ .assets = nullptr };
	s_driver->Startup(p);

	s_fonts = new res::FontLibrary(s_renderer->textures(), 8);
	s_font = s_fonts->Load(AssetPath(Fonts/roboto.ttf));

	game::sprite s;
	gfx::texture tex;

	// Checkerboard
	s = game::sprite{};
	s.position = glm::vec2(0, 0);
	s.scale = glm::vec2(10.f, 10.f);
	res::LoadTexture(AssetPath(Textures/checkerboard.png), &tex);
	s.texture = s_renderer->textures()->Load(tex);
	res::DeleteTexture(tex);
	s.colour = glm::vec4(1, .6f, .6f, 1);
	s.tilingFactor = 10;
	s_sprites.push(s);

	s = game::sprite{};
	// Colour Square
	s.position = glm::vec2(-1, 3);
	s.rotation = glm::radians(30.f);
	s.scale = glm::vec2(1.f, 1.f);
	s.moveable = true;
	s.colour = glm::vec4(.333f, 1, .9f, 1);
	s_sprites.push(s);

	s = game::sprite{};
	// Texture 1
	s.position = glm::vec2(4, -2);
	s.rotation = glm::radians(-90.f);
	s.scale = glm::vec2(2.f, 2.f);
	res::LoadTexture(AssetPath(Textures/logo_render.png), &tex);
	s.texture = s_renderer->textures()->Load(tex);
	res::DeleteTexture(tex);
	s.colour = glm::vec4(.1f, 1.f, .1f, .8f);
	s.tilingFactor = 3.f;
	s.parent = 1;
	s_sprites.push(s);

	s = game::sprite{};
	// Texture Fonts
	s.position = glm::vec2(-3, -4.4f);
	s.scale = glm::vec2(5.f, 5.f);
	s.texture = s_fonts->Get(s_font).texture();
	s_sprites.push(s);

	game::camera c;
	c.position = glm::vec2();
	c.aspectRatio = 1.7777f;
	c.zoom = 4;
	c.projection = glm::ortho(-(c.zoom * c.aspectRatio), c.zoom * c.aspectRatio, -c.zoom, c.zoom, .0f, 1.f);
	c.moveSpeed = 5;
	s_cameras.push(c);

	c = game::camera{};
	c.position = glm::vec2(-4, 5);
	c.aspectRatio = 1.4f;
	c.zoom = 6;
	c.projection = glm::ortho(-(c.zoom * c.aspectRatio), c.zoom * c.aspectRatio, -c.zoom, c.zoom, .0f, 1.f);
	c.moveSpeed = 3;
	s_cameras.push(c);

	s_driver->LoadScene(s_renderer->textures(), s_sprites.get(), s_cameras.get());

	s_clip = res::LoadAudioClip(AssetPath(Music/duomo_di_sirio.wav));
	s_clip.scale = 0.05f;
	res::StreamAudioClip(buffer, s_clip);

	dinp::RegisterKey(inp::EKey::A);
	dinp::RegisterKey(inp::EKey::D);
	dinp::RegisterKey(inp::EKey::W);
	dinp::RegisterKey(inp::EKey::S);
	dinp::RegisterKey(inp::EKey::Q);
	dinp::RegisterKey(inp::EKey::E);

	dinp::RegisterKey(inp::EKey::ESC);
	dinp::RegisterKey(inp::EKey::TAB);
}

void GameShutdown()
{
  DWIN(dbg::dwin::Shutdown());

	s_driver->Shutdown();

	delete s_fonts;
	delete s_dispatcher;
	delete s_renderer;
}

void GameUpdate(tik::frame_stamp& fs, arr::list<u8> controllerIDs)
{
	dinp::Update();

	if (!controllerIDs.contains(s_gamepad.ID))
	{
		if (!controllerIDs.empty())
		{
			s_gamepad.ID = controllerIDs.front();
		}
		else
		{
			s_gamepad.clear();
		}
	}

	glm::vec2 movement(0, 0);
	if (s_gamepad.active())
	{
		inp::GamepadUpdate(s_gamepad);
		movement = s_gamepad.control;
	}

	if (dinp::GetKeyState(inp::EKey::UP).pressed())
		movement.y += 1.f;
	if (dinp::GetKeyState(inp::EKey::DOWN).pressed())
		movement.y -= 1.f;
	if (dinp::GetKeyState(inp::EKey::LEFT).pressed())
		movement.x -= 1.f;
	if (dinp::GetKeyState(inp::EKey::RIGHT).pressed())
		movement.x += 1.f;

	FOR(it, s_sprites)
	{
		if (!it->moveable)
			continue;

		if (movement == glm::vec2())
			continue;
		movement = glm::normalize(movement) * 1.5f;
		it->position += movement * fs.deltaTime;
	}

	if (dinp::GetKeyState(inp::EKey::TAB) == inp::EState::DOWN
	    || (s_gamepad.active() && s_gamepad.A == inp::EState::DOWN))
		s_mainCamera = (s_mainCamera + 1) % 2;

	if (dinp::GetKeyState(inp::EKey::ESC).pressed()
	    || (s_gamepad.active() && s_gamepad.start.pressed()))
		app::Kill();

	DWIN(dbg::dwin::Update(fs));
}

void GameRender()
{
	s_renderer->clear_screen();
	s_renderer->reset_stats();

	game::camera &c = s_cameras[s_mainCamera];
	rdr::render_target rt{
		.camera = {
			.position = c.position,
			.rotation = 0,
			.halfSize = glm::vec2(c.zoom * c.aspectRatio, c.zoom)
		},
		.screenDimensions = app::WindowDimensions(),
		.frameBuffer = gfx::hdl_frame_bfr{}
	};
	s_renderer->StartBatch(rt);

	struct transform { glm::vec2 pos; f32 rot; };
	arr::sttc_list<transform, 8> cache;

	CFOR (it, s_sprites)
	{
		rdr::quad_data qd{
			.position = it->position,
			.rotation = it->rotation,
			.size = it->scale,

			.texture = it->texture,
			.colour = it->colour,
			.tilingFactor = it->tilingFactor
		};

		if (it->parent != MAX(u32))
		{
			qd.position += cache[it->parent].pos;
			qd.rotation += cache[it->parent].rot;
		}

		bool res = s_renderer->SubmitQuad(qd);
		if (!res)
			LG_ERR("Could not add sprite to batch");
		else
			cache.push({ qd.position, qd.rotation });
	}

	ui::Font font = s_fonts->Get(s_font);
	rdr::text_data text{
		.text = {
			.font = &font,
			.string = "This is a test string."
		},
		.position = { -5, 0 },
		.rotation = 0,
		.size = glm::vec2(5, 5)
	};

	s_renderer->SubmitText(text);

	s_renderer->FlushBatch();

	DWIN(dbg::dwin::Draw(s_renderer->statistics()));
}

void GameStreamAudio(snd::buffer buffer)
{
	res::StreamAudioClip(buffer, s_clip);
}

void GamePlatformEvent(evt::event e)
{
	s_dispatcher->SubmitEvent(e);
}

void GameFireEvents()
{
	s_dispatcher->Dispatch();
}

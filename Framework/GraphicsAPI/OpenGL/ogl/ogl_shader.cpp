#include "pch.h"
#include "_ogl.hpp"
#include "ogl_shader.hpp"

#include "core"
#include "debug"

#include "res/shader.hpp"

namespace {

bool CreateProgram(const res::shader_source &source, u32 *id);
};

#ifdef ENABLE_ASSERTS
	#define VALID_CHECK if (!_valid) {  \
	RD_ERR("Shader {} is not valid", _path); return; }
#else
	#define VALID_CHECK
#endif

namespace gfx {

OGLShader::OGLShader(res::shader_source &source, const char *path)
	: _ID(0), _path(nullptr), _uniformMap()
#ifdef ENABLE_ASSERTS
	, _valid(false)
#endif
{
#ifdef ENABLE_ASSERTS
	_valid =
#endif
	CreateProgram(source, &_ID);

	size_t length = strlen(path);
	char *buffer = SCAST(char *)(malloc(length + 1));
	strcpy(buffer, path);
	buffer[length] = '\0';
	_path = buffer;
}

OGLShader::~OGLShader()
{
	OGL_CHECK(glDeleteProgram(_ID))
	if (_path)
		FREE(_path);
}

void OGLShader::Bind() const   { VALID_CHECK OGL_CHECK(glUseProgram(_ID)) }
void OGLShader::UnBind() const { VALID_CHECK OGL_CHECK(glUseProgram(0)) }

void OGLShader::UploadInt1(const char *uniform, i32 value)
	{ VALID_CHECK OGL_CHECK(glUniform1i(GetUniformLocation(uniform), value)) }
void OGLShader::UploadFloat1(const char *uniform, f32 value)
	{ VALID_CHECK OGL_CHECK(glUniform1fv(GetUniformLocation(uniform), 1, &value)) }
void OGLShader::UploadFloat2(const char *uniform, const math::vec2& value)
	{ VALID_CHECK OGL_CHECK(glUniform2fv(GetUniformLocation(uniform), 1, &value.x)) }
void OGLShader::UploadFloat3(const char *uniform, const math::vec3& value)
	{ VALID_CHECK OGL_CHECK(glUniform3fv(GetUniformLocation(uniform), 1, &value.x)) }
void OGLShader::UploadFloat4(const char *uniform, const math::vec4& value)
	{ VALID_CHECK OGL_CHECK(glUniform4fv(GetUniformLocation(uniform), 1, &value.x)) }
void OGLShader::UploadMat3(const char *uniform, const glm::mat3& matrix)
{
	VALID_CHECK
	OGL_CHECK(
		glUniformMatrix3fv(GetUniformLocation(uniform), 1, GL_FALSE,
	                     glm::value_ptr(matrix))
	)
}
void OGLShader::UploadMat4(const char *uniform, const glm::mat4& matrix)
{
	VALID_CHECK
	OGL_CHECK(
		glUniformMatrix4fv(GetUniformLocation(uniform), 1, GL_FALSE,
		                   glm::value_ptr(matrix))
	)
}

void OGLShader::UploadInt1Array(const char *uniform, i32 *values, u32 count)
{
	VALID_CHECK
	GLint loc = GetUniformLocation(uniform);
	OGL_CHECK(glUniform1iv(loc, count, values))
}

i32 OGLShader::GetUniformLocation(const char *uniform)
{
	if (!_valid)
	{
		RD_ERR("Shader {} is not valid", _path);
		return 0;
	}

	if (_uniformMap.find(uniform) != _uniformMap.end())
		return _uniformMap[uniform];

	OGL_CHECK(i32 id = glGetUniformLocation(_ID, uniform))
	_uniformMap[uniform] = id;

#if defined(ENABLE_ASSERTS) && defined(HOLD_IDS)
	if (id == -1) RD_ERR("Shader \"{}\" has no uniform \"{}\"", _path, uniform);
#endif
	return id;
}


OGLShaderLib::OGLShaderLib(size_t capacity)
	: _shaders(capacity), _generations(capacity)
{ }

OGLShaderLib::~OGLShaderLib()
{
	_shaders.clear();
}

hdl_shader OGLShaderLib::Load(const char *path)
{
#ifdef ENABLE_ASSERTS
	FOR(it, _shaders)
	{
		if (strcmp(it->GetPath(), path) != 0)
			continue;

		RD_WRN("Already loaded shader {}", path);
		hdl_shader::TYPE index = it - _shaders.begin();
		return hdl_shader{ index, _generations[index] };
	}
#endif

	res::shader_source source;
	if (!res::ParseShader(path, source))
		return hdl_shader{};

	hdl_shader::TYPE index = _shaders.head();
	new (SCAST(void *)(_shaders.next_slot())) OGLShader(source, path);
	FREE(source.vertex);
	FREE(source.pixel);
	return hdl_shader{ index, _generations.tick(index) };
}

_Shader * OGLShaderLib::Get(hdl_shader handle)
{
#ifdef ENABLE_ASSERTS
	if (! IsValid(handle))
	{
		hdl_shader::TYPE gen = _generations[handle.index];
		if (handle.generation != gen)
		{
			hdl_shader::TYPE got = handle.generation;
			RD_ERR("Invalid Shader Access | Generation Mismatch GOT {} EXPECT {}",
				got, gen);
		}
		else
		{
			hdl_shader::TYPE index = handle.index;
			RD_ERR("Invalid Shader Access AT {}", index);
		}
		return nullptr;
	}
#endif
	return DCAST(_Shader *)(&_shaders[handle.index]);
}

bool OGLShaderLib::IsValid(hdl_shader handle) const
{
	return handle.generation == _generations[handle.index]
	       && _shaders[handle.index].is_valid();
}
};

namespace {

std::string GetName(GLuint type)
{
	if (type == GL_VERTEX_SHADER) return "Vertex";
	if (type == GL_FRAGMENT_SHADER) return "Pixel";

	RD_ERR("Unknown shader of type {}", type);
	return "";
}

u32 CompileShader(GLuint type, const char *source)
{
	OGL_CHECK(u32 shader = glCreateShader(type))
	OGL_CHECK(glShaderSource(shader, 1, &source, 0))
	OGL_CHECK(glCompileShader(shader))

#ifdef ENABLE_ASSERTS
	GLint isCompiled = 0;
	OGL_CHECK(glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled))
	if (isCompiled == GL_FALSE)
	{
		GLint length = 0;
		OGL_CHECK(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length))
		GLchar *log = (GLchar *) malloc(sizeof(GLchar) * length);
		OGL_CHECK(glGetShaderInfoLog(shader, length, &length, log))
		OGL_CHECK(glDeleteShader(shader))
		RD_ERR("Failed to compile {} shader", GetName(type));
		ERR(log);
		TRC("");
		TRC("================");
		TRC(source);
		return MAX(u32);
	}
#endif
	return shader;
}

bool CreateProgram(const res::shader_source &source, u32 *id)
{
	struct { u32 vertex, pixel; } ids;
	if (source.vertex)
		ids.vertex = CompileShader(GL_VERTEX_SHADER, source.vertex);
	if (source.pixel)
		ids.pixel = CompileShader(GL_FRAGMENT_SHADER, source.pixel);

	OGL_CHECK(GLuint program = glCreateProgram())
	if (source.vertex) { OGL_CHECK(glAttachShader(program, ids.vertex)) }
	if (source.pixel) { OGL_CHECK(glAttachShader(program, ids.pixel)) }

	OGL_CHECK(glLinkProgram(program))

#ifdef ENABLE_ASSERTS
	GLint isLinked = 0;
	OGL_CHECK(glGetProgramiv(program, GL_LINK_STATUS, &isLinked))
	if (isLinked == GL_FALSE)
	{
		GLint length = 0;
		OGL_CHECK(glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length))
		GLchar *log = (GLchar *) malloc(sizeof(GLchar) * length);
		OGL_CHECK(glGetProgramInfoLog(program, length, &length, log))
		OGL_CHECK(glDeleteProgram(program))

		if (source.vertex) { OGL_CHECK(glDeleteShader(ids.vertex)) }
		if (source.pixel) { OGL_CHECK(glDeleteShader(ids.pixel)) }

		RD_ERR("Shader linking failure:\n{}", log);
		return false;
	}
#endif

	if (source.vertex) { OGL_CHECK(glDetachShader(program, ids.vertex)) }
	if (source.pixel) { OGL_CHECK(glDetachShader(program, ids.pixel)) }

	*id = program;
	return true;
}
};

#pragma once

#include "app/application.hpp"
#include "Assets/scene.hpp"
#include "Util/callback.hpp"

#include "rdr/renderer.hpp"

// Global Stuff for the Editor Application

namespace editor {

extern rdr::_RenderAPI *CurrentAPI;
extern clbk::sink<void (const edt::asset::scene&)> e_OpenScene;

extern rdr::twod::Renderer2D *Renderer;
};

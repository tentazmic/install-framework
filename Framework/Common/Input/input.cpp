#include "pch.h"
#include "input.hpp"
#include "codes.hpp"
#include "debug"

namespace inp {

f32 ApplyDeadzone(f32 input, f32 deadzone)
{
	if (glm::abs(input) < deadzone)
		return .0f;

	f32 raw = glm::abs(input);
	f32 offset = raw - deadzone;
	f32 mag = glm::min(1.0f, offset * (1.0f / (1.0f - deadzone)));
	return mag * glm::sign(input);
}

void EState::Update(bool pressed)
{
	if (value == RELEASED.value)
	{
		value = pressed ? DOWN.value : value;
	}
	else if (value == DOWN.value || value == HELD.value)
	{
		value = pressed ? HELD.value : UP.value;
	}
	else
	{
		value = pressed ? DOWN.value : RELEASED.value;
	}
}

void keyboard::RegisterKey(EKey key)
{
#ifdef ENABLE_ASSERTS
	auto it = keys.find(key);
	if (it != keys.end())
	{
		LG_ERR("KEY {} already registered", key);
		return;
	}
#endif
	keys[key] = EState::RELEASED;
}

void keyboard::UnregisterKey(EKey key)
{
#ifdef ENABLE_ASSERTS
	auto it = keys.find(key);
	if (it == keys.end())
	{
		LG_ERR("KEY {} not registered", key);
		return;
	}
#endif
	keys.erase(key);
}

void mouse::RegisterButton(EMouseButton button)
{
#ifdef ENABLE_ASSERTS
	auto it = buttons.find(button);
	if (it != buttons.end())
	{
		LG_ERR("BUTTON {} already register", button);
		return;
	}
#endif
	buttons[button] = EState::RELEASED;
}

void mouse::UnregisterButton(EMouseButton button)
{
#ifdef ENABLE_ASSERTS
	auto it = buttons.find(button);
	if (it == buttons.end())
	{
		LG_ERR("BUTTON {} not registered", button);
		return;
	}
#endif
	buttons.erase(button);
}
};

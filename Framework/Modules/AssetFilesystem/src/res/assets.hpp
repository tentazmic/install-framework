#pragma once
#include "core"
#include "math"

#include "game_data.hpp"

namespace res {

// extern const std::map<const std::string, ecs::Type> g_TypeNameMap;
extern const char *g_AssetRoot;
extern const char *g_LocalAssetRoot;

struct asset_roots { const char *global, *local; };
inline asset_roots GetRoots() { return asset_roots{ g_AssetRoot, g_LocalAssetRoot }; }

enum struct EAssetType { TEXTURE };

// Taking char * as when asset packing is introduced the fs::path s will have to be
// replaced with some sort of hash
bool LoadTexture(const char *path, gfx::texture *tex);
void DeleteTexture(const gfx::texture& tex);

const char * MakePath(const char *root, const char *path);
inline const char * GetPath(const char *path) { return MakePath(g_AssetRoot, path); }
inline const char * GetLocalPath(const char *path) { return MakePath(g_LocalAssetRoot, path); }
};

// TODO Maybe replace with user defined literals
#define AssetPath(x) ::res::GetPath( #x )
#define LocalAssetPath(x) ::res::GetLocalPath( #x )

#ifdef BLD_EDITOR
#define EditorAssetPath(x) ::res::GetLocalPath( #x )
#endif

#pragma once
#include "rdr/renderer.hpp"

#include "math"
#include "debug"

namespace rdr {

class OGLRenderAPI : public _RenderAPI
{
public:
	OGLRenderAPI();
	~OGLRenderAPI() override { }

	void SetClearColour(const glm::vec4& colour) override;
	void SetViewport(u32 x, u32 y, u32 width, u32 height) override;
	void Clear() override;
	void DrawIndexed(gfx::_MeshBuffer*, u32 count = 0) override;

	// TODO Find the max texture slots by querying the driver
	u32 max_texture_slots() const override { return 32; }

	gfx::_MeshBufferLib * CreateMeshBufferLibrary(size_t capacity) const override;
	gfx::_ShaderLib * CreateShaderLibrary(size_t capacity) const override;
	gfx::_TextureLib * CreateTextureLibrary(size_t capacity) const override;
	gfx::_FrameBufferLib * CreateFrameBufferLibrary(size_t capacity) const override;

	void FreeMeshBufferLibrary(gfx::_MeshBufferLib *) const override;
	void FreeShaderLibrary(gfx::_ShaderLib *) const override;
	void FreeTextureLibrary(gfx::_TextureLib *) const override;
	void FreeFrameBufferLibrary(gfx::_FrameBufferLib *) const override;
};
};

#pragma once
#include <imgui.h>
#include "events"
#include "Application/layer.hpp"
#include "edt/Assets/scene.hpp"

#include "edt/editor.hpp"

// TODO Remove layer stuff
// TODO Add OnRender function

namespace edt::panel {

class _Panel
{
public:
	virtual ~_Panel() { }

	void GUIRender();

	// TODO OnRender
	virtual void Update(f32 delta) { }
	virtual void OnEvent(evt::event& e) { }
	virtual void PreGUI() { }
	virtual void GUI() = 0;
	virtual void PostGUI() { }

	// virtual void OnCreate() { }
	// virtual void OnDelete() { }
	virtual void OnFocusGained() { }
	virtual void OnFocusLost() { }
	virtual void OnHoverGained() { }
	virtual void OnHoverLost() { }

	bool ToDelete() const { return _delete; }

	const char * name() const { return _name; }
	bool hovered() const { return _hovered; }
	bool focused() const { return _focused; }
	bool moved() const   { return _moved; }
	bool resized() const { return _resized; }

	bool block() const { return _focused && _hovered; }

protected:
	_Panel();
	_Panel(const char *name);

	const char *_name;
	ImVec2 _position, _prevPosition;
	ImVec2 _size, _prevSize;
	ImGuiWindowFlags _fWindow = 0;
	bool _delete  : 1,
	     _hovered : 1,
	     _focused : 1,
	     _moved   : 1,
	     _resized : 1;
};

class PanelRegistry : public app::_Layer
{
public:
	PanelRegistry(const char *name, size_t nPanel);
	~PanelRegistry() override;

	void OnAttach() override;
	void OnDetach() override;
	void OnUpdate(f32 deltaTime);
	void OnImGUIRender() override;
	void OnEvent(evt::event& e) override;

	void EndOfFrameCleanup();

	template<class T, class ...Args>
	void CreatePanel(Args&&... args);
	void AddPanel(_Panel *panel);
	void DeletePanel(_Panel *panel);

	template<class T>
	T * GetPanel();
	template<class T>
	void GetPanels(arr::list<T*>& buffer);

	_Panel * GetFocused();
	_Panel * GetHovered();

	const char * name() const { return _name; }

private:
	const char *_name;
	arr::dync_list<_Panel*> _panels;
	i16 _iFocused, _iHovered;
};

template<class T, class... Args>
void PanelRegistry::CreatePanel(Args&&... args)
{
	if (_panels.full())
	{
		ED_ERR("Panel Registry {} is FULL {}", _name, _panels.count());
		return;
	}

if constexpr (T::MAX_NUM != 0) {
	size_t count = 0;
	FOR (it, _panels)
	{
		if (DCAST(T*)(*it))
			count++;
	}

	if (count >= T::MAX_NUM)
	{
		LG_WRN("{} Already have Max Panel Count CAP {}", util::TypeName<T>(), T::MAX_NUM);
		return;
	}
}

	T *panel = new T(std::forward<Args>(args)...);
	_panels.push(panel);
}

template<class T>
T * PanelRegistry::GetPanel()
{
	FOR(it, _panels)
	{
		T *p = DCAST(T *)(*it);
		if (p) return p;
	}

	return nullptr;
}

template<class T>
void PanelRegistry::GetPanels(arr::list<T*>& buffer)
{
	if (buffer.full())
		return;

	FOR (it, _panels)
	{
		if (buffer.full())
			return;

		T *p = DCAST(T *)(*it);
		if (p) buffer.push(p);
	}
}

// TODO Access the renderer of the current viewport
// then print its stats
class PnlDetails : public _Panel
{
public:
	inline const static u16 MAX_NUM = 1;

	PnlDetails()
		: _Panel("Details") { }

	void GUI() override
	{
		if (editor::Renderer)
		{
			const rdr::statistics& stats = editor::Renderer->GetRenderStats();
			ImGui::Text("Renderer2D Stats:");
			ImGui::Text("Draw Calls: " SSIZET_FMT, stats.drawCalls);
			ImGui::Text("Tris: " SSIZET_FMT, stats.trisDrawn);
			ImGui::Text("Vertices: " SSIZET_FMT, stats.vertexCount);
			ImGui::Text("Indices: " SSIZET_FMT, stats.indexCount);
		}
		else
		{
			// TODO Message about nothing rendering
		}
	}
};
};

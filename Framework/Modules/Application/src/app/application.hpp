#pragma once

#include "core"
#include "math"
#include "debug"

#include "Audio/audio.hpp"
#include "Application/events.hpp"
#include "Application/tick.hpp"

#include "Util/list.hpp"

namespace rdr { class _RenderAPI; };

namespace app {

enum struct EGraphicsAPI { OPENGL };

struct startup_config
{
	rdr::_RenderAPI *api;
};

bool IsAlive();
bool IsRunning();
void Kill();
void Restart();

void * NativeWindow();
glm::uvec2 WindowDimensions();

bool VSyncEnabled();
void SetVSync(bool enabled);
};


void GameStartup(const app::startup_config&, snd::buffer);
void GameShutdown();
void GameUpdate(tik::frame_stamp&, arr::list<u8>);
// TODO Pass in a struct to tell it to render to a frame buffer or write to an array of pixels
void GameRender();
void GameStreamAudio(snd::buffer);
void GameFireEvents();

void GamePlatformEvent(evt::event);

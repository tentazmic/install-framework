import sys

from reflection.reflection import CMP_EXAMPLES
from reflection.ppprocess import ppp_files_only

if __name__ == '__main__':
	if len(sys.argv) < 3:
		print(f'{sys.argv[0]} usage: {sys.argv[0]} [IN FILE] [OUT FILE]')
		exit(1)

	in_file = sys.argv[1]
	out_file = sys.argv[2]
	context = {
		'COMPONENTS': CMP_EXAMPLES,
		'root_folder': 'Framework/Common'
	}
	ppp_files_only(context, in_file, out_file)

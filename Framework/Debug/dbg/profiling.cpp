#include "pch.h"
#include "profiling.hpp"

#include "debug"

namespace dbg::profile {
namespace {
	session *s_session = nullptr;
};

void NewSession(session *sess, const char *name)
{
	if (!sess)
	{
		WRN("Passed in null");
		return;
	}

	sess->numWrites = 0;
	sess->flushFrequency = 0;
	strcpy(sess->name, name);
}

void SetSession(session *sess)
{
	s_session = sess;
}

void StartSession(session *sess, const char *filepath, u32 flushFreq /* = 20 */)
{
	if (!sess)
	{
		WRN("Passed in null");
		return;
	}

	sess->numWrites = 0,
	sess->flushFrequency = flushFreq == 0 ? 1 : flushFreq;

	fs::path dir = fs::path(filepath).parent_path();
	if (!fs::exists(dir))
		fs::create_directories(dir);
	sess->output.open(filepath);
	sess->output << "{\"otherData\": {}, \"traceEvents\":[";
	sess->output.flush();
	s_session = sess;
}

void EndSession(session *sess)
{
	sess->~session();
}

void WriteProfile(const profile_result& result)
{
	if (!s_session)
	{
		WRN("no session to write to");
		return;
	}

	std::lock_guard<std::mutex> lock(s_session->lock);
	// chrome://tracing does not allowing trailing commas
	if (s_session->numWrites > 0)
		s_session->output << ",";

	s_session->output <<
R"({
"cat": "function",
"dur":)" <<
		(result.end - result.start) << ",\n\"name\": \"";

	size_t len = strlen(result.name);
	if (len > sizeof(result.name)) len = sizeof(result.name);
	for (size_t i = 0; i < len; i++)
	{
		char c = result.name[i];
		s_session->output << (c == '"' ? '\'' : c);
	}

	s_session->output << "\"" <<
R"(,
"ph": "X",
"pid": 0,
"tid": )" <<
		result.threadID
		<< ",\n" << "\"ts\": " << result.start << "}";

	s_session->numWrites++;
	if (s_session->numWrites % s_session->flushFrequency)
		s_session->output.flush();
}

};

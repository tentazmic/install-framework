#include "pch.h"
#include "res/assets.hpp"

#include <stb_image.h>
#include "core"
#include "debug"
#include "math"

#include "Memory/arena.hpp"

namespace res {

bool LoadTexture(const char *path, gfx::texture *tex)
{
	i32 width, height, nChannels;
	// This needs to be moved somewhere, not sure where
	stbi_set_flip_vertically_on_load(1);

	tex->path = path;
	tex->image = stbi_load(path, &width, &height, &nChannels, 0);
	if (!tex->image)
	{
		// MAYBE populate the texture with a blank texture?
		AS_ERR("No texture at {}", path);
		return false;
	}

	if (nChannels == 0)
		return false;
	tex->dimensions = { (u32)width, (u32)height, 0 };
	tex->format = {
		.texel = gfx::ETextureTexel::NORMALISED_UINT8, .numChannels = SCAST(u8)(nChannels)
	};
	return true;
}

void DeleteTexture(const gfx::texture& tex)
{
	stbi_image_free(tex.image);
}

const char * MakePath(const char *root, const char *path)
{
	size_t lRoot = strlen(root);
	size_t len = lRoot + strlen(path) + 2; // NULL and /
	char *fullPath = new char[len];
	strcpy(fullPath, root);
	fullPath[lRoot] = '/';
	strcpy(&fullPath[lRoot + 1], path);
	fullPath[len - 1] = '\0';
	return fullPath;
}
}; // asset

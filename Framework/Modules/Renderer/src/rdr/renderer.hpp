#pragma once

#include "debug"

#include "math"
#include "Graphics/mesh_buffer.hpp"
#include "Graphics/shader.hpp"
#include "Graphics/texture.hpp"
#include "Graphics/frame_buffer.hpp"
#include "game_data.hpp"

#include "UI/font.hpp"

namespace rdr {

class _RenderAPI
{
public:
	virtual ~_RenderAPI() { }

	virtual void SetClearColour(const glm::vec4& colour) = 0;
	virtual void SetViewport(u32 x, u32 y, u32 width, u32 height) = 0;
	virtual void Clear() = 0;
	virtual void DrawIndexed(gfx::_MeshBuffer *, u32 count = 0) = 0;

	virtual u32 max_texture_slots() const = 0;

	virtual gfx::_MeshBufferLib * CreateMeshBufferLibrary(size_t capacity) const = 0;
	virtual gfx::_ShaderLib * CreateShaderLibrary(size_t capacity) const = 0;
	virtual gfx::_TextureLib * CreateTextureLibrary(size_t capacity) const = 0;
	virtual gfx::_FrameBufferLib * CreateFrameBufferLibrary(size_t capacity) const = 0;

	virtual void FreeMeshBufferLibrary(gfx::_MeshBufferLib *) const = 0;
	virtual void FreeShaderLibrary(gfx::_ShaderLib *) const = 0;
	virtual void FreeTextureLibrary(gfx::_TextureLib *) const = 0;
	virtual void FreeFrameBufferLibrary(gfx::_FrameBufferLib *) const = 0;
};

struct library_sizes
{
	u32 meshBuffer  = 8;
	u32 shader      = 8;
	u32 texture     = 8;
	u32 frameBuffer = 2;
};

struct statistics
{
	u64 drawCalls;
	u64 trisDrawn;
	u64 vertexCount;
	u64 indexCount;
};



// 64 bytes
struct render_target
{
	struct {
		// for view matrix
		glm::vec2 position;
		f32 rotation;
		// for orthographic projection
		glm::vec2 halfSize;
	} camera;

	glm::uvec2 screenDimensions;
	gfx::hdl_frame_bfr frameBuffer;

	u32 renderMask;
};

struct batched_quad
{
	struct vertex
	{
		glm::vec2 position;
		glm::vec4 colour;
		glm::vec2 uv;
		i32 texture;
		f32 tilingFactor;
	} vertexes[4];
};

struct batched_char
{
	struct vertex
	{
		glm::vec2 position;
		glm::vec4 colour;
		glm::vec2 uv;
	} vertexes[4];
};

struct quad_data
{
	glm::vec2 position;
	f32 rotation;
	glm::vec2 size = glm::vec2(1.0f);

	gfx::hdl_texture texture = mem::make_dead<gfx::hdl_texture>();
	glm::vec4 colour = glm::vec4(1.0f);
	f32 tilingFactor = 1.0f;
};

struct text_data
{
	ui::text text;
	glm::vec2 position;
	f32 rotation;
	glm::vec2 size = glm::vec2(1.0f);
};

class Renderer2D
{
public:
	Renderer2D(_RenderAPI *, const library_sizes&, u32 numQuads, u32 numChars,
	           const glm::vec4 &clearColour = glm::vec4(1.0f));
	~Renderer2D();

	void StartBatch(const render_target&);
	void FlushBatch();

	bool SubmitQuad(const quad_data&);
	bool SubmitText(const text_data&);

  inline gfx::_TextureLib * textures() { return m_libTextures; }
	inline const ::rdr::statistics & statistics() const { return _stats; }

	inline void clear_colour(const glm::vec4 &c) { _api->SetClearColour(c); }
	inline void clear_screen() { _api->Clear(); }
	inline void reset_stats() { _stats = ::rdr::statistics{}; }

private:
	_RenderAPI *_api;

	gfx::_MeshBufferLib *m_libMeshes;
	gfx::_ShaderLib *m_libShaders;
	gfx::_TextureLib *m_libTextures;
	gfx::_FrameBufferLib *m_libFrameBuffers;

	arr::dync_list<batched_quad> _quadBatch;
	gfx::hdl_mesh _mshQuad;

	arr::dync_list<batched_char> _textBatch;
	gfx::hdl_mesh _mshText;
	gfx::hdl_texture _texFont;

	arr::dync_list<gfx::hdl_texture> _textureSlots;

	gfx::hdl_shader  _shdBatchedQuad;
	gfx::hdl_shader  _shdText;

	::rdr::statistics _stats;
};

};

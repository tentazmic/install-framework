#include "pch.h"
#include "_ogl.hpp"
#include "ogl_mesh_buffer.hpp"

#include "debug"

namespace
{
	GLenum UniformTypeToGLType(gfx::EUniformType type)
	{
		switch (type)
		{
			case gfx::EUniformType::BOOL:      return GL_BOOL;
			case gfx::EUniformType::FLOAT1:    return GL_FLOAT;
			case gfx::EUniformType::FLOAT2:    return GL_FLOAT;
			case gfx::EUniformType::FLOAT3:    return GL_FLOAT;
			case gfx::EUniformType::FLOAT4:    return GL_FLOAT;
			case gfx::EUniformType::INT1:      return GL_INT;
			case gfx::EUniformType::INT2:      return GL_INT;
			case gfx::EUniformType::INT3:      return GL_INT;
			case gfx::EUniformType::INT4:      return GL_INT;
			case gfx::EUniformType::MAT3x3:    return GL_FLOAT;
			case gfx::EUniformType::MAT4x4:    return GL_FLOAT;
			case gfx::EUniformType::NONE:      break;
		}

		ASSERT(false, "Unknown EUniformType");
		return 0;
	}
};

namespace gfx {

void OGLVertexBuffer::Bind() const { OGL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, _data.ID)) }
void OGLVertexBuffer::UnBind() const { OGL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0)) }

void OGLVertexBuffer::AssignData(void *data, u32 size)
{
	OGL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, _data.ID))
	OGL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, 0, size, data))
}


OGLMeshBuffer::OGLMeshBuffer(index_buffer ib, const arr::list<vertex_buffer>& vbs, u32 vertexCount)
	: _ID(0), _indexBuffer(ib), _vertexBuffers(vbs), _vertexCount{vertexCount}
{
	OGL_CHECK(glCreateVertexArrays(1, &_ID))
}

OGLMeshBuffer::~OGLMeshBuffer() { OGL_CHECK(glDeleteVertexArrays(1, &_ID)) }

void OGLMeshBuffer::Bind() const { OGL_CHECK(glBindVertexArray(_ID)) }

void OGLMeshBuffer::UnBind() const { OGL_CHECK(glBindVertexArray(0)) }

unit<_VertexBuffer> OGLMeshBuffer::GetVertexBuffer(u8 index)
{
	_VertexBuffer *buf = nullptr;
	if (index < _vertexBuffers.count())
		buf = new OGLVertexBuffer(_vertexBuffers[index]);
	return unit(buf);
}



OGLMeshBufferLib::OGLMeshBufferLib(size_t capacity)
	: _vertexArrays(capacity), _generations(capacity)
{ }

OGLMeshBufferLib::~OGLMeshBufferLib()
{
	_vertexArrays.clear();
}

hdl_mesh OGLMeshBufferLib::Submit(index_buffer_data ib, const arr::list<vertex_buffer_data>& vbs)
{
	size_t count = vbs.count();
	if (count > OGLMeshBuffer::kMaxVertexBuffers)
	{
		u8 m = OGLMeshBuffer::kMaxVertexBuffers;
		RD_WRN("Submit too many Vertex Buffers TOTAL {} MAX {}", count, m);
		count = OGLMeshBuffer::kMaxVertexBuffers;
	}

	u32 buffers[OGLMeshBuffer::kMaxVertexBuffers + 1];
	arr::sttc_list<vertex_buffer, OGLMeshBuffer::kMaxVertexBuffers> vertexBuffers;
	OGL_CHECK(glCreateBuffers(count + 1, buffers))

	u32 vertexCount = 0;
	for (size_t i = 0; i < count; i++)
	{
		OGL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, buffers[i]))
		GLenum usage = vbs[i].data ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW;
		OGL_CHECK(glBufferData(GL_ARRAY_BUFFER, vbs[i].size, vbs[i].data, usage))
		vertexBuffers.push({ buffers[i], vbs[i].layout });
		vertexCount += vbs[i].size / vbs[i].layout.stride;
	}

	index_buffer indexBuffer{ buffers[count], ib.count };
	OGL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer.ID))
	OGL_CHECK(
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		             indexBuffer.count * sizeof(index_buffer_data::TYPE),
		             ib.indexes, GL_STATIC_DRAW)
	)

	size_t index = _vertexArrays.head();
	OGLMeshBuffer *va = new ((void *)_vertexArrays.next_slot()) OGLMeshBuffer(
		indexBuffer, vertexBuffers.get(), vertexCount
	);

	va->Bind();
	OGL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer.ID))

	for (size_t i = 0; i < count; i++)
	{
		OGL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, buffers[i]))
		const vertex_buffer_layout& layout = vbs[i].layout;

		for (size_t j = 0; j < layout.elements.count(); j++)
		{
			const vertex_buffer_element& elem = layout.elements[j];
			OGL_CHECK(glEnableVertexAttribArray(j))
			OGL_CHECK(glVertexAttribPointer(
				j,
				GetUniformComponentCount(elem.type),
				UniformTypeToGLType(elem.type),
				elem.normalised ? GL_TRUE : GL_FALSE,
				layout.stride,
				(const void*) (i64)elem.offset
			))
		}
	}

	return hdl_mesh{
		(hdl_mesh::TYPE) index,
		_generations.tick(index)
	};
}

_MeshBuffer * OGLMeshBufferLib::Get(hdl_mesh h)
{
#ifdef ENABLE_ASSERTS
	if (! IsValid(h))
	{
		hdl_mesh::TYPE got = h.generation;
		RD_ERR("Invalide Vertex Array GOT {} EXPECT {}",
			got, _generations[h.index]
			);
		return nullptr;
	}
#endif
	return &_vertexArrays[h.index];
}

bool OGLMeshBufferLib::IsValid(hdl_mesh h) const
{
	return h.generation == _generations[h.index];
}
};

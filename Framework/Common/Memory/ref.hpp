#pragma once
#include "core"
#include "Util/callback.hpp"

// A reference counting mechanism meant to be used sparingly
template<typename T>
struct ref
{
	ref() : _data(nullptr), _count(nullptr), _fnDelete() { }
	ref(T *data, u16 *count, clbk::delegate<void (T*)> fnDel) : _data(data), _count(count), _fnDelete(fnDel)
	{
		(*_count)++;
	}

	ref(const ref& other)
		: _data(other._data), _count(other._count), _fnDelete(other._fnDelete)
	{
		if (_count)
			(*_count)++;
	}

	ref(ref&& other)
		: _data(other._data), _count(other._count), _fnDelete(other._fnDelete)
	{
		MOVE_CONSTRUCTOR_CHECK
		other._data = nullptr;
		other._count = nullptr;
		other._fnDelete = clbk::delegate<void (T*)>{};
	}

	~ref()
	{
		if (!_count)
			return;
		(*_count)--;

		if (*_count == 0)
			_fnDelete(_data);
	}

	ref& operator=(const ref& other)
	{
		_data = other._data;
		_count = other._count;
		_fnDelete = other._fnDelete;
		if (_count)
			(*_count)++;
		return *this;
	}

	ref& operator=(ref&& other)
	{
		MOVE_CHECK
		_data = other._data;
		_count = other._count;
		_fnDelete = other._fnDelete;
		other._data = nullptr;
		other._count = nullptr;
		other._fnDelete = clbk::delegate<void (T*)>{};
		return *this;
	}

	T * operator ->() { return _data; }
	const T * operator ->() const { return _data; }
	operator bool() const { return _data; }

	inline bool operator==(const ref& other) const { return _data == other._data; }
	inline bool operator!=(const ref& other) const { return !(*this == other); }

	T * data() { return _data; }
	const T * data() const { return _data; }

private:
	T *_data;
	u16 *_count;
	clbk::delegate<void (T*)> _fnDelete;
};

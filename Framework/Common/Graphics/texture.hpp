#pragma once
#include "core"
#include "math"
#include "Memory/generations.hpp"

namespace gfx {

using hdl_texture = mem::handle<u16, 8, 8>;

enum struct ETextureType { TWOD, THREED };
enum struct ETextureTexel
{
	HALF_FLOAT, FLOAT,
	UINT8, UINT16, UINT32,
	SINT8, SINT16, SINT32,
	NORMALISED_FLOAT,
	NORMALISED_UINT8, NORMALISED_UINT16,
	NORMALISED_SINT8, NORMALISED_SINT16
};

struct texture_format
{
	ETextureTexel texel;
	u8 numChannels;
};

struct texture
{
	const char *path;
	void *image;
	glm::uvec3 dimensions;
	texture_format format;
};

class _Texture
{
public:
	virtual ~_Texture() { }
	virtual void Bind(u32 slot = 0) const = 0;
	virtual void SetTextureData(const void *, size_t) = 0;
	// LATER GetTextureData

	virtual const ETextureType type() const = 0;
	virtual texture_format format() const = 0;
	virtual glm::uvec3 dimensions() const = 0;

#ifdef HOLD_IDS
	virtual const char * GetPath() const = 0;
#endif
};

// TODO The data here does not need to be contiguous in memory
// So it is fine to use a sparse array to store the data
// This is true for all the graphics libraries
class _TextureLib
{
public:
	virtual ~_TextureLib() { }
	virtual hdl_texture Create(texture_format, u32 width, u32 height, u32 depth = 0) = 0;
	virtual hdl_texture Load(const texture&) = 0;
	virtual _Texture * Get(hdl_texture) = 0;
	virtual bool IsValid(hdl_texture) const = 0;
};
};

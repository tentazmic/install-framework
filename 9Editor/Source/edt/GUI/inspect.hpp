#pragma once

#include "math"
#include "ECS/ecs.hpp"
#include "Graphics/texture.hpp"

namespace edt::inspect {

void InspectComponent(ecs::Type type, void *component);

void Bool(const char *name, bool *b);

void Int(const char *name, i32 *i);
void Float(const char *name, f32 *f);
void Vec2(const char *name, math::vec2 *v);
void Vec3(const char *name, math::vec3 *v);
void Vec4(const char *name, math::vec4 *v);
void Mat4(const char *name, glm::mat4 *v);

void Texture(const char *name, gfx::hdl_texture tex, gfx::_TextureLib *library);
};

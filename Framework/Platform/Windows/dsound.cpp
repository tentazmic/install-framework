#include "pch.h"
#include "dsound.hpp"

#include "debug"

#include "Audio/audio.hpp"
#include "dll_handle.hpp"

#include <Windows.h>
#include <dsound.h>

namespace dsound {

namespace {
	struct {
		LPDIRECTSOUNDBUFFER SoundBuffer;

		// Audio Format Data
		u32 samplesPerSecond;
		i32 toneHz; // samplesPerSecond / frequency
		u32 numChannels;
		u32 bytesPerStereoSample;

		// Audio Memory
		size_t bfrSecondarySize;
		size_t writeAheadDistance;

		DWORD lastLockPos;
	} s_audio;

	bool s_initialised = false;
	// Marks the last written stereo sample
	DWORD s_iRunningSample = 0;

	snd::buffer s_buffer;

	dll_handle *s_libDSound;

	void FillSoundBuffer();
};
};

namespace audio {
u32 GetSamplesPerSecond() { return dsound::s_audio.samplesPerSecond; }
};

// DirectSound accepts audio channel data interweaved
// So you'll get a LEFT sample then a RIGHT sample then a LEFT then
// RIGHT all in the same sequence of bytes

namespace dsound {
using FnDSoundCreate = HRESULT WINAPI (*)(LPGUID lpGuid, LPDIRECTSOUND* ppDS, LPUNKNOWN  pUnkOuter);

snd::buffer GetBuffer() { return s_buffer; }

void Initialise(u32 samplesPerSecond, f32 bufferDuration, f32 latency)
{
	s_audio.samplesPerSecond = samplesPerSecond;
	s_audio.toneHz = 256;
	s_audio.numChannels = 2;
	s_audio.bytesPerStereoSample = sizeof(snd::sample) * s_audio.numChannels;

	s_audio.bfrSecondarySize = s_audio.samplesPerSecond * s_audio.bytesPerStereoSample * bufferDuration;
	s_audio.writeAheadDistance = s_audio.bfrSecondarySize * (latency / bufferDuration);

	s_audio.lastLockPos = 0;

	PF_TRC("{} {}", s_audio.bfrSecondarySize, s_audio.writeAheadDistance);

	s_libDSound = new dll_handle("dsound.dll");
	if (!s_libDSound->IsValid())
	{
		PF_ERR("DirectSound DLL not found");
		return;
	}

	FnDSoundCreate DirectSoundCreate =
		RCAST(FnDSoundCreate)(s_libDSound->GetFunction("DirectSoundCreate"));

	LPDIRECTSOUND directSound;
	if (!(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &directSound, 0))))
	{
		PF_ERR("FAIL Load DirectSoundCreate");
		return;
	}

	// Using the foreground window means that when the window loses focus
	// the music stops playing
	HWND hWnd = GetForegroundWindow();
	if (!hWnd)
		hWnd = GetDesktopWindow();

	if (!SUCCEEDED(directSound->SetCooperativeLevel(hWnd, DSSCL_PRIORITY)))
	{
		PF_ERR("FAIL Set Direct Sound Cooperative Level");
		return;
	}

	// Direct Sound wants a Primary Buffer for some reason I can't remember

	// It is recommended that you set the buffer description to zero
	// upon initialisation
	DSBUFFERDESC bfrDescription = {};
	bfrDescription.dwSize =  sizeof(bfrDescription);
	bfrDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;

	LPDIRECTSOUNDBUFFER bfrPrimary;
	if (!SUCCEEDED(directSound->CreateSoundBuffer(&bfrDescription, &bfrPrimary, 0)))
	{
		PF_ERR("FAIL Create Primary Buffer");
		return;
	}

	WAVEFORMATEX waveFormat = {};
	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = s_audio.numChannels;
	waveFormat.nSamplesPerSec = s_audio.samplesPerSecond;
	waveFormat.wBitsPerSample = sizeof(snd::sample) * 8;
	waveFormat.cbSize = 0;

	waveFormat.nBlockAlign = (waveFormat.nChannels * waveFormat.wBitsPerSample) / 8;
	waveFormat.nAvgBytesPerSec = waveFormat.nBlockAlign * waveFormat.nSamplesPerSec;

	if (!SUCCEEDED(bfrPrimary->SetFormat(&waveFormat)))
	{
		PF_ERR("FAIL Primary Buffer Format");
		return;
	}

	// Create Secondary Buffer
	bfrDescription = {};
	bfrDescription.dwSize = sizeof(bfrDescription);
	bfrDescription.dwFlags = 0;
	bfrDescription.dwBufferBytes = s_audio.bfrSecondarySize;
	bfrDescription.lpwfxFormat = &waveFormat;

	if (!SUCCEEDED(directSound->CreateSoundBuffer(&bfrDescription, &s_audio.SoundBuffer, 0)))
	{
		PF_ERR("FAIL Create Secondary Buffer");
		return;
	}

	s_initialised = true;
	// The number of write ahead samples
	snd::sample *fullBuffer = RCAST(snd::sample *)(malloc(s_audio.writeAheadDistance));
	s_buffer.length = s_audio.writeAheadDistance / s_audio.bytesPerStereoSample;
	s_buffer.left = fullBuffer;
	s_buffer.right = fullBuffer + s_buffer.length;
}

void Shutdown()
{
	free(s_buffer.left);
	delete s_libDSound;
}

void Play()
{
	if (!s_initialised)
	{
		PF_ERR("Direct Sound Uninitialised");
		return;
	}

	if (!SUCCEEDED(s_audio.SoundBuffer->Play(0, 0, DSBPLAY_LOOPING)))
	{
		PF_ERR("FAIL Play Direct Sound");
	}
}

void Stop()
{
	if (!s_initialised)
	{
		PF_ERR("Direct Sound Uninitialised");
		return;
	}

	if (!SUCCEEDED(s_audio.SoundBuffer->Stop()))
	{
		PF_ERR("FAIL Stop Direct Sound");
	}
}

void Update()
{
	if (s_buffer.length > 0)
		FillSoundBuffer();

	// The play cursor is an estimate of the point in the
	// buffer where the sound card is playing from
	DWORD cursorPlay, cursorWrite;
	if (!SUCCEEDED(s_audio.SoundBuffer->GetCurrentPosition(&cursorPlay, &cursorWrite)))
	{
		PF_ERR("FAIL Direct Sound Current Position");
		return;
	}

	// The starting position to write to
	const DWORD lockPos = (s_iRunningSample * s_audio.bytesPerStereoSample) % s_audio.bfrSecondarySize;
	s_audio.lastLockPos = lockPos;
	// The final position we intend to write to
	const DWORD cursorTarget = (cursorPlay + s_audio.writeAheadDistance) % s_audio.bfrSecondarySize;
	DWORD lenWrite;
	if (lockPos > cursorTarget)
	{
		lenWrite = (s_audio.bfrSecondarySize - lockPos) + cursorTarget;
	}
	else
	{
		lenWrite = cursorTarget - lockPos;
	}
	s_buffer.length = lenWrite / s_audio.bytesPerStereoSample;
}

namespace {
	DWORD Fill(snd::sample *out, DWORD index, DWORD lenWrite)
	{
		DWORD count = lenWrite / s_audio.bytesPerStereoSample;
		for (; index < count; index++)
		{
			*out = s_buffer.left[index];
			out++;
			*out = s_buffer.right[index];
			out++;
		}
		return index;
	}

	void FillSoundBuffer()
	{
		// DirectSound stores the audio to play in a ring buffer
		// In order to write to the buffer we must lock a region of the buffer
		// Thus, sometimes the region would need to wrap around to the beginning
		// of the ring buffer, which is what the region2 variables are for
		void *region1, *region2;
		DWORD lenRegion1, lenRegion2;

		DWORD lenLock = s_buffer.length * s_audio.bytesPerStereoSample;
		HRESULT res = s_audio.SoundBuffer->Lock(
			s_audio.lastLockPos, lenLock,
			&region1, &lenRegion1, &region2, &lenRegion2, 0
		);
		if (!SUCCEEDED(res))
		{
			PF_ERR("FAIL Direct Sound Lock Buffer");
			return;
		}

		PF_ASSERT((lenRegion1 % 16 == 0 && (!region2 || lenRegion2 % 16 == 0)), "Direct Sound Invalid Write Regions");

		DWORD index = Fill(RCAST(snd::sample *)(region1), 0, lenRegion1);
		Fill(RCAST(snd::sample *)(region2), index, lenRegion2);
		s_iRunningSample += (lenRegion1 + lenRegion2) / s_audio.bytesPerStereoSample;

		res = s_audio.SoundBuffer->Unlock(region1, lenRegion1, region2, lenRegion2);
		if (!SUCCEEDED(res))
		{
			PF_ERR("FAIL Direct Sound Unlock Buffer");
			return;
		}
	}
};
};

#!/usr/bin/python
import os
import sys
import shutil
import subprocess
import pathlib

import build.compiler as cmpl
import util.settings as stts

def get_externals_folder(name: str) -> str:
	return  stts.DIR_EXTERNALS + name

def get_cmake_generator() -> str:
	return '-G "MinGW Makefiles"' if sys.platform == 'win32' else ''

def get_build_folder(name: str) -> str:
	build = f'{stts.DIR_BUILD}/Externals/{name}'
	if not os.path.exists(build):
		os.makedirs(build)
	return build + '/'

def install_artifact(src: str, dest: str) -> bool:
	if not os.path.exists(src):
		return False
	res = shutil.copy(src, dest)
	return res == dest

def build_cpp_mmf(settings: stts.BuildSettings) -> int:
	library = 'libcpp_mmf.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('CPP MMF is already built')
		return 0

	folder = get_externals_folder('cpp_mmf')
	if not os.path.exists(folder):
		print(f'cpp_mmf not found at {folder}')
		return 1

	files = [ f'{folder}/memory_mapped_file.cpp' ]
	macros = ''
	if sys.platform == 'win32':
		macros = '-D_WIN32'

	print('Creating CPP MMF Library')

	dir_build = get_build_folder('cpp_mmf')

	obj = dir_build + '/mmf.o'
	command = f'g++ -fPIC -c {folder}/memory_mapped_file.cpp {macros} -o {obj}'
	out = subprocess.run(command, capture_output=True, shell = True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	return cmpl.link_static_library(settings, library, [ obj ])

def build_glad(settings: stts.BuildSettings) -> int:
	library = 'libglad.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('Glad is already built')
		return 0

	folder = get_externals_folder('Glad')
	if not os.path.exists(folder):
		print(f'Glad not found at {folder}')
		return 1

	print('Creating Glad Library')

	dir_build = get_build_folder('Glad')

	obj = dir_build + '/glad.o'

	command = f'gcc -fPIC -std=c17 -c {folder}/src/glad.c -o {obj}' +\
		f' -I{folder}/include'
	out = subprocess.run(command, capture_output=True, shell = True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	return cmpl.link_static_library(settings, library, [ obj ])

def build_glfw(settings: stts.BuildSettings) -> int:
	library = 'libglfw3.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('GLFW is already built')
		return 0

	folder = get_externals_folder('GLFW')
	if not os.path.exists(folder):
		print(f'GLFW not found at {folder}')
		return 1

	print('Creating GLFW Library')

	dir_build = get_build_folder('GLFW')
	command = f'cmake -S {folder} -B {dir_build} ' +\
		f'-D CMAKE_C_COMPILER=gcc -D GLFW_BUILD_EXAMPLES=OFF ' +\
		f'-D GLFW_BUILD_TESTS=OFF -D GLFW_BUILD_DOCS=OFF ' + get_cmake_generator()

	if settings.use_clean_slate:
		shutil.rmtree(dir_build)

	if settings.verbosity_level >= 2:
		print('Generating GLFW CMake files')

	out = subprocess.run(command, capture_output=True, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if settings.verbosity_level >= 2:
		print('Building GLFW')

	out = subprocess.run('cmake --build ' + dir_build, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if not os.path.exists(stts.DIR_LIBS):
		os.makedirs(stts.DIR_LIBS)

	lib_dest = f'{stts.DIR_LIBS}/{library}'
	res = shutil.move(f'{dir_build}/src/{library}', lib_dest)
	return 0 if res == lib_dest else 1

def build_imgui(settings: stts.BuildSettings) -> int:
	library = 'libimgui.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('ImGui is already built')
		return 0

	folder = get_externals_folder('imgui')
	if not os.path.exists(folder):
		print(f'imgui not found at {folder}')
		return 1

	print('Creating ImGui Library')

	dir_build = get_build_folder('imgui')

	sources = [ 'imgui_draw.cpp', 'imgui_tables.cpp', 'imgui_widgets.cpp',
		'imgui.cpp', 'imgui_demo.cpp' ]
	files = [
		( os.path.abspath(f'{folder}/{s}'),
			os.path.abspath(f'{dir_build}/{s.split(".")[0]}.o' ))
		for s in sources
	]
	command = shutil.which('gcc') + ' -fPIC -c {inp} -o {out}'
	work = [ (inp, command.format(inp=inp, out=out)) for inp, out in files ]

	ret = cmpl.parallel_compile('ImGui', work, settings)
	if ret != 0:
		return ret

	return cmpl.link_static_library(settings, library, [ out for _, out in files ])

def build_spdlog(settings: stts.BuildSettings) -> int:
	library = 'libspdlog.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('SPDLOG is already built')
		return 0

	folder = get_externals_folder('spdlog')
	if not os.path.exists(folder):
		print(f'spdlog not found at {folder}')
		return 1

	dir_build = get_build_folder('spdlog')
	print('Creating SPDLOG library')

	if settings.use_clean_slate:
		shutil.rmtree(dir_build)

	if settings.verbosity_level >= 2:
		print('Generating SPDLOG CMake files')

	command = f'cmake -S {folder} -B {dir_build} -D CMAKE_CXX_FLAGS="-fPIC"' +\
		f' -D SPDLOG_MASTER_PROJECT=OFF ' + get_cmake_generator()
	out = subprocess.run(command, capture_output=True, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if settings.verbosity_level >= 2:
		print('Building SPDLOG')

	command = 'cmake --build ' + dir_build
	out = subprocess.run(command, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if not os.path.exists(stts.DIR_LIBS):
		os.makedirs(stts.DIR_LIBS)

	lib_dest = f'{stts.DIR_LIBS}/{library}'
	res = shutil.move(dir_build + '/' + library, lib_dest)
	return 0 if res == lib_dest else 1

def build_stb(play_sound: int) -> int:
	library = 'libstb.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('stb is already built')
		return 0

	folder = get_externals_folder('stb')
	if not os.path.exists(folder):
		print(f'stb not found at {folder}')
		return 1

	print('Creating stb Library')

	dir_build = get_build_folder('stb')

	if settings.verbosity_level >= 1:
		print('Compile stb')

	obj = dir_build + '/stb_image.o'
	command = f'g++ -fPIC -std=c++1z -c {folder}/stb_image.cpp -o {obj}'
	out = subprocess.run(command, capture_output=True, shell = True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	return cmpl.link_static_library(settings, library, [ obj ])

def build_msdf(settings: stts.BuildSettings) -> int:
	exe = 'msdf-atlas-gen' +('.exe' if sys.platform == 'win32' else '')
	libraries = [ 'libmsdfgen-core.a', 'libmsdfgen-ext.a', 'libmsdf-atlas-gen.a' ]
	match sys.platform:
		case 'win32':
			deps = [ 'brotlicommon', 'brotlidec', 'bz2', 'freetype', 'libpng16', 'zlib1' ]
			deps = [ d + '.dll' for d in deps ]
			skia_command = '-D MSDF_ATLAS_USE_SKIA=OFF '
		case 'linux' | 'linux2':
			deps = [ 'freetype', 'png', 'z', 'bz2', 'skia', 'brotlidec', 'brotlicommon' ]
			deps = [ f'lib{d}.a' for d in deps ]
			skia_command = ''

	if not settings.use_clean_slate\
	   and all([ os.path.exists(stts.DIR_LIBS + l) for l in libraries ])\
	   and os.path.exists(stts.DIR_UTILS + exe)\
	   and all([ os.path.exists(stts.DIR_LIBS + l) for l in dlls ]):
		print('MSDF is already built')
		return 0

	folder = get_externals_folder('msdf_atlas_gen')
	dir_build = get_build_folder('msdf_atlas_gen')
	if not os.path.exists(folder):
		print(f'MSDF not found at {folder}')
		return 1

	print('Creating MSDF Atlas Gen library')

	if settings.use_clean_slate:
		shutil.rmtree(dir_build)

	if settings.verbosity_level >= 2:
		print('Generating MSDF Atlas Gen CMake files')

	dir_install = dir_build + 'install/'
	command =\
	  f'cmake -S {folder} -B {dir_build} -DCMAKE_INSTALL_PREFIX:PATH={dir_install} ' +\
	  '-D MSDF_ATLAS_INSTALL=ON ' + skia_command + get_cmake_generator()
	out = subprocess.run(command, capture_output=True, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if settings.verbosity_level >= 2:
		print('Building MSDF Atlas Gen')

	command = f'cmake --build {dir_build} --config Release'
	out = subprocess.run(command, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if settings.verbosity_level >= 2:
		print('Installing MSDF Atlas Gen')

	command = 'cmake --install ' + dir_build
	out = subprocess.run(command, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if not os.path.exists(stts.DIR_LIBS):
		os.makedirs(stts.DIR_LIBS)
	if not os.path.exists(stts.DIR_UTILS):
		os.makedirs(stts.DIR_UTILS)

	rc = 0
	exe_dest = stts.DIR_UTILS + exe
	rc = 0 if install_artifact(f'{dir_install}/bin/{exe}', exe_dest) else 1

	match sys.platform:
		case 'win32':
			dep_location = dir_build + '/bin/'
		case 'linux' | 'linux2':
			dep_location = dir_build + '/vcpkg_installed/x64-linux/lib/'

	for d in deps:
		dep_dest = stts.DIR_LIBS + d
		if not install_artifact(dep_location + d, dep_dest):
			rc = 1

	for l in libraries:
		lib_dest = stts.DIR_LIBS + l
		if not (install_artifact(f'{dir_install}/lib/{l}', lib_dest) \
		        or install_artifact(f'{dir_install}/lib64/{l}', lib_dest)):
			rc = 1

	inc_dest = stts.DIR_LIBS + 'include_msdf_atlas_gen'
	if os.path.exists(inc_dest):
		shutil.rmtree(inc_dest)
	res = shutil.copytree(dir_install + 'include', inc_dest)
	return rc + (0 if res == inc_dest else 1)


def build_libsoundio(settings: stts.BuildSettings) -> int:
	if sys.platform == 'win32':
		print('libsoundio is POSIX only')
		return 0

	library = 'libsoundio.a'
	if not settings.use_clean_slate and\
		 os.path.exists(stts.DIR_LIBS + library):
		print('libsoundio is already built')
		return 0

	folder = get_externals_folder('libsoundio')
	if not os.path.exists(folder):
		print(f'libsoundio not found at {folder}')
		return 1

	print('Creating libsoundio Library')

	dir_build = get_build_folder('libsoundio')
	command =\
		'ALSA_LIBRARY=/usr/lib64/libasound.so ' +\
		'ALSA_INCLUDE_DIR=/usr/include/sys/asoundlib.h ' +\
		f'cmake -S {folder} -B {dir_build} -D CMAKE_CXX_FLAGS="-fPIC" ' +\
		'-D BUILD_DYNAMIC_LIBS=OFF -D BUILD_EXAMPLE_PROGRAMS=OFF ' +\
		'-D BUILD_TESTS=OFF -D ENABLE_COREAUDIO=OFF -D ENABLE_WASAPI=OFF'

	if settings.use_clean_slate:
		shutil.rmtree(dir_build)

	if settings.verbosity_level >= 2:
		print('Generating libsoundio CMake files')

	out = subprocess.run(command, capture_output=True, shell=True)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if settings.verbosity_level >= 2:
		print('Building SPDLOG')

	out = subprocess.run('make', shell=True, cwd=dir_build)
	cmpl.alert_subprocess_results(command, out,
	                              settings.verbosity_level, settings.build_chime)

	if out.returncode != 0:
		return out.returncode

	if not os.path.exists(stts.DIR_LIBS):
		os.makedirs(stts.DIR_LIBS)

	lib_dest = f'{stts.DIR_LIBS}/{library}'
	res = shutil.move(dir_build + '/' + library, lib_dest)
	return 0 if res == lib_dest else 1


def execute(settings: stts.BuildSettings):
	lib_settings = settings.copy()
	if lib_settings.build_chime == stts.CHIME_ALWAYS:
		lib_settings.build_chime = stts.CHIME_FAIL

	# TODO If one build fails and the next succeed
	# the fail and success chimes are played simultaneously

	returncode = build_cpp_mmf(lib_settings)
	print("=========================")

	returncode += build_glad(lib_settings)
	print("=========================")

	returncode += build_glfw(lib_settings)
	print("=========================")

	returncode += build_imgui(lib_settings)
	print("=========================")

	returncode += build_spdlog(lib_settings)
	print("=========================")

	returncode += build_stb(lib_settings)
	print('=========================')

	returncode += build_msdf(lib_settings)
	print('=========================')

	returncode += build_libsoundio(lib_settings)

	if returncode == 0:
		cmpl.play_audio(True, settings.build_chime)

if __name__ == '__main__':
	settings = stts.get_settings()
	execute(settings)

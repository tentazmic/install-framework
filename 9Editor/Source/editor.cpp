#include "pch.h"
#include <imgui.h>

#include "edt/editor.hpp"

#include "Application/events.hpp"

#include "res/assets.hpp"
#include "res/scene.hpp"
#include "edt/Assets/scene.hpp"
#include "edt/Assets/watcher.hpp"

#include "edt/Panels/viewport.hpp"
#include "edt/Panels/inspector.hpp"
#include "edt/Panels/asset_explorer.hpp"
#include "ECS/ecs.hpp"

#include "input"

#include "gui/imgui.hpp"

const char *res::g_AssetRoot = PRJ_ROOT;
const char *res::g_LocalAssetRoot = EDR_ROOT;

namespace editor {

rdr::_RenderAPI *CurrentAPI;
clbk::sink<void (const edt::asset::scene&)> e_OpenScene;

rdr::twod::Renderer2D *Renderer = nullptr;
};

namespace {
	bool s_ShowDemoWindow;
	bool s_DockspaceOpen;
	edt::panel::PanelRegistry *s_Registry;

	evt::dispatcher *s_Dispatcher;

	ImGuiDockNodeFlags s_DockspaceFlags;

	void HelpMarker(const char* desc)
	{
		ImGui::TextDisabled("(?)");
		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted(desc);
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
		}
	}
};

void GameStartup(const app::startup_config& config, audio::buffer buffer)
{
	editor::CurrentAPI = config.api;

	ecs::sys::Init();
	ecs::sys::machine::FillWithAllSystems(&ecs::sys::Get()->mchMain);

	res::SceneInit();
	edt::asset::SceneInit();

	dinp::RegisterKey(inp::EKey::A);
	dinp::RegisterKey(inp::EKey::D);
	dinp::RegisterKey(inp::EKey::W);
	dinp::RegisterKey(inp::EKey::S);
	dinp::RegisterKey(inp::EKey::Q);
	dinp::RegisterKey(inp::EKey::E);

	dinp::RegisterKey(inp::EKey::ESC);
	dinp::RegisterKey(inp::EKey::TAB);

	s_Dispatcher = new evt::dispatcher(512);
	gui::Initialise(*s_Dispatcher);

	s_Registry = new edt::panel::PanelRegistry("Main", 16);

	s_ShowDemoWindow = false;
	s_DockspaceOpen = true;
	s_DockspaceFlags = ImGuiDockNodeFlags_None;

	memset(buffer.left, 0, buffer.length);
	memset(buffer.right, 0, buffer.length);
}

void GameShutdown()
{
	delete s_Registry;
	edt::asset::Shutdown();
	gui::Shutdown();
}

void GameUpdate(f32 deltaTime)
{
	dinp::Update();

	s_Registry->OnUpdate(deltaTime);

	if (dinp::GetKeyState(inp::EKey::ESC).IsPressed())
		app::Kill();
}

void GameRender()
{
	gui::BeginFrame();

	ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
	const ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->WorkPos);
	ImGui::SetNextWindowSize(viewport->WorkSize);
	ImGui::SetNextWindowViewport(viewport->ID);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
	windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

	if (s_DockspaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
		windowFlags |= ImGuiWindowFlags_NoBackground;

	// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
	// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
	// all active windows docked into it will lose their parent and become undocked.
	// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
	// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

	ImGui::Begin("DockSpace Demo", &s_DockspaceOpen, windowFlags);
	ImGui::PopStyleVar(3);

	// Submit the DockSpace
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
	{
		ImGuiID dockspace = ImGui::GetID("MyDockSpace");
		ImGui::DockSpace(dockspace, ImVec2(0.0f, 0.0f), s_DockspaceFlags);
	}

	if (ImGui::BeginMenuBar())
	{
		if (ImGui::BeginMenu("Windows"))
		{
			if (ImGui::MenuItem("Viewport", NULL))
				s_Registry->CreatePanel<edt::panel::viewport::PnlViewport>(editor::CurrentAPI);
			if (ImGui::MenuItem("Inspector", NULL))
				s_Registry->CreatePanel<edt::panel::inspector::PnlInspector>(s_Registry);
			if (ImGui::MenuItem("Asset Explorer", NULL))
				s_Registry->CreatePanel<edt::panel::asset_explorer::PnlAssetExplorer>("Assets");
			if (ImGui::MenuItem("Details", NULL))
				s_Registry->CreatePanel<edt::panel::PnlDetails>();

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Options"))
		{
			// Disabling fullscreen would allow the window to be moved to the front of other windows,
			// which we can't undo at the moment without finer window depth/z control.

			if (ImGui::MenuItem("Restart", NULL, false, true))
				app::Restart();

			if (ImGui::MenuItem("Close", NULL, false, true))
				app::Kill();

			ImGui::Separator();
			if (ImGui::MenuItem("Show Demo Window"))
				s_ShowDemoWindow = true;
			ImGui::EndMenu();
		}
		HelpMarker(
			"When docking is enabled, you can ALWAYS dock MOST window into another! Try it now!" "\n"
			"- Drag from window title bar or their tab to dock/undock." "\n"
			"- Drag from window menu button (upper-left button) to undock an entire node (all windows)." "\n"
			"- Hold SHIFT to disable docking." "\n"
			"This demo app has nothing to do with it!" "\n\n"
			"This demo app only demonstrate the use of ImGui::DockSpace() which allows you to manually create a docking node _within_ another window." "\n\n"
			"Read comments in ShowExampleAppDockSpace() for more details.");

		ImGui::EndMenuBar();
	}

	if (s_ShowDemoWindow)
		ImGui::ShowDemoWindow(&s_ShowDemoWindow);

	s_Registry->OnImGUIRender();

	ImGui::End();

	gui::EndFrame();
	s_Registry->EndOfFrameCleanup();
}

void GameStreamAudio(audio::buffer buffer)
{
	memset(buffer.left, 0, buffer.length);
	memset(buffer.right, 0, buffer.length);
}

void GamePlatformEvent(evt::event e)
{
	if (e.type == evt::Type::WINDOW_CLOSE)
		app::Kill();

	s_Dispatcher->SubmitEvent(e);
}

void GameFireEvents()
{
	s_Dispatcher->Dispatch();
}
